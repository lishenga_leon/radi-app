import React from 'react';
import { Scene, Router, Drawer } from 'react-native-router-flux';
import { StyleSheet, Dimensions, Platform, View, } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';


import Login from './components/Login';
import Home from './components/Home';
import Joint from './components/Joint'
import Maps from './components/Maps'
import Order from './components/Order'
import Payments from './components/Payments'
import JointPage from './components/common/Joint/JointPage'
import Menu from './components/common/Menu';
import Contact from './components/Contact';
import Settings from './components/Settings';
import Bookings from './components/Bookings';
import Profile from './components/Profile';
import Messages from './components/Messages';
import EditPay from './components/EditPay';
import TermsConditions from './components/TermsConditions';
import AppInfo from './components/AppInfo';
import Forgot from './components/Forgot';
import LoginRegister from './components/LoginRegister';
import AddCard from './components/AddCard';
import RateRestaurant from './components/RateRestaurant';
import UploadPost from './components/common/Home/UploadPost';
import FirstPage from './components/FirstPage';
import Test from './components/common/Test';
import LoginAfterOrder from './components/LoginAfterOrder';
import MapsRestaurants from './components/common/MapsRestaurants/MapsRestaurants';
import PhoneNumber from './components/PhoneNumber';
import PhoneNumberAfterOrder from './components/PhoneNumberAfterOrder';
import Hungry from './components/common/MapsCategory/Hungry';
import Thirsty from './components/common/MapsCategory/Thirsty';
import OutAbout from './components/common/MapsCategory/OutAbout';
import Favorites from './components/Favorites';
import PersonOrders from './components/PersonOrders';
import Privacy from './components/Privacy';
import Transactions from './components/Transactions';
import Password from './components/Password';
import PasswordAfterOrder from './components/PasswordAfterOrder';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const menuIcon = (
    <View style={{ marginLeft: 15, marginTop: PHONE_HEIGHT / 4, height: PHONE_HEIGHT / 8 }}>
    </View>
)

const RouterComponent = ({ auth, main }) => {

    const { navbarImage, navbarStyle } = styles;

    return (
        <Router>
            <Scene key='root'>
                <Scene key='auth' hideNavBar={true} initial={auth}>
                    <Scene key='firstpage' component={FirstPage} initial/>
                    <Scene key='login' component={Login} />
                    <Scene
                        key='phonenumber'
                        component={PhoneNumber}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key='password'
                        component={Password}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene key='forgot' component={Forgot} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                </Scene>
                <Scene key='main' type="replace" hideNavBar={true} initial={main}>
                    <Drawer
                        key='drawer'
                        drawer
                        contentComponent={Menu}
                        drawerIcon={menuIcon}
                        drawerWidth={300}
                        navigationBarTitleImageStyle={navbarImage}
                        navigationBarStyle={navbarStyle}
                    >
                        <Scene
                            key="drawer"
                            component={Home}
                            navigationBarTitleImage={false}
                            initial
                            navTransparent={true}
                        />
                        <Scene
                            key="maps"
                            component={Maps}
                            hideDrawerButton={true}
                            navigationBarTitleImage={false}
                            navTransparent={true}
                        />

                        <Scene
                            key="thirsty"
                            component={Thirsty}
                            hideDrawerButton={true}
                            navigationBarTitleImage={false}
                            navTransparent={true}
                        />

                        <Scene
                            key="hungry"
                            component={Hungry}
                            hideDrawerButton={true}
                            navigationBarTitleImage={false}
                            navTransparent={true}
                        />

                        <Scene
                            key="outandabout"
                            component={OutAbout}
                            hideDrawerButton={true}
                            navigationBarTitleImage={false}
                            navTransparent={true}
                        />
                    </Drawer>
                    <Scene
                        key="bookings"
                        component={Bookings}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="favorites"
                        component={Favorites}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="personorders"
                        component={PersonOrders}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="loginafterorder"
                        component={LoginAfterOrder}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="phonenumberafterorder"
                        component={PhoneNumberAfterOrder}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="passwordafterorder"
                        component={PasswordAfterOrder}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="addcard"
                        component={AddCard}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='black'
                    />
                    <Scene
                        key="transactions"
                        component={Transactions}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key="raterestaurant"
                        component={RateRestaurant}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='black'
                    />
                    <Scene
                        key='messages'
                        component={Messages}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key='settings'
                        component={Settings}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key='contact'
                        component={Contact}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key='privacy'
                        component={Privacy}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key='mapsrestaurants'
                        component={MapsRestaurants}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene
                        key='loginregister'
                        component={LoginRegister}
                        navTransparent={true}
                        hideNavBar={false}
                        navBarButtonColor='white'
                    />
                    <Scene key='home' component={Home} />
                    <Scene key='uploadpost' component={UploadPost} navBarButtonColor='black' navTransparent={true} hideNavBar={false} title='POST' />
                    <Scene key='joint' component={Joint} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='maps' component={Maps} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='jointpage' component={JointPage} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='order' component={Order} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='payments' component={Payments} navBarButtonColor='white' navBarButtonColor='black' navTransparent={true} hideNavBar={false} />
                    <Scene key='profile' component={Profile} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='editpay' component={EditPay} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='terms' component={TermsConditions} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                    <Scene key='appinfo' component={AppInfo} navBarButtonColor='white' navTransparent={true} hideNavBar={false} />
                </Scene>
            </Scene>
        </Router>
    )
}

const styles = StyleSheet.create({
    navbarImage: {
        width: 100,
        height: 130,
        alignSelf: 'center',
        paddingBottom: 5
    },
    navbarStyle: {
        height: 100,
        backgroundColor: '#001E24'
    },
})


export default RouterComponent;