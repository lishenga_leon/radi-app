import {

    GETTING_USER_DETAILS_ASYNCSTORAGE,

    UPLOAD_PICTURE_POST_SUCCESS, RESTAURANT_NAME_CHANGE_FAIL,

    RESTAURANT_NAME_CHANGE, PICK_RESTAURANT, UPLOAD_PICTURE_POST_ERROR,

    POST_CHANGE, GET_PICTURE, UPLOAD_POST, RESTAURANT_NAME_CHANGE_SUCCESS

} from "./types";

import { apiIp } from "../Config";

import axios from "axios";

import AsyncStorage from "@react-native-community/async-storage";

import FormData from "form-data";

import Geolocation from 'react-native-geolocation-service';

import getDistance from 'geolib/es/getDistance';


// function for changing restaurant name using redux
export const restaurantNameChange = text => {

    return dispatch => {

        AsyncStorage.getItem("token").then(token => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            const data = {

                name: text,

                page: 1,

                size: 10

            }

            dispatch({

                type: RESTAURANT_NAME_CHANGE,

                payload: text,

            });

            axios.post(apiIp + "/restaurant/searchByName", data, { headers: headers }).then(response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    dispatch({

                        type: RESTAURANT_NAME_CHANGE_FAIL,

                        error: 'Failed: Kindly try again later'

                    });

                } else if(JSON.stringify(response.data.status_code) == 200) {

                    dispatch({

                        type: RESTAURANT_NAME_CHANGE_SUCCESS,

                        restaurants: response.data.data

                    });

                }else if(JSON.stringify(response.data.status_code) == 401){

                    dispatch({

                        type: RESTAURANT_NAME_CHANGE_FAIL,

                        error: 'Session Expired: Kindly login again'

                    });

                }

            }).catch(() => { 

                dispatch({

                    type: RESTAURANT_NAME_CHANGE_FAIL,

                    error: 'Failed: Kindly try again later'

                });
                
            })

        })

    };

};



// function for picking a restaurant using redux
export const pickRestaurant = (name, id, location) => {

    return {

        type: PICK_RESTAURANT,

        payload: name,

        restaurant_id: id,

        location: location

    };

};

// function for getting a picture using redux
export const getPicture = picture => {

    return {

        type: GET_PICTURE,

        payload: picture

    };

};

// function for changing post using redux
export const postChange = text => {

    return {

        type: POST_CHANGE,

        payload: text

    };

};

// function for initiating upload of picture and post
export const uploadPost = ({ restaurant_id, post, picture, location }) => {

    return dispatch => {

        dispatch({ type: UPLOAD_POST });

        Geolocation.getCurrentPosition(

            (position) => {

                if(getDistance(position.coords, location) <= 500 ){

                    getUserDetails(dispatch, restaurant_id, post, picture);

                }else{

                    uploadPicturePostError(dispatch, 'Kindly ensure you are at the restaurant' )

                }

            },

            (error) => console.log({ error: error.message }),

            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },

        );

    };

};

// function for dispatching an error gotten when trying to get the user details from asyncstorage
const loginUserFail = dispatch => {

    dispatch({

        type: GETTING_USER_DETAILS_ASYNCSTORAGE

    });

};

// function for dispatching success message when upload has been successful
const uploadPicturePostSuccess = (dispatch, response) => {

    dispatch({

        type: UPLOAD_PICTURE_POST_SUCCESS,

        payload: response

    });

};

// function for dispatching success message when upload has been unsuccessful
const uploadPicturePostError = (dispatch, error) => {

    dispatch({

        type: UPLOAD_PICTURE_POST_ERROR,

        payload: error

    });

};

removeValue = async () => {

    try {

        await AsyncStorage.removeItem("user");

    } catch (e) {

        // remove error

    }

};

// function for gettting the details of the user from asyncstorage
const getUserDetails = (dispatch, restaurant_id, post, picture) => {

    AsyncStorage.getItem("user").then(token => {

        AsyncStorage.getItem("password").then(password => {

            const parse = JSON.parse(token);

            const details = {

                email: parse.data.email,

                password: password

            };

            uploadPicturePost(restaurant_id, post, picture, dispatch);

        });

    });

};

// // function for getting the user token from the server
// const getUserLoginToken = (details, restaurant_id, post, picture, dispatch) => {

//     axios.post(apiIp + "/person/email_login/", details).then(response => {

//         if (JSON.stringify(response.data.status_code) == 500) {

//             loginUserFail(dispatch);

//         } else {

//             storeData("userUpload", JSON.stringify(response.data));

//             uploadPicturePost(restaurant_id, post, picture, dispatch);

//         }

//     }).catch(() => loginUserFail(dispatch));

// };

// function for uploading picture and post of user
const uploadPicturePost = (restaurant_id, post, picture, dispatch) => {

    AsyncStorage.getItem("token").then(token => {

        let data = new FormData();

        data.append("person_id", parse.data.id);

        data.append("restaurant_id", restaurant_id);

        data.append("picture", picture.data);

        data.append("picture_name", picture.fileName);

        data.append("file_type", picture.type);

        data.append("description", post);

        axios.post(apiIp + "/post/createpost/", data, {

            headers: {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                "Content-Type": `multipart/form-data; boundary=${data._boundary}`,

                "token": token

            }

        }).then(response => {

            if (JSON.stringify(response.data.status_code) == 500) {

                uploadPicturePostError(dispatch, 'Failed: Kindly try again later');

            } else if(JSON.stringify(response.data.status_code) == 200) {

                uploadPicturePostSuccess(dispatch, response);

            } else if(JSON.stringify(response.data.status_code) == 401){

                uploadPicturePostError(dispatch, 'Session Expired: Kindly login again');

            }

        }).catch(error => {

            uploadPicturePostError(dispatch, 'Failed: Kindly try again later');

        });

    });

};

// function for storing data for user
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error("AsyncStorage error: " + error.message);

    }

};
