import { CALCULATE_DISTANCE } from './types';

import getDistance from 'geolib/es/getDistance';

import AsyncStorage from '@react-native-community/async-storage';


// function to get distance of a restaurant
export const getRestaurantDistance = (restaurants) => {

    return (dispatch) => {

        AsyncStorage.getItem('UserLocation').then((locate) => {

            const loc = JSON.parse(locate)

            const total = []

            restaurants.map((item) => {

                const final = {

                    'id': item.id,

                    'category': item.category,

                    'description': item.description,

                    'name': item.name,

                    'phone_number': item.phone_number,

                    'location': item.location,

                    'max_capacity': item.max_capacity,

                    'created_at': item.created_at,

                    'updated_at': item.updated_at,

                    'status': item.status,

                    'cover_image': item.cover_image,

                    'distance': getDistance(loc, item.location) + ' m'

                }

                total.push(final)

            })

            dispatch({

                type: CALCULATE_DISTANCE,

                payload: total

            });

        })

    }
    
}