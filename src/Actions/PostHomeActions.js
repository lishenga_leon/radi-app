import {

    POSTS_SUCCESS, POSTS_FAIL, LIKE_SUCCESS, ISREFRESH_STATE, LOADING_STATE, POSTS_REFRESH,

    LIKE_FAIL, USER_ID, SHOW_UPLOAD_CAMERA_ICON, DATE_TIMER_BOOKING_TIMER, NEXT_PAGE

} from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import { StyleSheet, Platform, Dimensions } from 'react-native'

import AsyncStorage from '@react-native-community/async-storage';

import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"

    ? Dimensions.get("window").height

    : ExtraDimensions.getRealWindowHeight();


const PHONE_HEIGHT_ANDROID = ExtraDimensions.getRealWindowHeight()

const PHONE_HEIGHT_IOS = Dimensions.get("window").height


const PHONE_WIDTH_ANDROID = ExtraDimensions.getRealWindowWidth()

const PHONE_WIDTH_IOS = Dimensions.get("window").width

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"

    ? Dimensions.get("window").width

    : ExtraDimensions.getRealWindowWidth();



// function for sending the user_id using redux
export const userId = (userId) => {

    return {

        type: USER_ID,

        payload: userId

    }

}


// function for setting allposts state via redux
export const postsRefresh = () => {

    return {

        type: POSTS_REFRESH,

        payload: [],

    }

}

// function for setting isRefresh state via redux
export const setIsRefreshState = (value) => {

    return {

        type: ISREFRESH_STATE,

        payload: value,

    }

}

// function for setting loading state via redux
export const setLoadingState = (value) => {

    return {

        type: LOADING_STATE,

        payload: value

    }

}

// function for setting page for pagination via redux
export const nextPagePagination = (value) => {

    return {

        type: NEXT_PAGE,

        payload: value

    }

}

// function for showing the camera icon when a user hasn't logged in
export const showCameraIcon = () => {

    return {

        type: SHOW_UPLOAD_CAMERA_ICON,

        payload: false

    }

}

// function for subtrating date time for booking
export const dateTimeBookingTimer = (text) => {

    return {

        type: DATE_TIMER_BOOKING_TIMER,

        payload: text

    }

}

// send push notification when an order is one hour ready
export const sendPushNotForOrder = (data) => {

    return (dispatch) => {

        // send push notification when order is one hour away from being ready

        const sendPush = {

            person_id: data,

            body: 'Your order is one hour away from being ready.'

        }

        axios.post(apiIp + '/person/sendPushNotification/', sendPush).then(

            response => {

                console.log(response)

            }

        ).catch(() => PostsFail(dispatch, 'Failed: Kindly try again later'))

    }

};

// function to make API call to server to get posts
export const getPostsServer = ({ page, user_id }) => {

    return (dispatch) => {

        const data = {

            page: page,

            items: 10,

        }
        AsyncStorage.getItem('token').then((token)=>{

            let headers = {
        
              accept: "application/json",
        
              "Accept-Language": "en-US,en;q=0.8",
        
              'Content-Type': 'application/json',
        
              "token": token
        
            }
        
            axios.post(apiIp + '/post/displayPostsSorted', data, { headers: headers }).then(

                response => {
    
                    if (JSON.stringify(response.data.status_code) == 500) {
    
                        PostsFail(dispatch, 'Failed: Kindly try again later')
    
                    } else if(JSON.stringify(response.data.status_code) == 200) {
    
                        const items = response.data.data.map(item => {
    
                            if (user_id == item.like_person_id) {
    
                                item.isSelect = true;
    
                                item.selectedClass = styles.likeIconStyle;
    
                                return item;
    
                            } else {
    
                                item.isSelect = false;
    
                                item.selectedClass = styles.likeIconStyle;
    
                                return item;
                            }
    
                        });
    
                        PostsSuccess(dispatch, items, user_id, countNumberOfObjects(items), headers)
    
                    }else if (JSON.stringify(response.data.status_code) == 401) {
    
                        PostsFail(dispatch, 'Session Expired: Kindly login again')
    
                    }
    
                }
    
            ).catch((error) => PostsFail(dispatch, 'Failed: Kindly try again later'))
        
        })

    }

}

// function to count the number of objects an array
const countNumberOfObjects = (array) => {

    let result = 0;

    for (let prop in array) {

        if (array.hasOwnProperty(prop)) {

            // or Object.prototype.hasOwnProperty.call(obj, prop)

            result++;

        }

    }

    return result;

}


// function for creating a like using redux and axios
export const createLike = (post_id, posts) => {

    return (dispatch) => {

        AsyncStorage.getItem("token").then(token => {

            let headers = {
        
                accept: "application/json",
        
                "Accept-Language": "en-US,en;q=0.8",
        
                'Content-Type': 'application/json',
        
                "token": token
        
            }

            const details = {

                post_id: post_id.post_id,

                person_id: post_id.person_id

            };

            axios.post(apiIp + "/post/like/", details, { headers: headers }).then(response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    LikeFail(dispatch, 'Failed: Kindly try again later');

                } else if(JSON.stringify(response.data.status_code) == 200) {

                    LikeSuccess(dispatch, post_id.post_id, posts);

                }else if(JSON.stringify(response.data.status_code) == 401){

                    LikeFail(dispatch, 'Session Expired: Kindly login again');

                }

            }).catch(() => LikeFail(dispatch, 'Failed: Kindly try again later'));

        });

    }

}

// function for unliking a post using redux and axios
export const unLike = (post_id, posts) => {

    return (dispatch) => {

        AsyncStorage.getItem("token").then(token => {

            let headers = {
        
                accept: "application/json",
        
                "Accept-Language": "en-US,en;q=0.8",
        
                'Content-Type': 'application/json',
        
                "token": token
        
            }

            const details = {

                post_id: post_id.post_id,

                person_id: post_id.person_id

            };

            axios.post(apiIp + "/post/unlike/", details, { headers: headers }).then(response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    LikeFail(dispatch, 'Failed: Kindly try again later');

                } else if(JSON.stringify(response.data.status_code) == 200) {

                    LikeSuccess(dispatch, post_id.post_id, posts);

                }else if(JSON.stringify(response.data.status_code) == 401){

                    LikeFail(dispatch, 'Session Expired: Kindly login again');

                }

            }).catch(() => LikeFail(dispatch, 'Failed: Kindly try again later'));

        });

    }

}


const PostsFail = (dispatch, error) => {

    dispatch({

        type: POSTS_FAIL,

        error: error

    })

}

const PostsSuccess = (dispatch, response, person_id, arrayNumber, headers) => {

    dispatch({

        type: POSTS_SUCCESS,

        payload: response,

        person_id: person_id,

        arrayNumber: arrayNumber,
        
        headers: headers

    })

}


const LikeFail = (dispatch, error) => {

    dispatch({

        type: LIKE_FAIL,

        error: error

    })

}

const LikeSuccess = (dispatch, post_id, posts) => {

    dispatch({

        type: LIKE_SUCCESS,

        payload: post_id,

        posts: posts

    })

}

const styles = StyleSheet.create({

    likeIconStyle: {

        marginTop: -15,

        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,

        width: PHONE_WIDTH / 15,

    },

});