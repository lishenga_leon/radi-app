import { TRANSACTIONS_SUCCESS, TRANSACTIONS_FAIL } from './types';

import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';

import { apiIp } from '../Config';


// function to get the 10 lastest transactions
export const getAllTransactions = () => {

    return (dispatch) => {

        AsyncStorage.getItem("token").then(token => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            AsyncStorage.getItem('user').then((details) => {

                const dat = JSON.parse(details)

                const data = {

                    person_id: dat.data.id,

                    page: 1

                }

                axios.post(apiIp + '/payments/getparticularPersontransactionsForApp/', data, { headers: headers }).then(

                    response => {

                        if (JSON.stringify(response.data.status_code) == 500) {

                            dispatch({

                                type: TRANSACTIONS_FAIL,

                                error: 'Failed: Kindly try again later'

                            })

                        } else if(JSON.stringify(response.data.status_code) == 200) {

                            dispatch({

                                type: TRANSACTIONS_SUCCESS,

                                payload: response.data.data

                            })

                        }else if(JSON.stringify(response.data.status_code) == 401){

                            dispatch({

                                type: TRANSACTIONS_FAIL,

                                error: 'Session Expired: Kindly login again'

                            })

                        }

                    }

                ).catch(() => {

                    dispatch({

                        type: TRANSACTIONS_FAIL,

                        error: 'Failed: Kindly try again later'

                    })

                })

            })

        })

    }

}