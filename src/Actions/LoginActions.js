import { 
    EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER, LOGIN_USER_FAIL, LOGIN_USER_SUCCESS
} from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import { Actions } from 'react-native-router-flux';

import AsyncStorage from '@react-native-community/async-storage';


// function for changing the email address text using redux
export const emailAddressChange = (text) => {

    return{

        type: EMAIL_CHANGED,

        payload: text

    }

}

// function for changing the password using redux
export const passwordChange = (text) => {

    return {

        type: PASSWORD_CHANGED,

        payload: text

    }

}

// function for logging in the user or taking an already user to payments page
export const loginUser = ( data ) => {

    return (dispatch) => {

        dispatch({ type: LOGIN_USER });

        axios.post(apiIp + '/person/email_login/', data).then(

            response => {

                if(JSON.stringify(response.data.status_code) == 500){

                    loginUserFail( dispatch )

                }else{

                    storePassword('password', data.password)

                    loginUserSuccess( dispatch, response )

                }

            }

        ).catch(() => loginUserFail(dispatch) )

    }

}

// Private function for dispatching a user failed request 
const loginUserFail = ( dispatch ) =>{

    dispatch({

        type: LOGIN_USER_FAIL, 

    })

}

// Private function for dispatching a user success request
const loginUserSuccess = ( dispatch, response ) => {

    dispatch({

        type: LOGIN_USER_SUCCESS, 

        payload: response 

    })

    // store user details
    storeData('user', JSON.stringify(response.data))

    console.log(response)

    // function to register user device uid
    AsyncStorage.getItem('device_uid').then((uid)=>{

        const data = {

            person_id: response.data.data.id,

            device_uid: JSON.parse(uid)

        }

        let headers = {

            accept: "application/json",

            "Accept-Language": "en-US,en;q=0.8",

            'Content-Type': 'application/json',

            "token": response.headers.token

        }

        axios.post(apiIp + '/person/person_device_uid/', data, { headers: headers }).then(

            response => {

                console.log(response)

            }

        )

    })

    // store user token
    storeData('token', response.headers.token)

    Actions.reset('main');

}

// Function for storing user details
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}

// Function for storing user password
storePassword = async (item, selectedValue)=>{

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }
    
}