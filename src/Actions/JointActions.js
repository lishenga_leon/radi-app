import { JOINT_DETAILS_FAIL, JOINT_DETAILS_SUCCESS } from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';



// function for getting the details of a particular restaurant
export const restaurantDetails = (data) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            axios.post(apiIp + '/restaurant/findById/', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        jointDetailsFail(dispatch, 'Failed: Kindly try again later')

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        AsyncStorage.setItem('restaurant_name', JSON.stringify(response.data.data.name))

                        jointDetailsSuccess(dispatch, response.data.data)

                    }else if (JSON.stringify(response.data.status_code) == 401){

                        jointDetailsFail(dispatch, 'Session Expired: Kindly login again')

                    }

                }

            ).catch(() => jointDetailsFail(dispatch, 'Failed: Kindly try again later'))

        })

    }

}

// function for initiating a redux change when particular restaurant hasn't been retrieved
const jointDetailsFail = (dispatch, error) => {

    dispatch({

        type: JOINT_DETAILS_FAIL,

        error: error

    })

}

// ffunction for initiating a redux change when particular restaurant has been retrieved
const jointDetailsSuccess = (dispatch, data) => {

    dispatch({

        type: JOINT_DETAILS_SUCCESS,

        payload: data

    })
    
}