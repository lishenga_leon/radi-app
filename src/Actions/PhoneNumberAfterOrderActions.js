import { GET_PHONE_NUMBER_AFTER_ORDER, GET_PASSWORD_AFTER_ORDER_GOOGLE_LOGIN } from './types';

// function for changing the phone number text using redux
export const getPhoneNumberCompoAfterOrderChange = (text) => {

    return{

        type: GET_PHONE_NUMBER_AFTER_ORDER,

        payload: text

    }

}

// function for changing the password text using redux
export const getPasswordCompoAfterOrderChange = (text) => {

    return{

        type: GET_PASSWORD_AFTER_ORDER_GOOGLE_LOGIN,

        payload: text

    }
    
}