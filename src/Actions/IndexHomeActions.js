import { SEARCH_MAP } from './types';


export const searchMapChange = (text) => {

    return{

        type: SEARCH_MAP,

        payload: text

    }

}


