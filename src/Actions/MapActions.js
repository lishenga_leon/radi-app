import {

    GET_PERSON_LOCATION, SEARCH_MAP_RESTAURANT, SEARCH_MAP_RESTAURANT_SUCCESS,

    MAP_JOINT_DETAILS_FAIL, MAP_JOINT_DETAILS_SUCCESS, SEARCH_MAP_RESTAURANT_FAIL

} from './types';

import axios from 'axios';

import { apiIp } from '../Config';

import AsyncStorage from "@react-native-community/async-storage";

import getDistance from 'geolib/es/getDistance';


// search for a specific restaurant on map
export const searchRestaurantMap = (text) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            const data = {

                name: text,

                page: 1,

                size: 10

            }

            dispatch({

                type: SEARCH_MAP_RESTAURANT,

                payload: text,

            });

            axios.post(apiIp + "/restaurant/searchByName", data, { headers: headers }).then(response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    dispatch({

                        type: SEARCH_MAP_RESTAURANT_FAIL,

                        error: 'Failed: Kindly try again later'

                    });

                } else if (JSON.stringify(response.data.status_code) == 200) {

                    const location = {

                        latitude: response.data.data[0].location.latitude,

                        longitude: response.data.data[0].location.longitude,

                        latitudeDelta: 1,

                        longitudeDelta: 1

                    }

                    dispatch({

                        type: SEARCH_MAP_RESTAURANT_SUCCESS,

                        restaurants: response.data.data,

                        location: location,

                        total: countNumberOfObjects(response.data.data)

                    });

                } else if (JSON.stringify(response.data.status_code) == 401) {

                    dispatch({

                        type: SEARCH_MAP_RESTAURANT_FAIL,

                        error: 'Session Expired: Kindly login again'

                    });

                }

            }).catch(() => {

                dispatch({

                    type: SEARCH_MAP_RESTAURANT_FAIL,

                    error: 'Failed: Kindly try again later'

                });

            })

        })

    }

};


// function for dispatching redux code used to count the number of objects in an array
export const getUserLocation = (region, arrayCount) => {

    return (dispatch) => {

        AsyncStorage.setItem('UserLocation', JSON.stringify(region))

        dispatch({ type: GET_PERSON_LOCATION, restaurants: [], total: countNumberOfObjects(arrayCount) })

    }

}

// function for getting the details of one searched restaurant
export const restaurantGetDetails = (data, coord) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            axios.post(apiIp + '/restaurant/findById/', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        jointDetailsFail(dispatch, coord, 'Failed: Kindly try again later')

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        AsyncStorage.getItem('UserLocation').then((locate) => {

                            const loc = JSON.parse(locate)

                            jointDetailsSuccess(dispatch, response.data.data, coord, getDistance(loc, coord) + ' m')

                        })

                    } else if (JSON.stringify(response.data.status_code) == 401) {

                        jointDetailsFail(dispatch, coord, 'Session Expired: Kindly login again')

                    }

                }

            ).catch(() => jointDetailsFail(dispatch, coord, 'Failed: Kindly try again later'))

        })

    }

}

// function to count the number of objects an array
const countNumberOfObjects = (array) => {

    let result = 0;

    for (let prop in array) {

        if (array.hasOwnProperty(prop)) {

            // or Object.prototype.hasOwnProperty.call(obj, prop)

            result++;

        }

    }

    return result;

}

// function for initiating a redux change when joint's clicked has been gotten
const jointDetailsFail = (dispatch, coord, error) => {

    dispatch({

        type: MAP_JOINT_DETAILS_FAIL,

        coord: coord,

        error: error

    })

}

// function for initiating a redux change when joint's clicked has been gotten
const jointDetailsSuccess = (dispatch, data, coord, distance) => {

    dispatch({

        type: MAP_JOINT_DETAILS_SUCCESS,

        payload: data,

        coord: coord,

        distance: distance

    })

}
