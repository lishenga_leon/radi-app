import {

    USER_NAMES, RATE_RESTAURANT, START_RATE_RESTAURANT,

    RATE_RESTAURANT_SUCCESS, RATE_RESTAURANT_FAIL

} from './types';

import axios from 'axios';

import { apiIp } from '../Config';

import { Actions } from 'react-native-router-flux';

import AsyncStorage from '@react-native-community/async-storage';



// function for dispatching redux code for getting the logged in user
export const userNameRateRestaurant = (text) => {

    return {

        type: USER_NAMES,

        payload: text

    }

}


// function for dispatching redux code for showing rate of a restaurant
export const restaurantRate = (text) => {

    return {

        type: RATE_RESTAURANT,

        payload: text

    }

}


// function for rating restaurant using axios and redux
export const rateRestaurant = (data) => {

    return (dispatch) => {

        AsyncStorage.getItem("token").then(token => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            dispatch({ type: START_RATE_RESTAURANT });

            axios.post(apiIp + '/restaurant/rateRestaurant', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        dispatch({

                            type: RATE_RESTAURANT_FAIL,

                            error: 'Failed: Kindly try again later'

                        })

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        dispatch({

                            type: RATE_RESTAURANT_SUCCESS

                        })

                        Actions.reset('main')

                    }else if (JSON.stringify(response.data.status_code) == 401){

                        dispatch({

                            type: RATE_RESTAURANT_FAIL,

                            error: 'Session Expired: Kindly login again'

                        })

                    }

                }

            ).catch(() => dispatch({

                type: RATE_RESTAURANT_FAIL,

                error: 'Failed: Kindly try again later'

            }))

        })

    }

}