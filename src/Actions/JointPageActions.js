import { GET_MENU_SUCCESS, MENU_FAIL, SHOW_OFFERS_MODAL } from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';


// function for showing the offers for a restaurant
export const showOffersModal = (status) => {

    return {

        type: SHOW_OFFERS_MODAL,

        payload: status

    }

}

// function to make API call to server to get menus
const getMenuServer = (dispatch, details, restaurant_name, headers) => {

    axios.post(apiIp + '/food/RestaurantMenus/', details, { headers: headers }).then(

        response => {

            if (JSON.stringify(response.data.status_code) == 500) {

                MenuFail(dispatch, restaurant_name, 'Failed: Kindly try again later')

            } else if(JSON.stringify(response.data.status_code) == 200) {

                // Store Menus used in comparing for getting foods picked
                storeData('Menus', JSON.stringify(response.data.data))

                // Remove foods that have been saved in the phone localstorage
                const data = response.data.data

                for (let i in data) {

                    AsyncStorage.removeItem('PeopleEating' + data[i].id)

                }

                MenuSuccess(dispatch, response.data.data, restaurant_name)

            }else if (JSON.stringify(response.data.status_code) == 401){

                MenuFail(dispatch, restaurant_name, 'Session Expired: Kindly login again')

            }

        }

    ).catch(() => MenuFail(dispatch, restaurant_name, 'Failed: Kindly try again later'))

}

// function for getting user token for authentication
export const getUserTokenRestaurant = (restaurant_id) => {

    return (dispatch) => {

        AsyncStorage.getItem("token").then((token) => {

            AsyncStorage.getItem("restaurant_name").then((data) => {

                let headers = {

                    accept: "application/json",
    
                    "Accept-Language": "en-US,en;q=0.8",
    
                    'Content-Type': 'application/json',
    
                    "token": token
    
                }

                const details = {

                    restaurant_id: restaurant_id,

                };

                const restaurant_name = JSON.parse(data)

                getMenuServer(dispatch, details, restaurant_name, headers);

            });

        });

    }

};

// function to dispatch failure to get menu from server
const MenuFail = (dispatch, restaurant_name, error) => {

    dispatch({

        type: MENU_FAIL,

        restaurant_name: restaurant_name,

        error: error

    })

}

//function to dispatch success on getting menus from server
const MenuSuccess = (dispatch, response, restaurant_name) => {

    dispatch({

        type: GET_MENU_SUCCESS,

        payload: response,

        restaurant_name: restaurant_name

    })

}

// function for storing data for menus
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error("AsyncStorage error: " + error.message);

    }
    
};