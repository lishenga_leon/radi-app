import { FAVORITE_RESTAURANTS, FAVORITE_RESTAURANTS_FAIL } from './types';

import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';

import { apiIp } from '../Config';


// function to 5 favorite restaurants of a person
export const get5FavoriteRestaurants = () => {

    return (dispatch) => {

        AsyncStorage.getItem('user').then((details) => {

            const dat = JSON.parse(details)

            const data = {

                person_id: dat.data.id

            }

            AsyncStorage.getItem('token').then((token) => {

                let headers = {

                    accept: "application/json",

                    "Accept-Language": "en-US,en;q=0.8",

                    'Content-Type': 'application/json',

                    "token": token

                }

                axios.post(apiIp + '/analytics/getTop5Favourites/', data, { headers: headers }).then(

                    response => {

                        if (JSON.stringify(response.data.status_code) == 500) {

                            dispatch({

                                type: FAVORITE_RESTAURANTS_FAIL,

                                error: 'Failed: Kindly login again later'

                            })

                        } else if(JSON.stringify(response.data.status_code) == 200) {

                            dispatch({

                                type: FAVORITE_RESTAURANTS,

                                payload: response.data.data

                            })

                        }else if(JSON.stringify(response.data.status_code) == 401){

                            dispatch({

                                type: FAVORITE_RESTAURANTS_FAIL,

                                error: 'Session Expired: Kindly login again'

                            })
            
                        }

                    }

                ).catch(() => {

                    dispatch({

                        type: FAVORITE_RESTAURANTS_FAIL,

                        error: 'Failed: Kindly login again later'

                    })

                })

            })

        })

    }

}