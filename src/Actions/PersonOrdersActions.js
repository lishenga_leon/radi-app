import { GET_ALL_ORDERS, GET_ALL_ORDERS_FAIL } from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';


// Get all orders that a person made
export const getAllOrdersPersonMade = () => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            AsyncStorage.getItem('user').then((details) => {

                const data = {

                    person_id: JSON.parse(details).data.id

                }

                axios.post(apiIp + '/orders/getPersonsOrders', data, { headers: headers }).then(

                    response => {

                        if (JSON.stringify(response.data.status_code) == 500) {

                            dispatch({

                                type: GET_ALL_ORDERS_FAIL,

                                error: 'Failed: Kindly try again later'

                            })

                        } else if(JSON.stringify(response.data.status_code) == 200) {

                            dispatch({

                                type: GET_ALL_ORDERS,

                                payload: response.data.data

                            })

                        } else if(JSON.stringify(response.data.status_code) == 401){

                            dispatch({

                                type: GET_ALL_ORDERS_FAIL,

                                error: 'Session Expired: Kindly login later'

                            })

                        }

                    }

                ).catch(() => dispatch({

                    type: GET_ALL_ORDERS_FAIL,

                    error: 'Failed: Kindly try again later'

                }))

            })

        })

    }

};