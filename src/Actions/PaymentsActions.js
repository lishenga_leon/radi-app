import {

    MAKE_PAYMENT_MPESA, MAKE_PAYMENT_CARD, MAKE_PAYMENT_PAYPAL,

    PAYMENT_SUCCESS, PAYMENT_FAIL, PHONE_NUMBER_CHANGED

} from './types';

import { Actions } from 'react-native-router-flux';

import axios from 'axios';

import { apiIp } from '../Config';

import AsyncStorage from '@react-native-community/async-storage';


// function for dispatching redux code for changing the phone number of a user
export const orderPhoneNumber = (text) => {

    return {

        type: PHONE_NUMBER_CHANGED,

        payload: text

    }

}

// function for going to payments page or user not logged in page
export const makePayment = (data) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            if (data.foods.length) {

                if (data.type === 'MPESA') {

                    dispatch({ type: MAKE_PAYMENT_MPESA });

                    axios.post(apiIp + '/orders/create', data, { headers: headers }).then(

                        response => {

                            if (JSON.stringify(response.data.status_code) == 500) {

                                PaymentFail(dispatch, 'Failed: Kindly try again later')

                            } else if(JSON.stringify(response.data.status_code) == 200) {

                                PaymentSuccess(dispatch, data, headers)

                                Actions.raterestaurant({ orderData: data })

                            }else if(JSON.stringify(response.data.status_code) == 401){

                                PaymentFail(dispatch, 'Session Expired: Kindly login again')

                            }

                        }

                    ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

                } else if (data.type === 'CARD') {

                    dispatch({ type: MAKE_PAYMENT_CARD });

                    axios.post(apiIp + '/orders/create', data, { headers: headers }).then(

                        response => {

                            if (JSON.stringify(response.data.status_code) == 500) {

                                PaymentFail(dispatch, 'Failed: Kindly try again later')

                            } else if(JSON.stringify(response.data.status_code) == 200) {

                                const payOrder = {

                                    person_id: data.person_id,

                                    amount: data.totalFoodPrice,

                                    currency: 'USD',

                                    order_id: response.data.data.order.id

                                }

                                axios.post(apiIp + '/stripe/createstripecharge/', payOrder, { headers: headers }).then(

                                    pay => {

                                        // Pay for food ordered
                                        if (JSON.stringify(pay.data.status_code) == 500) {

                                            PaymentFail(dispatch, 'Failed: Kindly try again later')

                                        } else if(JSON.stringify(pay.data.status_code) == 200) {

                                            PaymentSuccess(dispatch, data, headers)

                                            Actions.raterestaurant({ orderData: data })

                                        }else if(JSON.stringify(pay.data.status_code) == 401){

                                            PaymentFail(dispatch, 'Session Expired: Kindly login again')
            
                                        }

                                    }

                                ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

                            }else if(JSON.stringify(response.data.status_code) == 401){

                                PaymentFail(dispatch, 'Session Expired: Kindly login again')

                            }

                        }

                    ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

                } else if (data.type === 'PAYPAL') {

                    dispatch({ type: MAKE_PAYMENT_PAYPAL });

                    axios.post(apiIp + '/orders/create', data, { headers: headers }).then(

                        response => {

                            if (JSON.stringify(response.data.status_code) == 500) {

                                PaymentFail(dispatch, 'Failed: Kindly try again later')

                            } else if(JSON.stringify(response.data.status_code) == 200) {

                                const payOrder = {

                                    person_id: data.person_id,

                                    amount: data.totalFoodPrice,

                                    restaurant_id: data.restaurant_id,

                                    order_id: response.data.data.order.id

                                }

                                axios.post(apiIp + '/paypal/completePayPalTransaction/', payOrder, { headers: headers }).then(

                                    pay => {

                                        // log user who has logged in to a particular restaurant
                                        if (JSON.stringify(pay.data.status_code) == 500) {

                                            PaymentFail(dispatch, 'Failed: Kindly try again later')

                                        } else if(SON.stringify(pay.data.status_code) == 200) {

                                            PaymentSuccess(dispatch, data, headers)

                                            Actions.raterestaurant({ orderData: data })

                                        }else if(JSON.stringify(response.data.status_code) == 401){

                                            PaymentFail(dispatch, 'Session Expired: Kindly login again')
            
                                        }

                                    }

                                ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

                            }else if(JSON.stringify(response.data.status_code) == 401){

                                PaymentFail(dispatch, 'Session Expired: Kindly login again')

                            }

                        }

                    ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

                }


            } else {

                PaymentFail(dispatch, 'Failed: Kindly try again later')

            }

        })

    }

}

// function for initiating a redux change when payment has been unsuccessful
const PaymentFail = (dispatch, error) => {

    dispatch({

        type: PAYMENT_FAIL,

        error: error

    })

}

// function for initiating a redux change when payment has been successful
const PaymentSuccess = (dispatch, data, headers) => {

    dispatch({

        type: PAYMENT_SUCCESS,

    })

    // send push notification for successful order of food
    AsyncStorage.getItem('restaurant_name').then((details) => {

        const name = JSON.parse(details)

        const sendPush = {

            person_id: data.person_id,

            body: 'You have a made an order at ' + name + ' of KSH ' + data.totalFoodPrice

        }

        const logData = {

            person_id: data.person_id,

            restaurant_id: data.restaurant_id

        }

        axios.post(apiIp + '/person/sendPushNotification/', sendPush, { headers: headers }).then(

            response => {

                // log user who has logged in to a particular restaurant
                axios.post(apiIp + '/analytics/logVisit/', logData, { headers: headers }).then(

                    res => {

                        console.log(res)

                    }

                ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

            }

        ).catch(() => PaymentFail(dispatch, 'Failed: Kindly try again later'))

    })
}