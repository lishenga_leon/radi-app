import {

    MENU_GOTTEN, FOODS_FAIL, CHECKED_FOOD,

    NUMBER_OF_PEOPLE, FOODS_SUCCESS, FOODS_CHOOSEN

} from './types';

import axios from 'axios';

import { apiIp } from '../Config';

import { StyleSheet } from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';


// function to dispatch menu has been gotten successfully
export const menuGottenSuccessfully = (data) => {

    return {

        type: MENU_GOTTEN,

        menu: data

    }

}

// function for selecting a menu and the dispatching the foods under that menu using redux
export const selectMenu = (data) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            axios.post(apiIp + '/food/MenuFoods/', data, { headers: headers }).then(

                result => {

                    if (JSON.stringify(result.data.status_code) == 500) {

                        FoodsFail(dispatch, 'Failed: Kindly try again later')

                    } else if(JSON.stringify(result.data.status_code) == 200) {

                        AsyncStorage.getItem(data.MenuName).then((token) => {

                            const datas = JSON.parse(token)

                            if (datas && datas.id == data.menu_id) {

                                FoodsSuccess(dispatch, datas.foods, data.menu_id)

                            } else {

                                const items = result.data.data.map(item => {

                                    item.isSelect = false;

                                    item.selectedClass = styles.CheckBoxStyle;

                                    return item;

                                });

                                FoodsSuccess(dispatch, items, data.menu_id)

                            }

                        })

                    }else if(JSON.stringify(result.data.status_code) == 401){

                        FoodsFail(dispatch, 'Session Expired: Kindly login again')

                    }

                }

            ).catch(() => FoodsFail(dispatch, 'Failed: Kindly try again later'))

        })
    }

}

// functio to dispatch failure to get food for a certain menu
const FoodsFail = (dispatch, error) => {

    dispatch({

        type: FOODS_FAIL,

        error: error

    })

}

//function to dispatch success on getting food for a certain menu
const FoodsSuccess = (dispatch, response, menu_id) => {

    dispatch({

        type: FOODS_SUCCESS,

        payload: response,

        menu_id: menu_id

    })

}

// function to select specific food
export const selectFoods = (foods) => {

    return {

        type: FOODS_CHOOSEN,

        foods: foods

    }

}

// function to choose food
export const checkedFood = (foodId) => {

    return {

        type: CHECKED_FOOD,

        payload: foodId

    };

};

// function for number of people
export const numberOfPeople = (number) => {

    return {

        type: NUMBER_OF_PEOPLE,

        payload: number

    };

};

const styles = StyleSheet.create({

    CheckBoxStyle: {

        marginTop: 30,

        fontSize: 20

    },

    selected: { backgroundColor: "#FA7B5F" }

});
