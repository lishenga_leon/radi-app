import {

    FOODS_CHOOSEN_FAIL, FOODS_CHOOSEN_SUCCESS, ORDER_SUCCESS, ORDER_FAIL,

    ADD_PEOPLE_EATING, MINUS_PEOPLE_EATING, MAKE_AN_ORDER, ORDER_INSTRUCTIONS

} from './types';

import AsyncStorage from '@react-native-community/async-storage';

import { Actions } from 'react-native-router-flux';


// function for getting the foods chosen by user
export const getFoodsChoosen = () => {

    return (dispatch) => {

        const choosen = []

        const total = []

        AsyncStorage.getItem("Menus").then(token => {

            const data = JSON.parse(token)

            for (let i in data) {

                AsyncStorage.getItem("PeopleEating" + data[i].id).then(token => {

                    const food = JSON.parse(token)

                    for (let d in food) {

                        for (let s in food[d]) {

                            if (!food[d][s].isSelect) continue;

                            const final = {

                                'food_id': food[d][s].id,

                                'name': food[d][s].name,

                                'price': food[d][s].price,

                                'isSelect': food[d][s].isSelect,

                                'quantity': 1

                            }

                            total.push(food[d][s].price)

                            // Save a specific food details used in calculation of total price
                            AsyncStorage.setItem('FoodItem' + food[d][s].id, JSON.stringify(final))

                            choosen.push(final)

                        }

                        if (choosen.length) {

                            const TotalFoodPrice = total.reduce((a, b) => a + b, 0)

                            FoodsChoosenSuccess(dispatch, choosen, TotalFoodPrice)

                        } else {

                            FoodsChoosenSuccess(dispatch, 0)

                        }

                    }

                }).catch(() => FoodsChoosenFail(dispatch, 0))

            }

        }).catch(() => FoodsChoosenFail(dispatch, 0))

    }

}

// Private function for dispatching a foods choosen failed request 
const FoodsChoosenFail = (dispatch, totalFoodPrice) => {

    dispatch({

        type: FOODS_CHOOSEN_FAIL,

        totalFoodPrice: totalFoodPrice

    })

}

// Private function for dispatching a foods choosen success request
const FoodsChoosenSuccess = (dispatch, response, totalFoodPrice) => {

    dispatch({

        type: FOODS_CHOOSEN_SUCCESS,

        payload: response,

        totalFoodPrice: totalFoodPrice

    })

}

// Function for storing 
storePassword = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}

// function for dispatching redux code for adding the number of people eating
export const addNumberPeople = (foods, food_id, totalFoodPrice) => {

    return {

        type: ADD_PEOPLE_EATING,

        payload: foods,

        food_id: food_id,

        totalFoodPrice: totalFoodPrice

    }

}

// function for dispatching redux code for passing the instructions of an order
export const orderInstructions = (text) => {

    return {

        type: ORDER_INSTRUCTIONS,

        payload: text,

    }

} 

// function for going to payments page or user not logged in page
export const nextPage = (foodsChoosen, totalFoodPrice, restaurant_id, image, bookingData, instructions) => {

    return (dispatch) => {

        AsyncStorage.getItem('user').then((user) => {

            if (user) {

                if (foodsChoosen) {

                    dispatch({ type: MAKE_AN_ORDER });

                    const data = {

                        restaurant_id: restaurant_id,

                        person_id: JSON.parse(user).data.id,

                        email: JSON.parse(user).data.email,

                        date_card_added: JSON.parse(user).data.date_card_added,

                        fullname: JSON.parse(user).data.fullname,

                        foods: foodsChoosen,

                        totalFoodPrice: totalFoodPrice,

                        msisdn: JSON.parse(user).data.msisdn,

                        restaurant_image: image,

                        bookingData: bookingData,

                        order_type: bookingData.order_type,

                        expected_time: bookingData.datetimeBooking,

                        instructions: instructions

                    }
                    OrderSuccess(dispatch)

                    Actions.payments({ orderData: data })

                } else {

                    dispatch({ type: MAKE_AN_ORDER });

                    const data = {

                        restaurant_id: restaurant_id,

                        person_id: JSON.parse(user).data.id,

                        email: JSON.parse(user).data.email,

                        date_card_added: JSON.parse(user).data.date_card_added,

                        fullname: JSON.parse(user).data.fullname,

                        msisdn: JSON.parse(user).data.msisdn,

                        totalFoodPrice: '',

                        foods: [],

                        bookingData: bookingData,

                        restaurant_image: image,

                        order_type: bookingData.order_type,

                        expected_time: bookingData.datetimeBooking,

                        instructions: instructions

                    }

                    OrderSuccess(dispatch)

                    Actions.payments({ orderData: data })

                }

            } else {

                if (foodsChoosen) {

                    dispatch({ type: MAKE_AN_ORDER });

                    const data = {

                        restaurant_id: restaurant_id,

                        foods: foodsChoosen,

                        totalFoodPrice: totalFoodPrice,

                        msisdn: '',

                        bookingData: bookingData,

                        restaurant_image: image,

                        order_type: bookingData.order_type,

                        expected_time: bookingData.datetimeBooking,

                        instructions: instructions

                    }

                    OrderSuccess(dispatch)

                    Actions.loginregister({ orderData: data })

                } else {

                    dispatch({ type: MAKE_AN_ORDER });

                    const data = {

                        restaurant_id: restaurant_id,

                        msisdn: '',

                        foods: [],

                        bookingData: bookingData,

                        totalFoodPrice: '',

                        restaurant_image: image,

                        oorder_type: bookingData.order_type,

                        expected_time: bookingData.datetimeBooking,

                        instructions: instructions

                    }

                    OrderSuccess(dispatch)

                    Actions.loginregister({ orderData: data })

                }

            }

        }).catch(() => OrderFail(dispatch))

    }

}

// function for initiating a redux change when an order has been unsuccessful
const OrderFail = (dispatch) => {

    dispatch({

        type: ORDER_FAIL,

    })

}

// function for initiating a redux change when an order has been successful
const OrderSuccess = (dispatch) => {

    dispatch({

        type: ORDER_SUCCESS,

    })

}

// function for dispatching redux code for subtracting the number of people eating
export const minusNumberPeople = (foods, food_id, totalFoodPrice) => {

    return {

        type: MINUS_PEOPLE_EATING,

        payload: foods,

        food_id: food_id,

        totalFoodPrice: totalFoodPrice

    }

}