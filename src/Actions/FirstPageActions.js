import { POSTS_SUCCESS, POSTS_FAIL, REGISTER_DEVICE_ID_FAIL, REGISTER_DEVICE_ID_SUCCESS } from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import { StyleSheet, Platform, Dimensions } from 'react-native';

import ExtraDimensions from 'react-native-extra-dimensions-android';

import AsyncStorage from '@react-native-community/async-storage';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"

    ? Dimensions.get("window").height

    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"

    ? Dimensions.get("window").width

    : ExtraDimensions.getRealWindowWidth();


const PHONE_HEIGHT_ANDROID = ExtraDimensions.getRealWindowHeight()

const PHONE_HEIGHT_IOS = Dimensions.get("window").height


const PHONE_WIDTH_ANDROID = ExtraDimensions.getRealWindowWidth()

const PHONE_WIDTH_IOS = Dimensions.get("window").width


// function get posts to display on first page
const getFirstPagePosts = (token, dispatch) => {

    let headers = {

        accept: "application/json",

        "Accept-Language": "en-US,en;q=0.8",

        'Content-Type': 'application/json',

        "token": token

    }

    const data = {

        page: 1,

        items: 10000,

    }

    axios.post(apiIp + '/post/displayPostsSorted', data, { headers: headers }).then(

        response => {

            if (JSON.stringify(response.data.status_code) == 500) {

                PostsFail(dispatch)

            } else {

                const items = response.data.data.map(item => {

                    item.isSelect = false;

                    item.selectedClass = styles.likeIconStyle;

                    return item;

                });

                PostsSuccess(dispatch, items)

            }

        }

    ).catch((error) => console.log(error))

};


// function to register device ID for someone who has skipped loggin or create account
export const skipLogInCreateAccount = (deviceId) => {

    return (dispatch) => {

        const data = {

            deviceId: deviceId

        }

        axios.post(apiIp + '/person/registerDeviceIdForLoggin', data).then(

            response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    registerDeviceIdFail(dispatch)

                } else {

                    // store token for device authentication
                    storeData('token', response.headers.token)

                    getFirstPagePosts(response.headers.token, dispatch)

                    registerDeviceIdSuccess(dispatch)

                }

            }

        ).catch(() => registerDeviceIdFail(dispatch))

    }

};

// function for storing the details of the user
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}


const registerDeviceIdFail = (dispatch) => {

    dispatch({

        type: REGISTER_DEVICE_ID_FAIL,

    })

}


const registerDeviceIdSuccess = (dispatch) => {

    dispatch({

        type: REGISTER_DEVICE_ID_SUCCESS,

    })

}


const PostsFail = (dispatch) => {

    dispatch({

        type: POSTS_FAIL,

    })

}

const PostsSuccess = (dispatch, response) => {

    dispatch({

        type: POSTS_SUCCESS,

        payload: response,

    })

}

const styles = StyleSheet.create({

    likeIconStyle: {

        marginTop: -5,

        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,

        width: PHONE_WIDTH / 15,

    },

});