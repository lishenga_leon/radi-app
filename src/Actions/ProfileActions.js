import {

    GET_PROFILE_PIC, PROFILE_EMAIL_CHANGED, PROFILE_FULL_NAME_CHANGED, PROFILE_PASSWORD_CHANGED, USER_ID, TOKEN,

    PROFILE_PHONE_NUMBER_CHANGED, PROFILE_UPDATE_USER, PROFILE_UPDATE_USER_FAIL, PROFILE_UPDATE_USER_SUCCESS,

} from './types';

import axios from 'axios';

import AsyncStorage from '@react-native-community/async-storage';

import { apiIp } from '../Config';



// function for getting user profile picture using redux and axios
export const getUserProfPic = (profpic) => {

    return {

        type: GET_PROFILE_PIC,

        payload: profpic

    }

}

// function for getting user ID using redux and axios
export const profileUserId = (userId) => {

    return {

        type: USER_ID,

        payload: userId

    }

}


// function for get user token using redux
export const getUpdateToken = (text) => {

    return {

        type: TOKEN,

        payload: text

    }

}


// function for updating user name fullname via redux
export const profileFullnameChanged = (text) => {

    return {

        type: PROFILE_FULL_NAME_CHANGED,

        payload: text

    }

}


// function for changing password using redux
export const profilePassChanged = (text) => {

    return {

        type: PROFILE_PASSWORD_CHANGED,

        payload: text

    }

}


// function for changing email using redux
export const profileEmailChanged = (text) => {

    return {

        type: PROFILE_EMAIL_CHANGED,

        payload: text

    }

}


// function for changing phone number using redux
export const profilePhoneNumberChanged = (text) => {

    return {

        type: PROFILE_PHONE_NUMBER_CHANGED,

        payload: text

    }

}


// function for updating user using axios and redux
export const updateUser = (data) => {

    return (dispatch) => {

        AsyncStorage.getItem("token").then(token => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            dispatch({ type: PROFILE_UPDATE_USER });

            axios.post(apiIp + '/person/updateperson/', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        updateUserFail(dispatch, 'Failed: Kindly try again later')

                    } else if(JSON.stringify(response.data.status_code) == 200) {

                        updateUserSuccess(dispatch, data, headers)

                    }else if(JSON.stringify(response.data.status_code) == 401){

                        updateUserFail(dispatch, 'Session Expired: Kindly login again')

                    }

                }

            ).catch(() => updateUserFail(dispatch, 'Failed: Kindly try again later'))

        })

    }

}


// function for initiating a redux change when user has not been updated
const updateUserFail = (dispatch, error) => {

    dispatch({

        type: PROFILE_UPDATE_USER_FAIL,

        error: error

    })

}


// function for initiating a redux change when user has been updated
const updateUserSuccess = (dispatch, data) => {

    dispatch({

        type: PROFILE_UPDATE_USER_SUCCESS,

        payload: data

    })

    const datas = {

        email: data.email,

        password: data.password

    }

    axios.post(apiIp + '/person/email_login/', datas).then(

        response => {

            if (JSON.stringify(response.data.status_code) == 500) {

                alert(JSON.stringify(response.data))

            } else {

                storePassword('password', data.password)

                storeData('user', JSON.stringify(response.data))

                storeData('token', response.headers.token)

            }

        }

    )

}


// function for storing the details of the user
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}

// Function for storing user password
storePassword = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }
    
}
