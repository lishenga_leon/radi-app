import { 

    FORGOT_EMAIL_ADDRESS, FORGOT_PASSWORD, 

    FORGOT_PASSWORD_FAIL, FORGOT_PASSWORD_SUCCESS 

} from './types';

import axios from 'axios';

import { apiIp } from '../Config';


// function for changing the text for the email address input
export const forgotEmailAddressChange = (text) =>{

    return{

        type: FORGOT_EMAIL_ADDRESS,

        payload: text

    }

}


// function for requesting new password for user
export const forgotLogic = ( data ) => {

    return (dispatch) => {

        dispatch({ type: FORGOT_PASSWORD });

        axios.post(apiIp + '/person/sendEmailForgotPassword', data).then(

            response => {

                if(JSON.stringify(response.data.status_code) == 500){

                    forgotFail( dispatch )

                }else{

                    forgotSuccess( dispatch )

                }

            }

        ).catch(() => forgotFail(dispatch) )

    }

}

// function for initiating a redux change when the function forgotLogic has failed
const forgotFail = ( dispatch ) =>{

    dispatch({

        type: FORGOT_PASSWORD_FAIL, 

    })

}

// ffunction for initiating a redux change when the function forgotLogic has succeeded
const forgotSuccess = ( dispatch ) => {

    dispatch({

        type: FORGOT_PASSWORD_SUCCESS, 

    })
    
}
