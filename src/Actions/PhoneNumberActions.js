import { GET_PHONE_NUMBER, GET_PASSWORD_GOOGLE_LOGIN } from './types';


// function for changing the phone number text using redux
export const getPhoneNumberCompoChange = (text) => {

    return{

        type: GET_PHONE_NUMBER,

        payload: text

    }

}

// function for changing the password text using redux
export const getPasswordCompoChange = (text) => {

    return{

        type: GET_PASSWORD_GOOGLE_LOGIN,

        payload: text

    }
    
}