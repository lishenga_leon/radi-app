export const API_IP = 'http://142.93.7.234:84/api/'
export const TOAST_DISPLAY= 'toast_display'
export const TOAST_CLEAR= 'toast_clear'
export const SPINNER_CLEAR= 'spinner_clear'
export const ERROR_CLEAR= 'error_clear'
export const EMAIL_CLEAR= 'email_clear'
export const INCORRECT_PASSWORD='incorrect_password'
export const TRY_AGAIN='try_again'
export const GETTING_USER_DETAILS_ASYNCSTORAGE = 'getting_user_details_from_ayncstorage'
export const PAGINATION = 'add_them_up'
export const USER_ID = 'id_of_user'
export const TOKEN = 'user_token'
export const CREATE_USER_GOOGLE_FACEBOOK = 'user_created_google_or_facebook'
export const LOGIN_USER_GOOGLE_FACEBOOK = 'login_user_with_facebook_google'
export const REGISTER_USER_PUSH_NOTIFICATION = 'register_user_device_pushNotification'
export const REGISTER_USER_PUSH_NOTIFICATION_SUCCESS = 'register_user_device_pushNotification_success'
export const REGISTER_USER_PUSH_NOTIFICATION_FAIL = 'register_user_device_pushNotification_fail'
export const USER_DEVICE_UID = 'user_device_uid'
export const CALCULATE_DISTANCE = 'calculate_distance'
export const RESET_USER_LOCATION = 'reset_user_location'


//Login Component
export const USERNAME_CHANGED = 'username_changed'
export const PASSWORD_CHANGED = 'password_changed'
export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAIL = 'login_user_fail'
export const LOGIN_USER = 'login_user'


//LoginAfterOrder Component
export const LOGIN_AFTER_ORDER_USER_SUCCESS = 'login_user_after_order_success'
export const LOGIN_AFTER_ORDER_USER_FAIL = 'login_user_after_order_fail'
export const LOGIN_AFTER_ORDER_USER = 'login_after_order_user'


//Create Component
export const CREATE_USER_SUCCESS = 'create_user_success'
export const CREATE_USER_FAIL = 'create_user_fail'
export const CREATE_USER = 'create_user'
export const FULL_NAME_CHANGED = 'first_name_changed'
export const EMAIL_CHANGED = 'email_changed'
export const PHONE_NUMBER_CHANGED = 'phone_number_changed'


//CreateAfterOrder Component
export const CREATE_AFTER_ORDER_USER_SUCCESS = 'create_user_after_order_success'
export const CREATE_AFTER_ORDER_USER_FAIL = 'create_user_after_order_fail'
export const CREATE_AFTER_ORDER_USER= 'create_after_order_user'
export const FULL_NAME_AFTER_ORDER_CHANGED = 'first_name_after_order_changed'
export const EMAIL_AFTER_ORDER_CHANGED = 'email_after_order_changed'
export const PHONE_NUMBER_AFTER_ORDER_CHANGED = 'phone_number_after_order_changed'
export const PASSWORD_AFTER_ORDER_CHANGED = 'password_after_order_changed'


//AfterCreate Component
export const COMPONENT_MOUNTED= 'component_mounted'
export const AFTER_CREATE_PASSWORD_CHANGED = 'password_changed'

//Landing Component
export const USER_NAMES= 'user_names'

//Forgot Component
export const FORGOT_USER_SUCCESS= 'forgot_user_success'
export const FORGOT_USER_FAIL= 'forgot_user_fail'
export const FORGOT_USER= 'forgot_user'

//Posts Component
export const POSTS_SUCCESS = 'post_success'
export const POSTS_FAIL = 'posts_fail'
export const SHOW_UPLOAD_CAMERA_ICON = 'show_upload_camera' 
export const DATE_TIMER_BOOKING_TIMER = 'datetimebookingtimer' 
export const ISREFRESH_STATE = 'set_isrefresh_state' 
export const LOADING_STATE = 'set_loading_state'
export const NEXT_PAGE = 'next_page'
export const POSTS_REFRESH = 'posts_refresh_page'
export const REGISTER_DEVICE_ID_FAIL = 'failed_to_register_device_id'
export const REGISTER_DEVICE_ID_SUCCESS = 'success_to_register_device_id'

//Index Home Component
export const SEARCH_MAP = 'search_map'
export const GET_PICTURE = 'get_picture'


//UploadPost component
export const UPLOAD_PICTURE_POST_SUCCESS = 'success'
export const RESTAURANT_NAME_CHANGE = 'restaurant_name'
export const RESTAURANT_NAME_CHANGE_SUCCESS = 'success_for_restaurant'
export const RESTAURANT_NAME_CHANGE_FAIL = 'fail_for_restaurant'
export const POST_CHANGE = 'success_for_post'
export const UPLOAD_POST = 'success_for_upload'
export const LIKE_SUCCESS = 'success_like'
export const LIKE_FAIL = 'fail_like'
export const PICK_RESTAURANT = 'pick_a_restaurant'
export const UPLOAD_PICTURE_POST_ERROR = 'error_uploading_post'

//Joint Component
export const JOINT_DETAILS_SUCCESS = 'get_all_joint_details'
export const JOINT_DETAILS_FAIL = 'get_all_details_fail'

//JointPage component
export const GET_MENU_SUCCESS = 'menu_gotten_successfully'
export const MENU_FAIL = 'menu_fail'
export const SHOW_OFFERS_MODAL = 'show_offers_modal'

//List Menu component
export const MENU_GOTTEN = 'flatlist_looped_successfully'
export const SELECT_MENU = 'select_menu'
export const CHECKED_FOOD = 'food_selected'
export const NUMBER_OF_PEOPLE = 'number_of_people_eating'


//Menu component
export const FOODS_SUCCESS = 'foods_success'
export const FOODS_FAIL = 'foods_fail'
export const FOODS_CHOOSEN = 'choosen_foods'


//Order component
export const FOODS_CHOOSEN_SUCCESS = 'foods_choosen_success'
export const FOODS_CHOOSEN_FAIL = 'foods_choosen_fail'
export const MINUS_PEOPLE_EATING = 'minus_number_of_people_eating'
export const ADD_PEOPLE_EATING = 'add_number_of_people_eating'
export const MAKE_AN_ORDER = 'make_an_order'
export const ORDER_SUCCESS = 'order_successful'
export const ORDER_FAIL = 'order_fail'
export const ORDER_INSTRUCTIONS = 'instructions_for_order'


//Bookings component
export const NUMBER_OF_SEATS_CHANGED = 'number_of_seats_changed'
export const DATE_TIME_FOR_BOOKING_CHANGED = 'datetime_changed_for_booking'
export const DATETIME_VISIBLE_MODAL = 'datetime_visible_modal'
export const BOOKING_SUCCESS = 'booking_successful'
export const BOOKING_FAIL = 'booking_unsuccessful'
export const MAKE_A_BOOKING = 'make_a_booking'
export const ORDER_TYPE = 'what_type_of_order'


//Payment Component
export const MAKE_PAYMENT_MPESA = 'make_payment_mpesa'
export const MAKE_PAYMENT_CARD = 'make_payment_card'
export const MAKE_PAYMENT_PAYPAL = 'make_payment_paypal'
export const PAYMENT_SUCCESS = 'payment_success'
export const PAYMENT_FAIL = 'payment_fail'


//Map Component
export const GET_PERSON_LOCATION = 'user_location_coordinates'
export const GET_ALL_RESTAURANTS_FAIL = 'get_all_restaurants_fail'
export const MAP_JOINT_DETAILS_SUCCESS = 'map_get_all_joint_details'
export const MAP_JOINT_DETAILS_FAIL = 'map_get_all_details_fail'
export const SEARCH_MAP_RESTAURANT = 'search_restaurant_map'
export const SEARCH_MAP_RESTAURANT_SUCCESS = 'restaurant_success'
export const SEARCH_MAP_RESTAURANT_FAIL = 'restaurant_fail'


//Thirsty Map Component
export const GET_PERSON_THIRSTY_LOCATION = 'user_thirsty_location_coordinates'
export const GET_ALL_THIRSTY_RESTAURANTS_FAIL = 'get_all_thirsty_restaurants_fail'
export const MAP_JOINT_THIRSTY_DETAILS_SUCCESS = 'map_get_all_thirsty_joint_details'
export const MAP_JOINT_THIRSTY_DETAILS_FAIL = 'map_get_all_thirsty_details_fail'
export const SEARCH_MAP_THIRSTY_RESTAURANT = 'search_thirsty_restaurant_map'
export const SEARCH_MAP_THIRSTY_RESTAURANT_SUCCESS = 'restaurant_thirsty_success'
export const SEARCH_MAP_THIRSTY_RESTAURANT_FAIL = 'restaurant_thirsty_fail'


//Hungry Map Component
export const GET_PERSON_HUNGRY_LOCATION = 'user_hungry_location_coordinates'
export const GET_ALL_HUNGRY_RESTAURANTS_FAIL = 'get_all_hungry_restaurants_fail'
export const MAP_JOINT_HUNGRY_DETAILS_SUCCESS = 'map_get_all_hungry_joint_details'
export const MAP_JOINT_HUNGRY_DETAILS_FAIL = 'map_get_all_hungry_details_fail'
export const SEARCH_MAP_HUNGRY_RESTAURANT = 'search_hungry_restaurant_map'
export const SEARCH_MAP_HUNGRY_RESTAURANT_SUCCESS = 'restaurant_hungry_success'
export const SEARCH_MAP_HUNGRY_RESTAURANT_FAIL = 'restaurant_hungry_fail'


//Outandabout Map Component
export const GET_PERSON_OUTANDABOUT_LOCATION = 'user_outandabout_location_coordinates'
export const GET_ALL_OUTANDABOUT_RESTAURANTS_FAIL = 'get_all_outandabout_restaurants_fail'
export const MAP_JOINT_OUTANDABOUT_DETAILS_SUCCESS = 'map_get_all_outandabout_joint_details'
export const MAP_JOINT_OUTANDABOUT_DETAILS_FAIL = 'map_get_all_outandabout_details_fail'
export const SEARCH_MAP_OUTANDABOUT_RESTAURANT = 'search_outandabout_restaurant_map'
export const SEARCH_MAP_OUTANDABOUT_RESTAURANT_SUCCESS = 'restaurant_outandabout_success'
export const SEARCH_MAP_OUTANDABOUT_RESTAURANT_FAIL = 'restaurant_outandabout_fail'


//Phone Number Component
export const GET_PHONE_NUMBER = 'user_phone_number'
export const GET_PASSWORD_GOOGLE_LOGIN = 'user_password'


//Phone Number After Order component
export const GET_PHONE_NUMBER_AFTER_ORDER = 'user_phone_number_after_order'
export const GET_PASSWORD_AFTER_ORDER_GOOGLE_LOGIN = 'user_password_after_order'


//Profile Component
export const GET_PROFILE_PIC = 'profile_get_user_profile_pic'
export const PROFILE_UPDATE_USER_SUCCESS = 'profile_update_user_success'
export const PROFILE_UPDATE_USER_FAIL = 'profile_update_user_fail'
export const PROFILE_UPDATE_USER = 'profile_update_user'
export const PROFILE_FULL_NAME_CHANGED = 'profile_first_name_changed'
export const PROFILE_EMAIL_CHANGED = 'profile_email_changed'
export const PROFILE_PHONE_NUMBER_CHANGED = 'profile_phone_number_changed'
export const PROFILE_PASSWORD_CHANGED = 'profile_password_changed'


// RateRestaurant Component
export const RATE_RESTAURANT = 'rate_restaurant'
export const START_RATE_RESTAURANT = 'start_rating_restaurant' 
export const RATE_RESTAURANT_SUCCESS = 'success_rate_restaurant'
export const RATE_RESTAURANT_FAIL = 'fail_rate_restaurant' 


// Person Orders Component
export const GET_ALL_ORDERS = 'get_all_orders_of_a_person'
export const GET_ALL_ORDERS_FAIL = 'get_all_orders_of_a_person_fail'


// Forgot Component
export const FORGOT_EMAIL_ADDRESS = 'user_email_address'
export const FORGOT_PASSWORD = 'forgot_password'
export const FORGOT_PASSWORD_SUCCESS = 'forgot_password_success'
export const FORGOT_PASSWORD_FAIL = 'forgot_password_fail'

// Favorites Component
export const FAVORITE_RESTAURANTS = 'five_favorite_restaurants'
export const FAVORITE_RESTAURANTS_FAIL = 'failed_getting_five_best_restaurants'

// Transactions Component
export const TRANSACTIONS_SUCCESS = 'transactions_success'
export const TRANSACTIONS_FAIL = 'transactions_fail'

// Add Card Component
export const ADD_CARD_NUMBER = 'add_card_number'
export const ADD_CARD_YEAR = 'add_card_year' 
export const ADD_CARD_MONTH = 'add_card_month'
export const ADD_CARD_CVC = 'add_card_cvc' 
export const REGISTER_CARD = 'add_card_of_a_user' 
export const REGISTER_CARD_SUCCESS = 'success_adding_card' 
export const REGISTER_CARD_FAIL = 'fail_adding_card'


// Edit Pay Component 
export const EDIT_CARD = 'edit_card_of_a_user' 
export const EDIT_CARD_SUCCESS = 'success_editing_card' 
export const EDIT_CARD_FAIL = 'fail_editing_card'