import {

    PASSWORD_AFTER_ORDER_CHANGED, EMAIL_AFTER_ORDER_CHANGED, CREATE_AFTER_ORDER_USER, REGISTER_USER_PUSH_NOTIFICATION_SUCCESS,

    CREATE_AFTER_ORDER_USER_FAIL, CREATE_AFTER_ORDER_USER_SUCCESS, CREATE_USER_GOOGLE_FACEBOOK, REGISTER_USER_PUSH_NOTIFICATION_FAIL,

    PHONE_NUMBER_AFTER_ORDER_CHANGED, FULL_NAME_AFTER_ORDER_CHANGED, CREATE_USER, REGISTER_USER_PUSH_NOTIFICATION, USER_DEVICE_UID

} from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import { Actions } from 'react-native-router-flux';

import AsyncStorage from '@react-native-community/async-storage';


// function for changing full name using redux
export const fullnameAfterOrderChanged = (text) => {

    return {

        type: FULL_NAME_AFTER_ORDER_CHANGED,

        payload: text

    }

}

// function for getting user device uid
export const getUserDeviceAfterOrderUid = (text) => {

    return {

        type: USER_DEVICE_UID,

        payload: text

    }

}

// function for changing password using redux
export const passAfterOrderChanged = (text) => {

    return {

        type: PASSWORD_AFTER_ORDER_CHANGED,

        payload: text

    }

}

// function for changing email using redux
export const emailAfterOrderChanged = (text) => {

    return {

        type: EMAIL_AFTER_ORDER_CHANGED,

        payload: text

    }

}

// function for changing phone number using redux
export const phoneNumberAfterOrderChanged = (text) => {

    return {

        type: PHONE_NUMBER_AFTER_ORDER_CHANGED,

        payload: text

    }

}

// function for checking if a user is logged in via google or not
export const createAfterOrderGoogleSignIn = (data, orderData) => {

    return (dispatch) => {

        dispatch({ type: CREATE_USER });

        axios.post(apiIp + '/person/googleFacebookSignIn/', data).then(

            response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    createUserFacebookGoogle(dispatch)

                    Actions.phonenumberafterorder({ userDetails: data, orderData: orderData })

                } else if (JSON.stringify(response.data.status_code) == 200) {

                    loginUserFacebookGoogle(dispatch, data, orderData)

                }

            }

        ).catch(() => createUserFail(dispatch))

    }

}

// function for creating user using axios and redux
export const createAfterOrderUser = (data, orderData) => {

    return (dispatch) => {

        dispatch({ type: CREATE_AFTER_ORDER_USER });

        axios.post(apiIp + '/person/createperson/', data).then(

            response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    createUserFail(dispatch)

                } else {

                    createUserAfterOrderSuccess(dispatch, data, orderData)

                }

            }

        ).catch(() => createUserFail(dispatch))

    }

}

// register user's device for push notification
const registerPhoneUserAfterOrderPushNotification = (data, dispatch, headers) => {

    dispatch({ type: REGISTER_USER_PUSH_NOTIFICATION });

    axios.post(apiIp + '/person/person_device_uid/', data, { headers: headers }).then(

        response => {

            if (JSON.stringify(response.data.status_code) == 500) {

                registerUserNotificationFail(dispatch, 'Failed: Kindly try again')

            } else if (JSON.stringify(response.data.status_code) == 200) {

                registerUserNotificationSuccess(dispatch)

            } else if (JSON.stringify(response.data.status_code) == 401) {

                registerUserNotificationFail(dispatch, 'Session Expired: Kindly login again')

            }

        }

    ).catch(() => registerUserNotificationFail(dispatch, 'Failed: Kindly try again'))

}

// function for initiating a failed register of user's device for push notification
const registerUserNotificationFail = (dispatch, error) => {

    dispatch({

        type: REGISTER_USER_PUSH_NOTIFICATION_FAIL,

        error: error

    })

}

// function for initiating a successful register of user's device for push notification
const registerUserNotificationSuccess = (dispatch) => {

    dispatch({

        type: REGISTER_USER_PUSH_NOTIFICATION_SUCCESS,

    })

}

// function for initiating a redux change when user has been checked for google or facebook login
const createUserFacebookGoogle = (dispatch) => {

    dispatch({

        type: CREATE_USER_GOOGLE_FACEBOOK,

    })

}

// function for initiating a redux change to take someone to password component 
const loginUserFacebookGoogle = ( dispatch, data, orderData ) => {

    dispatch({

        type: LOGIN_USER_GOOGLE_FACEBOOK,

    })

    Actions.passwordafterorder({ userDetails: data, orderData: orderData })

}

// function for initiating a redux change when user has not been created
const createUserFail = (dispatch) => {

    dispatch({

        type: CREATE_AFTER_ORDER_USER_FAIL,

    })

}

// function for initiating a redux change when user has been created, saving user device uid for push notifications and sending food data to the next component
const createUserAfterOrderSuccess = (dispatch, data, orderData) => {

    dispatch({

        type: CREATE_AFTER_ORDER_USER_SUCCESS,

        payload: data

    })

    const datas = {

        email: data.email,

        password: data.password

    }

    AsyncStorage.getItem('device_uid').then((uid) => {

        axios.post(apiIp + '/person/email_login/', datas).then(

            response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    createUserFail(dispatch)

                } else {

                    // store user password
                    storePassword('password', data.password)

                    // store user details
                    storeData('user', JSON.stringify(response.data))

                    // store User token
                    storeData('token', response.headers.token)


                    // push notification function
                    const details = {

                        person_id: response.data.data.id,

                        device_uid: uid,

                    }

                    let headers = {

                        accept: "application/json",

                        "Accept-Language": "en-US,en;q=0.8",

                        'Content-Type': 'application/json',

                        "token": response.headers.token

                    }

                    registerPhoneUserAfterOrderPushNotification(details, dispatch, headers);

                    if (orderData.foods.length) {

                        AsyncStorage.getItem('restaurant_name').then((data) => {

                            const foodDetails = {

                                restaurant_id: orderData.restaurant_id,

                                foods: orderData.foods,

                                totalFoodPrice: orderData.totalFoodPrice,

                                msisdn: response.data.data.msisdn,

                                person_id: response.data.data.id,

                                email: response.data.data.email,

                                date_card_added: response.data.data.date_card_added,

                                restaurant_image: orderData.restaurant_image,

                                restaurant_name: JSON.parse(data),

                                order_type: orderData.order_type,

                                expected_time: orderData.expected_time,

                                instructions: orderData.instructions

                            }

                            Actions.payments({ orderData: foodDetails })

                        })

                    } else {

                        Actions.drawer()

                    }

                }

            }

        )

    })

}

// function for storing the details of the user
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}

// Function for storing user password
storePassword = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}