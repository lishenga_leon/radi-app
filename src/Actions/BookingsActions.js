import { 
    
    NUMBER_OF_SEATS_CHANGED, DATE_TIME_FOR_BOOKING_CHANGED, ORDER_TYPE,

    DATETIME_VISIBLE_MODAL, BOOKING_FAIL, BOOKING_SUCCESS, MAKE_A_BOOKING 

} from './types';

import { Actions } from 'react-native-router-flux';

// function for changing number of seats for a booking using redux
export const numberOfSeatsChanged = (text) => {

    return{

        type: NUMBER_OF_SEATS_CHANGED,

        payload: text

    }

}

// function for changing number of seats for a booking using redux
export const typeOfOrder = (text) => {

    return{

        type: ORDER_TYPE,

        payload: text

    }

}

// function for changing date of booking using redux
export const dateTimeChangedBooking = (text) => {

    return {

        type: DATE_TIME_FOR_BOOKING_CHANGED,

        payload: text

    }

}

// function for showing if the datetime modal is visible using redux
export const dateTimeVisibleModal = () => {

    return {

        type: DATETIME_VISIBLE_MODAL,

    }

}

// function for making a booking using axios and redux
export const makeABooking = ( data ) => {

    return (dispatch) => {

        dispatch({ type: MAKE_A_BOOKING });

        if(data.order_type !== undefined && data.datetimeBooking !== undefined){

            bookingSuccess(dispatch, data)

        }else{

            bookingFail(dispatch, 'Kindly choose the type of order and state the time for order to be ready')

        }

    }

}


// function for initiating a redux change when a booking has not been made
const bookingFail = ( dispatch, error ) =>{

    dispatch({

        type: BOOKING_FAIL,

        payload: error

    })

}

// function for initiating a redux change when a booking has been made
const bookingSuccess = ( dispatch, data ) => {

    dispatch({

        type: BOOKING_SUCCESS, 

    })

    Actions.jointpage({ image: data.image, naming: data.naming, restaurant_id: data.restaurant_id, bookingData: data, restaurant_rating: data.rating })
}