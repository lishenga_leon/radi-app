import {

    GET_PERSON_HUNGRY_LOCATION, GET_ALL_HUNGRY_RESTAURANTS_FAIL, SEARCH_MAP_HUNGRY_RESTAURANT, SEARCH_MAP_HUNGRY_RESTAURANT_SUCCESS,

    MAP_JOINT_HUNGRY_DETAILS_FAIL, MAP_JOINT_HUNGRY_DETAILS_SUCCESS, SEARCH_MAP_HUNGRY_RESTAURANT_FAIL

} from './types';

import axios from 'axios';

import { apiIp } from '../Config';

import AsyncStorage from "@react-native-community/async-storage";

import getDistance from 'geolib/es/getDistance';


// search for a specific restaurant on map
export const searchHungryRestaurantMap = (text) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            const data = {

                name: text,

                page: 1,

                size: 10

            }

            dispatch({

                type: SEARCH_MAP_HUNGRY_RESTAURANT,

                payload: text,

            });

            axios.post(apiIp + "/restaurant/searchByName", data, { headers: headers }).then(response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    dispatch({

                        type: SEARCH_MAP_HUNGRY_RESTAURANT_FAIL,

                        error: 'Kindly try again later'

                    });

                } else if(JSON.stringify(response.data.status_code) == 200) {

                    const location = {

                        latitude: response.data.data[0].location.latitude,

                        longitude: response.data.data[0].location.longitude,

                        latitudeDelta: 1,

                        longitudeDelta: 1

                    }

                    dispatch({

                        type: SEARCH_MAP_HUNGRY_RESTAURANT_SUCCESS,

                        restaurants: response.data.data,

                        location: location,

                        total: countNumberOfObjects(response.data.data)

                    });

                }else if(JSON.stringify(response.data.status_code) == 401){

                    dispatch({

                        type: SEARCH_MAP_HUNGRY_RESTAURANT_FAIL,

                        error: 'Session Expired: Kindly login again'

                    });

                }

            }).catch(() => dispatch({

                type: SEARCH_MAP_HUNGRY_RESTAURANT_FAIL, 

                error: 'Failed: Kindly try again later' 

            }))

        })

    }

};


// function for dispatching redux code for getting hungry restaurants
export const getUserHungryLocation = (region) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            const data = {

                latitude: region.latitude,

                longitude: region.longitude,

                radius: 2

            }

            const total = []

            axios.post(apiIp + '/restaurant/searchProximity', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        RestaurantsFail(dispatch, 'Failed: Kindly try again later')

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        AsyncStorage.setItem('UserLocation', JSON.stringify(region))

                        response.data.data.map((s) => {

                            if (s.category == 'HUNGRY') {

                                const final = {

                                    'id': s.id,

                                    'category': s.category,

                                    'description': s.description,

                                    'name': s.name,

                                    'phone_number': s.phone_number,

                                    'location': s.location,

                                    'max_capacity': s.max_capacity,

                                    'created_at': s.created_at,

                                    'updated_at': s.updated_at,

                                    'status': s.status,

                                    'cover_image': s.cover_image

                                }

                                total.push(final)

                            };

                        })

                        dispatch({ type: GET_PERSON_HUNGRY_LOCATION, restaurants: total, total: countNumberOfObjects(total) })

                    } else if(JSON.stringify(response.data.status_code) == 401){

                        RestaurantsFail(dispatch, 'Session Expired: Kindly login again')

                    }

                }

            ).catch(() => RestaurantsFail(dispatch, 'Failed: Kindly try again later'))

        })

    }

}

// function for getting specific restaurants details
export const restaurantGetHungryDetails = (data, coord) => {

    return (dispatch) => {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }

            axios.post(apiIp + '/restaurant/findById/', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        jointDetailsFail(dispatch, coord, 'Failed: Kindly try again later')

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        AsyncStorage.getItem('UserLocation').then((locate) => {

                            const loc = JSON.parse(locate)

                            jointDetailsSuccess(dispatch, response.data.data, coord, getDistance(loc, coord) + ' m')

                        })

                    } else if (JSON.stringify(response.data.status_code) == 401){

                        jointDetailsFail(dispatch, coord, 'Session Expired: Kindly login again')

                    }

                }

            ).catch(() => jointDetailsFail(dispatch, coord, 'Failed: Kindly try again later'))

        })

    }

}

// function to count the number of objects an array
const countNumberOfObjects = (array) => {

    let result = 0;

    for (let prop in array) {

        if (array.hasOwnProperty(prop)) {

            // or Object.prototype.hasOwnProperty.call(obj, prop)

            result++;

        }

    }

    return result;

}

// function for initiating a redux change when joint details hasn't been gotten
const jointDetailsFail = (dispatch, coord, error) => {

    dispatch({

        type: MAP_JOINT_HUNGRY_DETAILS_FAIL,

        coord: coord,

        error: error

    })

}

// function for initiating a redux change when joint details has been gotten
const jointDetailsSuccess = (dispatch, data, coord, distance) => {

    dispatch({

        type: MAP_JOINT_HUNGRY_DETAILS_SUCCESS,

        payload: data,

        coord: coord,

        distance: distance

    })

}

// function for initiating a redux change when get restaurants has been unsuccessful
const RestaurantsFail = (dispatch, error) => {

    dispatch({

        type: GET_ALL_HUNGRY_RESTAURANTS_FAIL,

        error: error

    })

}
