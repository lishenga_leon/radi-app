import { 

    PASSWORD_CHANGED,EMAIL_CHANGED, PHONE_NUMBER_CHANGED, USER_DEVICE_UID, LOGIN_USER_GOOGLE_FACEBOOK,

    FULL_NAME_CHANGED,CREATE_USER, CREATE_USER_GOOGLE_FACEBOOK, REGISTER_USER_PUSH_NOTIFICATION_SUCCESS,

    CREATE_USER_FAIL, CREATE_USER_SUCCESS, REGISTER_USER_PUSH_NOTIFICATION, REGISTER_USER_PUSH_NOTIFICATION_FAIL

} from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import { Actions } from 'react-native-router-flux';

import AsyncStorage from '@react-native-community/async-storage';


// function for changing full name using redux
export const fullnameChanged = (text) => {

    return{

        type: FULL_NAME_CHANGED,

        payload: text

    }

}

// function for getting user device uid
export const getUserDeviceUid= (text) => {

    return{

        type: USER_DEVICE_UID,

        payload: text

    }

}

// function for changing password using redux
export const passChanged = (text) => {

    return {

        type: PASSWORD_CHANGED,

        payload: text

    }

}

// function for changing email using redux
export const emailChanged = (text) => {

    return {

        type: EMAIL_CHANGED,

        payload: text

    }

}

// function for changing phone number using redux
export const phoneNumberChanged = (text) => {

    return {

        type: PHONE_NUMBER_CHANGED,

        payload: text

    }

}

// function for checking if a user is logged in via google or facebook
export const googleFacebookSignIn = ( data ) => {

    return (dispatch) => {

        dispatch({ type: CREATE_USER });

        axios.post(apiIp + '/person/googleFacebookSignIn/', data).then(

            response => {

                if(JSON.stringify(response.data.status_code) == 500){

                    createUserFacebookGoogle( dispatch )

                    Actions.phonenumber({ userDetails: data })

                }else if(JSON.stringify(response.data.status_code) == 200){

                    loginUserFacebookGoogle( dispatch, data )

                }

            }

        ).catch((error) => createUserFail(dispatch) )

    }

}



// function for creating user using axios and redux
export const createUser = ( data ) => {

    return (dispatch) => {

        dispatch({ type: CREATE_USER });

        axios.post(apiIp + '/person/createperson/', data).then(

            response => {
                
                if(JSON.stringify(response.data.status_code) == 500){

                    createUserFail( dispatch )

                }else{

                    createUserSuccess( dispatch, data )

                }

            }

        ).catch(() => createUserFail(dispatch) )

    }

}

// register user's device for push notification
const registerPhoneUserPushNotification = (data, dispatch) =>{

    dispatch({ type: REGISTER_USER_PUSH_NOTIFICATION });

    AsyncStorage.getItem('token').then((token)=>{

        let headers = {
    
          accept: "application/json",
    
          "Accept-Language": "en-US,en;q=0.8",
    
          'Content-Type': 'application/json',
    
          "token": token
    
        }

        axios.post(apiIp + '/person/person_device_uid/', data, { headers: headers }).then(

            response => {

                if(JSON.stringify(response.data.status_code) == 500){

                    registerUserNotificationFail( dispatch, 'Failed: Kindly try again later' )

                }else if(JSON.stringify(response.data.status_code) == 200){

                    registerUserNotificationSuccess( dispatch )

                }else if(JSON.stringify(response.data.status_code) == 401){

                    registerUserNotificationFail( dispatch, 'Session Expired: Kindly login again' )

                }

            }

        ).catch(() => registerUserNotificationFail(dispatch, 'Failed: Kindly try again later') )

    })
}

// function for initiating a failed register of user's device for push notification
const registerUserNotificationFail = ( dispatch, error ) =>{

    dispatch({

        type: REGISTER_USER_PUSH_NOTIFICATION_FAIL,  

        error: error

    })

}

// function for initiating a successful register of user's device for push notification
const registerUserNotificationSuccess = ( dispatch ) =>{

    dispatch({

        type: REGISTER_USER_PUSH_NOTIFICATION_SUCCESS,  

    })

}

// function for initiating a redux change when user has been checked for google login
const createUserFacebookGoogle = ( dispatch ) =>{

    dispatch({

        type: CREATE_USER_GOOGLE_FACEBOOK,  

    })

}

// function for initiating a redux change when user has not been created
const createUserFail = ( dispatch ) =>{

    dispatch({

        type: CREATE_USER_FAIL,  

    })

}


// function for initiating a redux change to take someone to password component 
const loginUserFacebookGoogle = ( dispatch, data ) =>{

    dispatch({

        type: LOGIN_USER_GOOGLE_FACEBOOK,  

    })

    Actions.password({ userDetails: data })

}


// function for initiating a redux change when user has been created
const createUserSuccess = ( dispatch, data ) => {

    dispatch({

        type: CREATE_USER_SUCCESS, 

        payload: data

    })

    const datas = {

        email:data.email.toLowerCase(),

        password: data.password

    }

    AsyncStorage.getItem('device_uid').then((token)=>{

        axios.post(apiIp + '/person/email_login/', datas).then(

            response => {
    
                if(JSON.stringify(response.data.status_code) == 500){
    
                    createUserFail( dispatch )
    
                }else{
    
                    //store password
                    storePassword('password', data.password)
    
                    // store user details
                    storeData('user', JSON.stringify(response.data))
    
    
                    // store user token
                    storeData('token', response.headers.token)
                    
    
                    // push notification function
                    const details = {

                        person_id:response.data.data.id,

                        device_uid: token,

                    }
    
                    registerPhoneUserPushNotification(details, dispatch)
    
                    Actions.reset('main');
                }
            }
        )

    })

}

// function for storing the details of the user
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}

// Function for storing user password
storePassword = async (item, selectedValue)=>{

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}