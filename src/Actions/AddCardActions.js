import { 
    
    ADD_CARD_NUMBER, ADD_CARD_YEAR, ADD_CARD_MONTH, EDIT_CARD, EDIT_CARD_FAIL,
    
    ADD_CARD_CVC, REGISTER_CARD, REGISTER_CARD_SUCCESS, REGISTER_CARD_FAIL, EDIT_CARD_SUCCESS

} from './types';

import { Actions } from 'react-native-router-flux';

import AsyncStorage from '@react-native-community/async-storage';

import axios from 'axios';

import { apiIp } from '../Config';


// function for changing card number using redux
export const addCardNumber = (text) => {

    return{

        type: ADD_CARD_NUMBER,

        payload: text

    }

}

// function for changing card cvc using redux
export const addCardCVC = (text) => {

    return {

        type: ADD_CARD_CVC,

        payload: text

    }

}

// function for changing card month using redux
export const addCardMonth = (text) => {

    return {

        type: ADD_CARD_MONTH,

        payload: text

    }

}

// function for changing card year using redux
export const addCardYear = ( text ) => {

    return {

        type: ADD_CARD_YEAR,

        payload: text

    }

}

// function for registering the card of a user
export const registerCard = (data) => {

    return (dispatch) => {

        dispatch({ type: REGISTER_CARD });

        AsyncStorage.getItem('token').then((token)=>{

            let headers = {
        
              accept: "application/json",
        
              "Accept-Language": "en-US,en;q=0.8",
        
              'Content-Type': 'application/json',
        
              "token": token
        
            }

            axios.post(apiIp + '/stripe/addpersoncard/', data, { headers: headers }).then(

                response => {

                    if(JSON.stringify(response.data.status_code) == 500){

                        addCardFail( dispatch, 'Failed: Kindly try again later' )

                    }else if(JSON.stringify(response.data.status_code) == 200){

                        addCardSuccess( dispatch, data )

                    }else if(JSON.stringify(response.data.status_code) == 401){

                        addCardFail( dispatch, 'Session Expired: Kindly login again' )

                    }

                }

            ).catch(() => addCardFail( dispatch, 'Failed: Kindly try again later' ) )

        })

    }

}


// function for editting the card of a user
export const editPayCard = (data) => {

    return (dispatch) => {

        dispatch({ type: EDIT_CARD });

        AsyncStorage.getItem('token').then((token)=>{

            let headers = {
        
              accept: "application/json",
        
              "Accept-Language": "en-US,en;q=0.8",
        
              'Content-Type': 'application/json',
        
              "token": token
        
            }

            axios.post(apiIp + '/stripe/addpersoncard/', data, { headers: headers }).then(

                response => {

                    if(JSON.stringify(response.data.status_code) == 500){

                        editPayCardFail( dispatch, 'Failed: Kindly try again later' )

                    }else if(JSON.stringify(response.data.status_code) == 200){

                        editPayCardSuccess( dispatch )

                    }else if(JSON.stringify(response.data.status_code) == 401){

                        editPayCardFail( dispatch, 'Session Expired: Kindly login again' )

                    }

                }

            ).catch(() => addCardFail( dispatch, 'Failed: Kindly try again later' ) )

        })

    }

}


// function for initiating a redux change when editting a card has been unsuccessful
const editPayCardFail = ( dispatch, error ) =>{

    dispatch({

        type: EDIT_CARD_FAIL,  

        error: error

    })

}

// function for initiating a redux change when editting a card has been successful
const editPayCardSuccess = ( dispatch ) => {

    dispatch({

        type: EDIT_CARD_SUCCESS, 

    })

    AsyncStorage.getItem('user').then((user)=>{

        AsyncStorage.getItem('password').then((pass)=>{

            const details = {

                email: JSON.parse(user).data.email,

                password: pass

            }

            axios.post(apiIp + '/person/email_login/', details).then(

                response => {

                    // store user details
                    storeData('user', JSON.stringify(response.data))

                    // store user token
                    storeData('token', response.headers.token)

                }

            )

        })

    })

}


// function for initiating a redux change when adding a card has been unsuccessful
const addCardFail = ( dispatch, error ) =>{

    dispatch({

        type: REGISTER_CARD_FAIL,  

        error: error

    })

}

// function for initiating a redux change when adding a card has been successful
const addCardSuccess = ( dispatch, data ) => {

    dispatch({

        type: REGISTER_CARD_SUCCESS, 

    })

    AsyncStorage.getItem('user').then((user)=>{

        AsyncStorage.getItem('password').then((pass)=>{

            const details = {

                email: JSON.parse(user).data.email,

                password: pass

            }

            axios.post(apiIp + '/person/email_login/', details).then(

                response => {

                    // store user details
                    storeData('user', JSON.stringify(response.data))

                    // store user token
                    storeData('token', response.headers.token)


                    Actions.payments({ orderData: data.orderData })

                }

            )

        })

    })

}

// Function for storing user details
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}