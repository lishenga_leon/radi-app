import { 

    EMAIL_AFTER_ORDER_CHANGED,PASSWORD_AFTER_ORDER_CHANGED, LOGIN_AFTER_ORDER_USER, 

    LOGIN_AFTER_ORDER_USER_SUCCESS, LOGIN_AFTER_ORDER_USER_FAIL

} from './types';

import { apiIp } from '../Config';

import axios from 'axios';

import { Actions } from 'react-native-router-flux';

import AsyncStorage from '@react-native-community/async-storage';


// function for changing the email address text using redux
export const emailAddressAfterOrderChange = (text) => {

    return{

        type: EMAIL_AFTER_ORDER_CHANGED,

        payload: text

    }

}

// function for changing the password using redux
export const passwordAfterOrderChange = (text) => {

    return {

        type: PASSWORD_AFTER_ORDER_CHANGED,

        payload: text

    }

}

// function for logging in the user or taking an already user to payments page
export const loginAfterOrderUser = ( data, orderData ) => {

    return (dispatch) => {

        dispatch({ type: LOGIN_AFTER_ORDER_USER });

        axios.post(apiIp + '/person/email_login/', data).then(

            response => {
                
                if(JSON.stringify(response.data.status_code) == 500){

                    loginUserFail( dispatch )

                }else{

                    storePassword('password', data.password)

                    loginUserAfterOrderSuccess( dispatch, response, orderData )

                }

            }

        ).catch(() => loginUserFail(dispatch) )

    }

}

// Private function for dispatching a user failed request 
const loginUserFail = ( dispatch ) =>{

    dispatch({

        type: LOGIN_AFTER_ORDER_USER_FAIL,  

    })

}

// Private function for dispatching a user success request
const loginUserAfterOrderSuccess = ( dispatch, response, orderData ) => {

    dispatch({

        type: LOGIN_AFTER_ORDER_USER_SUCCESS, 

    })

    // store user details
    storeData('user', JSON.stringify(response.data))

    // function to register user device uid
    AsyncStorage.getItem('device_uid').then((uid)=>{

        const data = {

            person_id: response.data.data.id,

            device_uid: JSON.parse(uid)

        }

        let headers = {

            accept: "application/json",

            "Accept-Language": "en-US,en;q=0.8",

            'Content-Type': 'application/json',

            "token": response.headers.token

        }
        
        axios.post(apiIp + '/person/person_device_uid/', data, { headers: headers }).then(

            response => {

                console.log(response)

            }

        )

    })

    // store user token
    storeData('token', response.headers.token)

    if( orderData.foods.length ){
        
        AsyncStorage.getItem('restaurant_name').then((data) => {

            const foodDetails = {

                restaurant_id: orderData.restaurant_id,

                foods: orderData.foods,

                totalFoodPrice: orderData.totalFoodPrice,

                msisdn: response.data.data.msisdn,

                person_id: response.data.data.id,

                email: response.data.data.email,

                date_card_added: response.data.data.date_card_added,

                restaurant_image: orderData.restaurant_image,

                restaurant_name: JSON.parse(data),

                order_type: orderData.order_type,

                expected_time: orderData.expected_time,

                instructions: orderData.instructions

            }

            Actions.payments({ orderData: foodDetails })

        })

    }else{

        Actions.drawer()

    }

}

// Function for storing user details
storeData = async (item, selectedValue) => {

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}

// Function for storing user password
storePassword = async (item, selectedValue)=>{

    try {

        await AsyncStorage.setItem(item, selectedValue);

    } catch (error) {

        console.error('AsyncStorage error: ' + error.message);

    }

}