import { TRANSACTIONS_FAIL, TRANSACTIONS_SUCCESS } from '../Actions/types';

const INITIAL_STATE = {
    alltransactions: [],
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case TRANSACTIONS_SUCCESS:
            return {
                ...state,
                alltransactions: action.payload
            };

        case TRANSACTIONS_FAIL:
            return {
                ...state,
                error: action.error
            };

        default:
            return state;
    }
}