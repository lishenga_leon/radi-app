import { CALCULATE_DISTANCE } from '../Actions/types';

const INITIAL_STATE = { 
    finalData: []
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){

        case CALCULATE_DISTANCE:
            return { 
                ...state, 
                finalData: action.payload 
            };

        default:
            return state;
    }
}