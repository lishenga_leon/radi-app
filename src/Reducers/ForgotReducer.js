import { FORGOT_EMAIL_ADDRESS, FORGOT_PASSWORD, FORGOT_PASSWORD_FAIL, FORGOT_PASSWORD_SUCCESS } from '../Actions/types';

const INITIAL_STATE = {
    load: false,
    error: '',
    email: '',
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case FORGOT_PASSWORD:
            return {
                ...state,
                load: true,
                error: ''
            }

        case FORGOT_PASSWORD_SUCCESS:
            return {
                ...INITIAL_STATE,
                error: 'Kindly check your Inbox, Spasm or Junk MailBoxes',
            };

        case FORGOT_PASSWORD_FAIL:
            return {
                ...state,
                error: 'Kindly try again later',
                load: false
            }

        case FORGOT_EMAIL_ADDRESS:
            return {
                ...state,
                email: action.payload
            };

        default:
            return state;
    }
}