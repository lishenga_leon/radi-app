import {
    GET_PROFILE_PIC, PROFILE_EMAIL_CHANGED, PROFILE_FULL_NAME_CHANGED, PROFILE_PASSWORD_CHANGED, USER_ID, TOKEN,
    PROFILE_PHONE_NUMBER_CHANGED, PROFILE_UPDATE_USER, PROFILE_UPDATE_USER_FAIL, PROFILE_UPDATE_USER_SUCCESS,
} from '../Actions/types';

const INITIAL_STATE = {
    profpic: '',
    loader: false,
    error: '',
    password: '',
    email: '',
    fullname: '',
    msisdn: '',
    userId: '',
    token: '',
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case GET_PROFILE_PIC:
            return {
                ...state,
                profpic: action.payload,
            }

        case TOKEN:
            return {
                ...state,
                token: action.payload,
            }

        case USER_ID:
            return {
                ...state,
                userId: action.payload,
            }

        case PROFILE_PASSWORD_CHANGED:
            return {
                ...state,
                password: action.payload
            };

        case PROFILE_EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload
            };

        case PROFILE_PHONE_NUMBER_CHANGED:
            return {
                ...state,
                msisdn: action.payload
            };

        case PROFILE_FULL_NAME_CHANGED:
            return {
                ...state,
                fullname: action.payload
            };

        case PROFILE_UPDATE_USER:
            return {
                ...state,
                loader: true,
                errors: ''
            }

        case PROFILE_UPDATE_USER_SUCCESS:
            return {
                ...state,
                password: '',
                loader: false,
                errors: 'Success'
            };

        case PROFILE_UPDATE_USER_FAIL:
            return {
                ...state,
                errors: action.error,
                password: '',
                loader: false
            }

        default:
            return state;
    }
}