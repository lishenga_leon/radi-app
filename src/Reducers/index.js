import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer';
import CreateReducer from './CreateReducer';
import PostHomeReducer from './PostHomeReducer';
import IndexHomeReducer from './IndexHomeReducer';
import UploadPostReducer from './UploadPostReducer';
import JointReducer from './JointReducer';
import JointPageReducer from './JointPageReducer';
import ListMenuReducer from './ListMenuReducer';
import OrderReducer from './OrderReducer';
import FirstPageReducer from './FirstPageReducer';
import BookingsReducer from './BookingsReducer';
import CreaterAfterOrderReducer from './CreaterAfterOrderReducer';
import LoginAfterOrderReducer from './LoginAfterOrderReducer';
import PaymentsReducer from './PaymentsReducer';
import MapReducer from './MapReducer';
import PhoneNumberReducer from './PhoneNumberReducer';
import PhoneNumberAfterOrderReducer from './PhoneNumberAfterOrderReducer';
import ProfileReducer from './ProfileReducer';
import RateRestaurantReducer from './RateRestaurantReducer';
import ForgotReducer from './ForgotReducer';
import ThirstyReducer from './ThirstyReducer';
import HungryReducer from './HungryReducer';
import OutAboutReducer from './Out&AboutReducer';
import MapRestaurantReducer from './MapRestaurantReducer';
import FavoritesReducer from './FavoritesReducer';
import PersonOrdersReducer from "./PersonOrdersReducer";
import AddCardReducer from './AddCardReducer';
import TransactionsReducer from './TransactionsReducer';



export default combineReducers({
    login: LoginReducer,
    posts: PostHomeReducer,
    indexHome: IndexHomeReducer,
    create: CreateReducer,
    uploadPost: UploadPostReducer,
    joints: JointReducer,
    jointPage: JointPageReducer,
    listMenu: ListMenuReducer,
    Orders: OrderReducer,
    FirstPage: FirstPageReducer,
    bookings: BookingsReducer,
    createAfterOrder: CreaterAfterOrderReducer,
    loginAfterOrder: LoginAfterOrderReducer,
    payments: PaymentsReducer,
    map: MapReducer,
    phone: PhoneNumberReducer,
    phoneAfterOrder:PhoneNumberAfterOrderReducer,
    profile:ProfileReducer,
    raterestaurant: RateRestaurantReducer,
    forgot: ForgotReducer,
    thirsty: ThirstyReducer,
    hungry: HungryReducer,
    outandabout: OutAboutReducer,
    mapsrestaurant: MapRestaurantReducer,
    favorites: FavoritesReducer,
    personorders: PersonOrdersReducer,
    addcard: AddCardReducer,
    transactions: TransactionsReducer,


})