import { 
    JOINT_DETAILS_FAIL, JOINT_DETAILS_SUCCESS
} from '../Actions/types';

const INITIAL_STATE = { 
    joint: {},
    coord:{
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    },
    error: ''
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){

        case JOINT_DETAILS_SUCCESS:
            return { 
                ...state, 
                joint: action.payload,
                coord: {
                    latitude: Number(action.payload.lat),
                    longitude: Number(action.payload.long),
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421, 
                }
            };

        case JOINT_DETAILS_FAIL:
            return { 
                ...state, 
                error: action.error
            }

        default:
            return state;
    }
}