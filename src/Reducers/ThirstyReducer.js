import {
    GET_PERSON_THIRSTY_LOCATION, GET_ALL_THIRSTY_RESTAURANTS_FAIL, SEARCH_MAP_THIRSTY_RESTAURANT, SEARCH_MAP_THIRSTY_RESTAURANT_SUCCESS,
    MAP_JOINT_THIRSTY_DETAILS_FAIL, MAP_JOINT_THIRSTY_DETAILS_SUCCESS, SEARCH_MAP_THIRSTY_RESTAURANT_FAIL
} from '../Actions/types';

const INITIAL_STATE = {
    coord: {},
    restaurants: [],
    joint: {},
    distance: '',
    restaurant_name: '',
    total: '',
    error: '',
    location: {}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case GET_ALL_THIRSTY_RESTAURANTS_FAIL:
            return {
                ...state,
                restaurants: [],
                error: action.error
            }

        case MAP_JOINT_THIRSTY_DETAILS_SUCCESS:
            return {
                ...state,
                joint: action.payload,
                coord: {
                    latitude: action.coord.latitude,
                    longitude: action.coord.longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                },
                distance: action.distance
            };

        case SEARCH_MAP_THIRSTY_RESTAURANT:
            return {
                ...state,
                restaurant_name: action.payload
            }

        case SEARCH_MAP_THIRSTY_RESTAURANT_SUCCESS:
            return {
                ...state,
                restaurants: action.restaurants,
                total: action.total,
                location: action.location
            };

        case GET_PERSON_THIRSTY_LOCATION:
            return {
                ...state,
                restaurants: action.restaurants,
                total: action.total,
                joint: {}
            }

        case SEARCH_MAP_THIRSTY_RESTAURANT_FAIL:
            return {
                ...state,
                error: action.error
            };

        case MAP_JOINT_THIRSTY_DETAILS_FAIL:
            return {
                ...state,
                error: action.error
            }

        default:
            return state;
    }
}