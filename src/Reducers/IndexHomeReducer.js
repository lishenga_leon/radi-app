import { SEARCH_MAP } from '../Actions/types';

const INITIAL_STATE = { 
    search: '',
    errors: '',
    loader: false
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){

        case SEARCH_MAP:
            return { 
                ...state, 
                search: action.payload 
            }

        default:
            return state;
    }
}