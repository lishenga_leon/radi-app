import {
    PASSWORD_AFTER_ORDER_CHANGED, EMAIL_AFTER_ORDER_CHANGED, CREATE_AFTER_ORDER_USER, REGISTER_USER_PUSH_NOTIFICATION_SUCCESS,
    CREATE_AFTER_ORDER_USER_FAIL, CREATE_AFTER_ORDER_USER_SUCCESS, CREATE_USER_GOOGLE, REGISTER_USER_PUSH_NOTIFICATION_FAIL,
    PHONE_NUMBER_AFTER_ORDER_CHANGED, FULL_NAME_AFTER_ORDER_CHANGED, CREATE_USER, REGISTER_USER_PUSH_NOTIFICATION, USER_DEVICE_UID
} from '../Actions/types';

const INITIAL_STATE = {
    pass: '',
    email: '',
    fullname: '',
    phone: '',
    user: null,
    errors: '',
    loader: false,
    device_uid: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case CREATE_USER_GOOGLE:
            return INITIAL_STATE;

        case CREATE_USER:
            return {
                ...state,
                loader: true,
                errors: ''
            }

        case USER_DEVICE_UID:
            return {
                ...state,
                device_uid: action.payload
            };

        case REGISTER_USER_PUSH_NOTIFICATION:
            return {
                ...state,
            };

        case REGISTER_USER_PUSH_NOTIFICATION_FAIL:
            return {
                ...INITIAL_STATE,
                errors: action.error
            };

        case REGISTER_USER_PUSH_NOTIFICATION_SUCCESS:
            return INITIAL_STATE;

        case PASSWORD_AFTER_ORDER_CHANGED:
            return {
                ...state,
                pass: action.payload
            };

        case EMAIL_AFTER_ORDER_CHANGED:
            return {
                ...state,
                email: action.payload
            };

        case PHONE_NUMBER_AFTER_ORDER_CHANGED:
            return {
                ...state,
                phone: action.payload
            };

        case FULL_NAME_AFTER_ORDER_CHANGED:
            return {
                ...state,
                fullname: action.payload
            };

        case CREATE_AFTER_ORDER_USER:
            return {
                ...state,
                loader: true,
                errors: ''
            }

        case CREATE_AFTER_ORDER_USER_SUCCESS:
            return INITIAL_STATE;

        case CREATE_AFTER_ORDER_USER_FAIL:
            return {
                ...state,
                errors: 'Failed: Kindly try again later',
                password: '',
                loader: false
            }

        default:
            return state;
    }
}