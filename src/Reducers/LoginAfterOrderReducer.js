import { 
    EMAIL_AFTER_ORDER_CHANGED,PASSWORD_AFTER_ORDER_CHANGED, LOGIN_AFTER_ORDER_USER, 
    LOGIN_AFTER_ORDER_USER_SUCCESS, LOGIN_AFTER_ORDER_USER_FAIL
} from '../Actions/types';

const INITIAL_STATE = { 
    emailAddress: '',
    password: '',
    user: null,
    error: '',
    loading: false
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){

        case EMAIL_AFTER_ORDER_CHANGED:
            return { 
                ...state, 
                emailAddress: action.payload 
            };

        case PASSWORD_AFTER_ORDER_CHANGED:
            return { 
                ...state, 
                password: action.payload 
            };

        case LOGIN_AFTER_ORDER_USER:
            return{ 
                ...state, 
                loading: true, 
                error: '' 
            }

        case LOGIN_AFTER_ORDER_USER_SUCCESS:
            return { ...INITIAL_STATE };

        case LOGIN_AFTER_ORDER_USER_FAIL:
            return { 
                ...state, 
                error: 'Kindly Provide the Right Credentials.', 
                password: '', 
                loading: false 
            }

        default:
            return state;
    }
}