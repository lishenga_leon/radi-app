import {
    POSTS_FAIL, POSTS_SUCCESS, REGISTER_DEVICE_ID_FAIL, REGISTER_DEVICE_ID_SUCCESS
} from '../Actions/types';

const INITIAL_STATE = {
    post: [],
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case POSTS_SUCCESS:
            return {
                ...state,
                post: state.post.concat(action.payload),
            }

        case REGISTER_DEVICE_ID_FAIL:
            return INITIAL_STATE;

        case REGISTER_DEVICE_ID_SUCCESS:
            return { ...state };

        case POSTS_FAIL:
            return INITIAL_STATE;

        default:
            return state;
    }
}