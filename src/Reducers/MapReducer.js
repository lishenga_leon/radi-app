import {
    GET_PERSON_LOCATION, SEARCH_MAP_RESTAURANT, SEARCH_MAP_RESTAURANT_FAIL, 
    MAP_JOINT_DETAILS_FAIL, MAP_JOINT_DETAILS_SUCCESS, SEARCH_MAP_RESTAURANT_SUCCESS
} from '../Actions/types';

const INITIAL_STATE = {
    coord: {},
    restaurants: [],
    joint: {},
    distance: '',
    restaurant_name: '',
    total: '',
    error: '',
    location: {}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case MAP_JOINT_DETAILS_SUCCESS:
            return {
                ...state,
                joint: action.payload,
                coord: {
                    latitude: action.coord.latitude,
                    longitude: action.coord.longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                },
                distance: action.distance
            };

        case SEARCH_MAP_RESTAURANT:
            return {
                ...state,
                restaurant_name: action.payload
            }

        case SEARCH_MAP_RESTAURANT_SUCCESS:
            return {
                ...state,
                restaurants: action.restaurants,
                total: action.total,
                location: action.location
            };

        case GET_PERSON_LOCATION:
            return {
                ...state,
                restaurants: action.restaurants,
                total: action.total,
                joint: {}
            }

        case SEARCH_MAP_RESTAURANT_FAIL:
            return {
                ...state,
                error: action.error
            };

        case MAP_JOINT_DETAILS_FAIL:
            return {
                ...state,
                error: action.error
            }

        default:
            return state;
    }
}