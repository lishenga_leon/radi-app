import {
    GETTING_USER_DETAILS_ASYNCSTORAGE, GET_PICTURE, UPLOAD_POST, PICK_RESTAURANT, RESTAURANT_NAME_CHANGE_SUCCESS,
    UPLOAD_PICTURE_POST_SUCCESS, POST_CHANGE, RESTAURANT_NAME_CHANGE, RESTAURANT_NAME_CHANGE_FAIL, UPLOAD_PICTURE_POST_ERROR
} from '../Actions/types';

const INITIAL_STATE = {
    restaurant: '',
    restaurants: [],
    post: '',
    picture: '',
    errors: '',
    loader: false,
    restaurant_id: '',
    showLoading: false,
    location: {}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case RESTAURANT_NAME_CHANGE:
            return {
                ...state,
                restaurant: action.payload,
                showLoading: true
            }

        case RESTAURANT_NAME_CHANGE_FAIL:
            return {
                ...state,
                showLoading: false,
                errors: action.error
            };

        case RESTAURANT_NAME_CHANGE_SUCCESS:
            return {
                ...state,
                restaurants: action.restaurants,
                showLoading: false
            }

        case PICK_RESTAURANT:
            return {
                ...state,
                restaurant: action.payload,
                restaurant_id: action.restaurant_id,
                location: action.location
            }

        case GET_PICTURE:
            return {
                ...state,
                picture: action.payload,
            }

        case POST_CHANGE:
            return {
                ...state,
                post: action.payload
            }
        case GETTING_USER_DETAILS_ASYNCSTORAGE:
            return {
                ...state,
                ...INITIAL_STATE,
                errors: 'Failed: Kindly login again',
                loader: false
            }
        case UPLOAD_PICTURE_POST_SUCCESS:
            return {
                ...state,
                ...INITIAL_STATE,
                errors: 'Picture Uploaded Successfully',
                loader: false
            }

        case UPLOAD_PICTURE_POST_ERROR:
            return {
                ...state,
                post: '',
                errors: action.payload,
                loader: false
            }
        case UPLOAD_POST:
            return {
                ...state,
                loader: true,
                errors: ''
            }
        default:
            return INITIAL_STATE;
    }
}