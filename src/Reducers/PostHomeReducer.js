import {
    POSTS_FAIL, POSTS_SUCCESS, LIKE_SUCCESS, ISREFRESH_STATE, LOADING_STATE, POSTS_REFRESH,
    LIKE_FAIL, USER_ID, SHOW_UPLOAD_CAMERA_ICON, DATE_TIMER_BOOKING_TIMER, NEXT_PAGE
} from '../Actions/types';

const INITIAL_STATE = {
    allPosts: [],
    post_id: '',
    like_id: [],
    person_id: '',
    user_id: '',
    cameraIcon: true,
    dateTimeBooking: 0,
    bookingDate: '',
    page: 1,
    isRefreshing: false,
    loading: false,
    arrayNumber: 0, 
    headers: {},
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case LIKE_SUCCESS:
            return {
                ...state,
                post_id: action.payload,
                allPosts: action.posts
            }

        case DATE_TIMER_BOOKING_TIMER:
            return {
                ...state,
                dateTimeBooking: 0,
            }

        case POSTS_REFRESH:
            return {
                ...state,
                allPosts: [],
            }

        case ISREFRESH_STATE:
            return {
                ...state,
                isRefreshing: action.payload,
            }

        case NEXT_PAGE:
            return {
                ...state,
                page: action.payload,
            }

        case LOADING_STATE:
            return {
                ...state,
                loading: action.payload,
            }

        case SHOW_UPLOAD_CAMERA_ICON:
            return {
                ...state,
                cameraIcon: action.payload,
            }

        case USER_ID:
            return {
                ...state,
                user_id: action.payload,
            }

        case POSTS_SUCCESS:
            if (action.payload.length) {
                return {
                    ...state,
                    allPosts: state.allPosts.concat(action.payload),
                    person_id: action.person_id,
                    dateTimeBooking: 0,
                    bookingDate: 0,
                    loading: false,
                    arrayNumber: action.arrayNumber, 
                    headers: action.headers
                }
            } else {
                return {
                    ...state,
                    person_id: action.person_id,
                    dateTimeBooking: 0,
                    bookingDate: 0,
                    loading: false,
                    headers: action.headers
                }
            }

        case LIKE_FAIL:
            return {
                ...state,
                error: action.error
            }

        case POSTS_FAIL:
            return {
                ...state,
                error: action.error
            }

        default:
            return state;
    }
}