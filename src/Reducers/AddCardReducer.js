import {
    ADD_CARD_NUMBER, ADD_CARD_YEAR, ADD_CARD_MONTH, EDIT_CARD, EDIT_CARD_FAIL,
    ADD_CARD_CVC, REGISTER_CARD, REGISTER_CARD_SUCCESS, REGISTER_CARD_FAIL, EDIT_CARD_SUCCESS
} from '../Actions/types';

const INITIAL_STATE = {
    cardcvc: '',
    cardyear: '',
    cardnumber: '',
    cardmonth: '',
    load: false,
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case ADD_CARD_CVC:
            return {
                ...state,
                cardcvc: action.payload
            };

        case ADD_CARD_MONTH:
            return {
                ...state,
                cardmonth: action.payload
            };

        case EDIT_CARD:
            return {
                ...state,
                load: true
            };

        case EDIT_CARD_SUCCESS:
            return {
                ...INITIAL_STATE,
                error: 'Card added Successfully'
            };

        case EDIT_CARD_FAIL:
            return {
                ...INITIAL_STATE,
                error: action.error
            };

        case REGISTER_CARD:
            return {
                ...state,
                load: true
            };

        case REGISTER_CARD_FAIL:
            return {
                ...state,
                error: action.error
            };

        case REGISTER_CARD_SUCCESS:
            return INITIAL_STATE;

        case ADD_CARD_NUMBER:
            return {
                ...state,
                cardnumber: action.payload
            };

        case ADD_CARD_YEAR:
            return {
                ...state,
                cardyear: action.payload
            };

        default:
            return state;
    }
}