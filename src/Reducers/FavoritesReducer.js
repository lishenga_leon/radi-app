import { FAVORITE_RESTAURANTS, FAVORITE_RESTAURANTS_FAIL } from '../Actions/types';

const INITIAL_STATE = {
    restaurants: [],
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case FAVORITE_RESTAURANTS:
            return {
                ...state,
                restaurants: action.payload
            };

        case FAVORITE_RESTAURANTS_FAIL:
            return {
                ...state,
                error: action.error
            };

        default:
            return state;
    }
}