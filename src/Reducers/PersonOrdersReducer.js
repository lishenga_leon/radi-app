import { GET_ALL_ORDERS, GET_ALL_ORDERS_FAIL } from '../Actions/types';

const INITIAL_STATE = {
    orders: [],
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case GET_ALL_ORDERS:
            return {
                ...state,
                orders: action.payload,
            }

        case GET_ALL_ORDERS_FAIL:
            return {
                ...INITIAL_STATE,
                error: action.error
            };

        default:
            return state;
    }
}