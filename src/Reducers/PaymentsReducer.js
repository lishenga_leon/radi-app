import {
    MAKE_PAYMENT_CARD, MAKE_PAYMENT_MPESA, MAKE_PAYMENT_PAYPAL,
    PAYMENT_SUCCESS, PAYMENT_FAIL, PHONE_NUMBER_CHANGED
} from '../Actions/types';

const INITIAL_STATE = {
    loadCard: false,
    loadMpesa: false,
    loadPaypal: false,
    error: '',
    msisdn: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case MAKE_PAYMENT_CARD:
            return {
                ...state,
                loadCard: true,
                error: ''
            }
        case MAKE_PAYMENT_MPESA:
            return {
                ...state,
                loadMpesa: true,
                error: ''
            }
        case MAKE_PAYMENT_PAYPAL:
            return {
                ...state,
                loadPaypal: true,
                error: ''
            }

        case PHONE_NUMBER_CHANGED:
            return {
                ...state,
                msisdn: action.payload,
            }


        case PAYMENT_SUCCESS:
            return INITIAL_STATE;

        case PAYMENT_FAIL:
            return {
                ...state,
                error: action.error,
                load: false
            }

        default:
            return state;
    }
}