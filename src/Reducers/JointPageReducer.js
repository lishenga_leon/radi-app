import { 
    MENU_FAIL, GET_MENU_SUCCESS, SHOW_OFFERS_MODAL
} from '../Actions/types';

const INITIAL_STATE = { 
    menu: [],
    showModal: false,
    restaurant_name: '',
    error: ''
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){
        case GET_MENU_SUCCESS:
            return {  
                ...state,
                menu: action.payload,
                restaurant_name: action.restaurant_name
            }

        case SHOW_OFFERS_MODAL:
            return {
                ...state,
                showModal: action.payload
            }

        case MENU_FAIL:
            return {
                ...state,
                restaurant_name: action.restaurant_name,
                error: action.error
            };

        default:
            return state;
    }
}