import { GET_PHONE_NUMBER, GET_PASSWORD_GOOGLE_LOGIN } from '../Actions/types';

const INITIAL_STATE = { 
    msisdn: '', 
    password: ''
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){

        case GET_PHONE_NUMBER:
            return {
                ...state,
                msisdn: action.payload,
            }

        case GET_PASSWORD_GOOGLE_LOGIN:
            return{
                ...state,
                password: action.payload
            }

        default:
            return state;
    }
}