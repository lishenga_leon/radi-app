import {
    NUMBER_OF_SEATS_CHANGED, DATE_TIME_FOR_BOOKING_CHANGED, DATETIME_VISIBLE_MODAL,
    MAKE_A_BOOKING, BOOKING_SUCCESS, BOOKING_FAIL, ORDER_TYPE
} from '../Actions/types';

const INITIAL_STATE = {
    numberSeats: undefined,
    datetimeBooking: undefined,
    isDateTimePickerVisible: false,
    error: '',
    load: false,
    order_type: undefined
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case NUMBER_OF_SEATS_CHANGED:
            return {
                ...state,
                numberSeats: action.payload
            };

        case ORDER_TYPE:
            return {
                ...state,
                order_type: action.payload
            };

        case DATE_TIME_FOR_BOOKING_CHANGED:
            return {
                ...state,
                datetimeBooking: action.payload
            };

        case DATETIME_VISIBLE_MODAL:
            return {
                ...state,
                isDateTimePickerVisible: !state.isDateTimePickerVisible
            };

        case MAKE_A_BOOKING:
            return {
                ...state,
                load: true,
                error: ''
            }

        case BOOKING_FAIL:
            return {
                ...state,
                load: false,
                error: action.payload
            }

        case BOOKING_SUCCESS:
            return INITIAL_STATE;

        default:
            return state;
    }
}