import { 
    EMAIL_CHANGED,PASSWORD_CHANGED, LOGIN_USER, LOGIN_USER_SUCCESS, LOGIN_USER_FAIL
} from '../Actions/types';

const INITIAL_STATE = { 
    emailAddress: '',
    password: '',
    user: null,
    error: '',
    loading: false
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){

        case EMAIL_CHANGED:
            return { 
                ...state, 
                emailAddress: action.payload 
            };

        case PASSWORD_CHANGED:
            return { 
                ...state, 
                password: action.payload 
            };

        case LOGIN_USER:
            return{ 
                ...state, 
                loading: true, 
                error: '' 
            }

        case LOGIN_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE }

        case LOGIN_USER_FAIL:
            return { 
                ...state, 
                error: 'Kindly Provide the Right Credentials.', 
                password: '', 
                loading: false 
            }

        default:
            return state;
    }
}