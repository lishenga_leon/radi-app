import { 
    MENU_GOTTEN, SELECT_MENU, FOODS_FAIL, FOODS_SUCCESS,
    CHECKED_FOOD, NUMBER_OF_PEOPLE, FOODS_CHOOSEN
} from '../Actions/types';

const INITIAL_STATE = { 
    menu: [],
    menu_id:'',
    number: '',
    foodId: '',
    checked: false,
    checker: false,
    numbers: '',
    user: [], 
    data: [], 
    foods: [],
    error: ''
};

export default ( state = INITIAL_STATE, action ) => {
    switch(action.type){
        case MENU_GOTTEN:
            return { 
                ...state, 
                menu: action.menu
            }
        case SELECT_MENU:
            return { 
                ...state, 
                menuId: action.payload
            }
        case CHECKED_FOOD:
            state.user.push({'foodId':action.payload})
            return { 
                ...state, 
                data: state.user,
                foodId: action.payload,
                checked: false,
                checker: true,
                numbers: 1
            }

        case NUMBER_OF_PEOPLE:
            return { 
                ...state, 
                number: '',
                numbers: action.payload
            }

        case FOODS_SUCCESS:
            return { 
                ...state, 
                foods: action.payload,
                menu_id: action.menu_id
            }

        case FOODS_CHOOSEN:
            return { 
                ...state, 
                foods: action.foods
            }

        case FOODS_FAIL:
            return { 
                ...state, 
                foods: state.foods,
                error: action.error
            }
        default:
            return state;
    }
}