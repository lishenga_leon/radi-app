import {
    USER_NAMES, RATE_RESTAURANT, START_RATE_RESTAURANT,
    RATE_RESTAURANT_SUCCESS, RATE_RESTAURANT_FAIL
} from '../Actions/types';

const INITIAL_STATE = {
    fullname: '',
    starCount: 0, 
    load: false,
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case USER_NAMES:
            return {
                ...state,
                fullname: action.payload
            };

        case RATE_RESTAURANT_SUCCESS:
            return {
                ...state,
                load: false,
                error: '',
                starCount: 0
            };

        case START_RATE_RESTAURANT:
            return {
                ...state,
                load: true
            };

        case RATE_RESTAURANT_FAIL:
            return {
                ...state,
                load: false,
                error: action.error
            };

        case RATE_RESTAURANT:
            return {
                ...state,
                starCount: action.payload
            }

        default:
            return state;
    }
}