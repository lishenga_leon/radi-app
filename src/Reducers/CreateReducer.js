import {
    PASSWORD_CHANGED, EMAIL_CHANGED, CREATE_USER, REGISTER_USER_PUSH_NOTIFICATION, LOGIN_USER_GOOGLE_FACEBOOK,
    CREATE_USER_FAIL, CREATE_USER_SUCCESS, CREATE_USER_GOOGLE_FACEBOOK, REGISTER_USER_PUSH_NOTIFICATION_FAIL,
    PHONE_NUMBER_CHANGED, FULL_NAME_CHANGED, USER_DEVICE_UID, REGISTER_USER_PUSH_NOTIFICATION_SUCCESS
} from '../Actions/types';

const INITIAL_STATE = {
    pass: '',
    email: '',
    fullname: '',
    phone: '',
    user: null,
    errors: '',
    loader: false,
    device_uid: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case CREATE_USER_GOOGLE_FACEBOOK:
            return INITIAL_STATE;

        case LOGIN_USER_GOOGLE_FACEBOOK:
            return INITIAL_STATE;

        case USER_DEVICE_UID:
            return {
                ...state,
                device_uid: action.payload
            };

        case REGISTER_USER_PUSH_NOTIFICATION:
            return {
                ...state,
            };

        case REGISTER_USER_PUSH_NOTIFICATION_FAIL:
            return { 
                ...INITIAL_STATE,
                errors: action.error 
            };

        case REGISTER_USER_PUSH_NOTIFICATION_SUCCESS:
            return INITIAL_STATE;

        case PASSWORD_CHANGED:
            return {
                ...state,
                pass: action.payload
            };

        case EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload
            };

        case PHONE_NUMBER_CHANGED:
            return {
                ...state,
                phone: action.payload
            };

        case FULL_NAME_CHANGED:
            return {
                ...state,
                fullname: action.payload
            };

        case CREATE_USER:
            return {
                ...state,
                loader: true,
                errors: ''
            }

        case CREATE_USER_SUCCESS:
            return INITIAL_STATE;

        case CREATE_USER_FAIL:
            return {
                ...state,
                errors: 'Failed: Kindly try again later',
                password: '',
                loader: false
            }

        default:
            return state;
    }
}