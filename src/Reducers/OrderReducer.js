import {
    FOODS_CHOOSEN_FAIL, FOODS_CHOOSEN_SUCCESS, ORDER_FAIL, ORDER_SUCCESS,
    ADD_PEOPLE_EATING, MINUS_PEOPLE_EATING, MAKE_AN_ORDER, ORDER_INSTRUCTIONS
} from '../Actions/types';

const INITIAL_STATE = {
    foodsChoosen: [],
    food_id: '',
    totalFoodPrice: '',
    load: '',
    error: '',
    instructions: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case FOODS_CHOOSEN_SUCCESS:
            return {
                ...state,
                foodsChoosen: action.payload,
                totalFoodPrice: action.totalFoodPrice
            };

        case MINUS_PEOPLE_EATING:
            return {
                ...state,
                foodsChoosen: action.payload,
                food_id: action.food_id,
                totalFoodPrice: action.totalFoodPrice
            };

        case ADD_PEOPLE_EATING:
            return {
                ...state,
                foodsChoosen: action.payload,
                food_id: action.food_id,
                totalFoodPrice: action.totalFoodPrice
            };

        case ORDER_INSTRUCTIONS:
            return {
                ...state,
                instructions: action.payload
            };

        case FOODS_CHOOSEN_FAIL:
            return {
                ...state,
                foodsChoosen: [],
                totalFoodPrice: action.totalFoodPrice
            };

        case MAKE_AN_ORDER:
            return {
                ...state,
                load: true,
                error: ''
            }

        case ORDER_SUCCESS:
            return INITIAL_STATE;

        case ORDER_FAIL:
            return {
                ...state,
                error: 'Failed: Kindly Try Again',
                load: false
            }

        default:
            return state;
    }
}