import {
    Container, Content, Grid,
    Row, Col, Card,
} from 'native-base';
import { StyleSheet, FlatList, Dimensions, Text, Platform, Image } from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllTransactions } from '../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { apiIp } from '../Config';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class Transactions extends Component {

    componentDidMount() {
        this.props.getAllTransactions()
    }

    renderItem = (item) => {

        const { order, transactions } = item.item

        const { thumbNailStyle, contentNameTextStyles, cardStyle } = styles

        return (
            <Card style={cardStyle}>
                <Grid>
                    <Row size={1}>
                        <Col size={1}>
                            <Image source={{ uri: apiIp + order.restaurant.cover_image }} style={thumbNailStyle} />
                        </Col>
                    </Row>
                    <Row size={1}>
                        <Col size={1}>
                            <Card transparent >
                                <Text style={contentNameTextStyles}>
                                    Restaurant:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10, marginLeft: 5 }}>
                                        {order.restaurant.name}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Category:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {order.restaurant.category}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Amount:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {transactions.amount}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Payment Status:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {order.status}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Order Type:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {order.order_type}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Food Instructions:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {order.food_instructions}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Order due date:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {Date(order.expected_time)}
                                    </Text>
                                </Text>
                                <Text style={contentNameTextStyles}>
                                    Payment Date:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                        {Date(order.updated_at)}
                                    </Text>
                                </Text>
                            </Card>
                        </Col>
                    </Row>
                </Grid>
            </Card>
        )
    }

    render() {

        const { contentStyle, headerStyle, contentNameTextStyles, favoritesHeading } = styles

        const { alltransactions } = this.props

        return (
            <Container style={{ backgroundColor: '#352644', }}>
                <Text style={favoritesHeading}>Latest Transactions</Text>
                <Content style={contentStyle}>
                    <FlatList
                        data={alltransactions}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.order.id}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    cardStyle: {
        alignSelf: 'center',
        borderRadius: 10,
        width: PHONE_WIDTH / 1.5
    },
    contentStyle: {
        padding: 0,
        marginTop: 40
    },
    thumbNailStyle: {
        height: 100,
        width: PHONE_WIDTH / 1.5,
        borderRadius: 10,
    },
    contentNameTextStyles: {
        marginTop: 20,
        fontSize: 10,
        marginLeft: 20,
    },
    headerStyle: {
        height: 50
    },
    favoritesHeading: {
        fontSize: 35,
        marginTop: 20,
        marginLeft: 10,
        color: 'white',
        alignSelf: 'center'
    }
})

const mapStateToProps = ({ transactions }) => {
    const { alltransactions } = transactions;
    return { alltransactions };
};

export default connect(mapStateToProps, {
    getAllTransactions
})(Transactions);