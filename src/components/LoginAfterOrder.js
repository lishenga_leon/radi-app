import React, { Component } from 'react';
import { StyleSheet, SafeAreaView, StatusBar } from 'react-native';
import  LoginAfterOrderSwiper  from './common/Swiper/LoginAfterOrderSwiper';

class LoginAfterOrder extends Component {

    render() {

        const { container } = styles;
        const { orderData } = this.props
        
        return (
            <SafeAreaView style={container}>
                <StatusBar barStyle="light-content"/>
                <LoginAfterOrderSwiper orderData={orderData}/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'rgb(219, 220, 221)',
        flexDirection: 'column',
    },
})

export default LoginAfterOrder;