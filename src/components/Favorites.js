import { 
    Container, Content, Grid, 
    Row, Col, Card, 
} from 'native-base';
import { 
    StyleSheet, TouchableOpacity, FlatList, 
    Dimensions, Image, Text, Platform 
} from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { apiIp } from '../Config';
import { connect } from 'react-redux';
import { get5FavoriteRestaurants } from '../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class Favorites extends Component {

    componentDidMount(){
        this.props.get5FavoriteRestaurants()
    }

    renderItem = (item) =>{
        const { id ,name, cover_image, rating, category, description } = item.item
        console.log(item)
        const { thumbNailStyle, contentNameTextStyles, cardStyle } = styles

        return(
            <Card style={cardStyle}>
                <TouchableOpacity onPress={()=> Actions.joint({ restaurant_id: id })}>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Image source={{ uri: apiIp+'/media/restaurant/picture/'+cover_image }} style={thumbNailStyle}/>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <Text style={contentNameTextStyles}>
                                        Restaurant:  
                                        <Text style={{ fontWeight: 'bold', fontSize: 10, marginLeft: 5 }}>
                                            {name.toUpperCase()} 
                                        </Text>
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                        Category:
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                            {category} 
                                        </Text>
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                         Description: 
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                            {description}
                                        </Text>
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                        Rating: 
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                            {rating} stars 
                                        </Text>
                                    </Text>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </TouchableOpacity>
            </Card>
        )
    }

    render() {

        const { contentStyle, headerStyle, contentNameTextStyles, favoritesHeading } = styles

        const { restaurants } = this.props

        return (
            <Container style={{ backgroundColor: '#352644', }}>
                <Text style={favoritesHeading}>Favorite Restaurants</Text>
                <Content style={contentStyle}>
                    <FlatList
                        data={restaurants}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.toString()}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    cardStyle:{
        alignSelf: 'center', 
        borderRadius: 10, 
        width: PHONE_WIDTH/1.5
    },
    contentStyle:{
        padding: 0,
        marginTop: 40
    },
    thumbNailStyle:{  
        height: 100, 
        width: PHONE_WIDTH/1.5,
        borderRadius: 10,
    },
    contentNameTextStyles:{
        marginTop: 20,
        fontSize: 10,
        marginLeft: 20,
    },
    headerStyle:{
        height: 50
    },
    favoritesHeading:{
        fontSize: 35, 
        marginTop: 80, 
        marginLeft: 10, 
        color: 'white', 
        alignSelf: 'center'
    }
})

const mapStateToProps = ({ favorites }) => {
    const { restaurants } = favorites;
    return { restaurants };
};

export default connect(mapStateToProps, {
    get5FavoriteRestaurants
})(Favorites);