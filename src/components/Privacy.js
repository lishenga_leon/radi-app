import {
    Container, Content, Grid,
    Row, Col, Card, Header, Textarea
} from 'native-base';
import { StyleSheet, Text, View, Dimensions, Platform } from 'react-native';
import React, { Component } from 'react';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { apiIp } from '../Config';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');

class Privacy extends Component {

    constructor(props) {
        super(props)
        this.state = {
            privacy: null,
        }
    }

    componentDidMount() {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }
            const data = {

            }

            axios.post(apiIp + '/RadiPrivacy/getRadiPrivacyApp/', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        this.setState({
                            privacy: null
                        })

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        this.setState({
                            privacy: response.data.data
                        })

                        console.log(response.data.data)

                    }

                }

            ).catch((error) => console.log(error))

        })
    }

    render() {

        const {
            headerStyle, contentStyle,
            textHeaderStyle, mapStyle, textAreaStyle
        } = styles

        if (this.state.privacy == null) {

            return (
                <Container>
                    <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                        <Text style={textHeaderStyle}>About Us</Text>
                    </Header>
                    <Content style={contentStyle}>

                        <Grid style={{ marginTop: 40 }}>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card transparent >
                                        <View style={mapStyle}>

                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>

                </Container>
            );
        }

        return (

            <Container>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                    <Text style={textHeaderStyle}>About Us</Text>
                </Header>
                <Content style={contentStyle}>
                    <Grid style={{ marginTop: 40 }}>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <Textarea
                                        rowSpan={5}
                                        bordered
                                        placeholderTextColor="black"
                                        maxLength={100000}
                                        value={this.state.privacy.message}
                                        style={mapStyle}
                                    />
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </Content>

            </Container>

        )

    }
}

const styles = StyleSheet.create({
    headerStyle: {
        height: PHONE_HEIGHT / 5,
        backgroundColor: '#352644'
    },
    textHeaderStyle: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white',
        paddingTop: 60,
        alignContent: 'center',
        alignSelf: 'center',
    },
    contentStyle: {
        padding: 0,
        backgroundColor: '#34183E',
    },
    mapStyle: {
        height: PHONE_HEIGHT / 1.5,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 10
    },
    textAreaStyle: {
        fontSize: 15,
        margin: 10,
        backgroundColor: 'white',
        borderRadius: 10
    }

})

export default Privacy;