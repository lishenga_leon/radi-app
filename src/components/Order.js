import {
    Container, Content, Grid, Card, Col,
    ListItem, Header, Badge, Right, Row, Textarea
} from 'native-base';
import { Button, Spinner } from './common'
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, FlatList, Dimensions, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { getFoodsChoosen, addNumberPeople, minusNumberPeople, nextPage, orderInstructions } from '../Actions';
import AsyncStorage from '@react-native-community/async-storage';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Accordion from 'react-native-collapsible/Accordion';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const SECTIONS = [
    {
        title: 'Instructions for Order',
    },
];

class Order extends Component {

    constructor() {
        super();
        this.state = {
            isModalVisible: false,
            refresh: false,
            data: [],
            activeSections: []
        };
    }

    componentDidMount() {
        this.props.getFoodsChoosen()
        this.setState({
            refresh: true
        })
    }

    // function for adding the number of people taking a specific type of food
    addPeople(food) {
        const { totalFoodPrice } = this.props
        AsyncStorage.getItem('FoodItem' + food.item.food_id).then((token) => {
            const data = JSON.parse(token)
            food.item.quantity = food.item.quantity + 1;
            const TotalFoodPrice = totalFoodPrice + data.price
            food.item.price = food.item.quantity * data.price
            const index = this.props.foodsChoosen.findIndex(
                item => food.item.food_id === item.food_id
            );
            this.props.foodsChoosen[index] = food.item;
            this.props.addNumberPeople(this.props.foodsChoosen, food.item.food_id, TotalFoodPrice)
            this.setState({
                refresh: !this.state.refresh
            })
        })
    }

    // function for subtracting the number of people taking a specific type of food
    minusPeople(food) {
        const { totalFoodPrice } = this.props
        food.item.quantity = food.item.quantity - 1;

        // remove an object from an array when the number of people eating is zero
        if (food.item.quantity === 0) {
            const TotalFoodPrice = totalFoodPrice - food.item.price
            const index = this.props.foodsChoosen.findIndex(
                item => food.item.food_id === item.food_id
            );
            this.props.foodsChoosen.splice(index, 1)
            this.props.minusNumberPeople(this.props.foodsChoosen, 0, TotalFoodPrice)
            this.setState({
                refresh: !this.state.refresh
            })
        } else {
            AsyncStorage.getItem('FoodItem' + food.item.food_id).then((token) => {
                const data = JSON.parse(token)
                const TotalFoodPrice = totalFoodPrice - data.price
                food.item.price = food.item.price - data.price
                const index = this.props.foodsChoosen.findIndex(
                    item => food.item.food_id === item.food_id
                );
                this.props.foodsChoosen[index] = food.item;
                this.props.minusNumberPeople(this.props.foodsChoosen, food.item.food_id, TotalFoodPrice)
                this.setState({
                    refresh: !this.state.refresh
                })
            })
        }
    }

    _updateSections = activeSections => {
        this.setState({ activeSections });
    };


    // function for rendering the foods a person has choosen
    renderItem(food) {

        const { name, price, isSelect, quantity, food_id } = food.item

        const expanded = food_id === this.props.food_id

        const { ListItemStyle, listItemTextStyle, viewPriceStyle, priceStyle, viewTimesStyle } = styles

        if (expanded) {
            return (
                <View>
                    <ListItem style={ListItemStyle}>
                        <Icon
                            name="pluscircle"
                            size={20}
                            onPress={this.addPeople.bind(this, food)}
                            color="#211414"
                        />
                        <Icon
                            name="minuscircle"
                            size={20}
                            onPress={this.minusPeople.bind(this, food)}
                            style={{
                                marginLeft: 5
                            }}
                            color="#211414"
                        />
                        <Text style={listItemTextStyle}>{name}</Text>
                        <View style={viewTimesStyle}>
                            <Text style={priceStyle}>  * {quantity}  </Text>
                        </View>
                        <View style={viewPriceStyle}>
                            <Text style={priceStyle}>Ksh {price}</Text>
                        </View>
                    </ListItem>
                </View>
            )
        } else {
            return (
                <View>
                    <ListItem style={ListItemStyle}>
                        <Icon
                            name="pluscircle"
                            size={20}
                            onPress={this.addPeople.bind(this, food)}
                            color="#211414"
                        />
                        <Icon
                            name="minuscircle"
                            size={20}
                            onPress={this.minusPeople.bind(this, food)}
                            style={{
                                marginLeft: 5
                            }}
                            color="#211414"
                        />
                        <Text style={listItemTextStyle}>{name}</Text>
                        <View style={viewTimesStyle}>
                            <Text style={priceStyle}>  * {quantity}  </Text>
                        </View>
                        <View style={viewPriceStyle}>
                            <Text style={priceStyle}>Ksh {price}</Text>
                        </View>
                    </ListItem>
                </View>
            )
        }
    }

    // function for initiating the next page
    nextComponent() {
        const { foodsChoosen, totalFoodPrice, image, bookingData, instructions } = this.props
        if (instructions) {
            this.props.nextPage(foodsChoosen, totalFoodPrice, this.props.restaurant_id, image, bookingData, instructions)
        } else {
            this.props.nextPage(foodsChoosen, totalFoodPrice, this.props.restaurant_id, image, bookingData, '')
        }
    }

    onOrderInstruct(text) {
        this.props.orderInstructions(text)
    }

    // Function for displaying confirm error
    renderConfirmError() {
        const { error } = this.props
        if (error) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // function for rendering the confirm button
    renderConfirmButton() {
        const { load } = this.props

        if (load) {
            return <Spinner size='large' />
        }

        return (
            <Button onPress={this.nextComponent.bind(this)}>CONFIRM</Button>
        )
    }

    // render accrodion header
    _renderHeader = section => {
        return (
            <Grid>
                <Row size={1}>
                    <Col size={0.9}>
                        <Text style={styles.titleStyle}>
                            {section.title}
                        </Text>

                    </Col>
                    <Col size={0.1}>
                        <Icon
                            name="plus"
                            size={20}
                            color="#211414"
                            style={{
                                paddingTop: 10,
                                paddingBottom: 10
                            }}
                        />
                    </Col>
                </Row>
            </Grid>
        );
    };

    // render accordion content
    _renderContent = section => {
        return (
            <Textarea
                rowSpan={5}
                bordered
                placeholderTextColor="black"
                keyboardType='default'
                maxLength={100000}
                placeholder="................"
                onChangeText={this.onOrderInstruct.bind(this)}
                value={this.props.instructions}
                style={styles.textAreaStyle}
            />
        );
    };

    // function to render accordion
    renderAccord() {
        if (this.props.foodsChoosen.length) {
            return (
                <Accordion
                    sections={SECTIONS}
                    activeSections={this.state.activeSections}
                    renderHeader={this._renderHeader}
                    renderContent={this._renderContent}
                    underlayColor='#41424D'
                    onChange={this._updateSections}
                />
            )
        }
    }

    render() {
        const {
            contentStyle, headerStyle, descriptionTextStyle,
            descriptionViewStyle, bottomLineStyle, subtotalStyle,
            buttonStyle, downTotalStyle, downStyle, radiBadgeStyle, totalStyle,
        } = styles

        const { totalFoodPrice, foodsChoosen, image } = this.props

        return (
            <Container>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' style={headerStyle} >
                    <Right>
                        <TouchableOpacity style={{ top: '3%' }} >
                            <RadiBadge style={{ width: PHONE_WIDTH / 3.2 }}>
                                <Text style={radiBadgeStyle}>
                                    <Icon style={radiBadgeStyle} active name="shoppingcart" />
                                    KSH {totalFoodPrice}
                                </Text>
                            </RadiBadge>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content style={contentStyle}>
                    <View style={{
                        padding: 20,
                    }}>
                        <Text style={descriptionTextStyle}>Your Order</Text>
                        <View style={descriptionViewStyle}>
                            <FlatList
                                data={foodsChoosen}
                                renderItem={item => this.renderItem(item)}
                                keyExtractor={item => item.food_id.toString()}
                                extraData={this.state.refresh}
                            />
                        </View>
                    </View>
                    {this.renderAccord()}
                    <View style={{ marginTop: 70 }}>
                        <Right>
                            <Icon style={downStyle} active name="down" />
                        </Right>
                        <Right>
                            <Text style={subtotalStyle}>
                                Subtotal
                            </Text>
                        </Right>
                        <View style={bottomLineStyle} />
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Right>
                            <Text style={downStyle}>
                                Tax Amount
                            </Text>
                        </Right>
                        <Right>
                            <Text style={subtotalStyle}>
                                3%
                            </Text>
                        </Right>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Right>
                            <Text style={downTotalStyle}>
                                Total
                            </Text>
                        </Right>
                        <Right>
                            <Text style={totalStyle}>
                                {totalFoodPrice}
                            </Text>
                        </Right>
                    </View>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    {this.renderConfirmError()}
                                    <View style={buttonStyle}>
                                        {this.renderConfirmButton()}
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    contentStyle: {
        padding: 0,
    },
    textHeaderStyle: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white',
        paddingTop: 100,
        paddingLeft: 10
    },
    descriptionTextStyle: {
        alignContent: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 15,
        color: '#211414',
        fontFamily: "Helvetica"
    },
    descriptionViewStyle: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row'
    },
    buttonStyle: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 100,
        marginTop: 30,
        paddingTop: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    headerStyle: {
        backgroundColor: '#2A2E43',
        borderRadius: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 0,
        elevation: 0,
        height: 100
    },
    listItemTextStyle: {
        color: '#211414',
        marginLeft: 30,
        width: PHONE_WIDTH / 5
    },
    ListItemStyle: {
        width: PHONE_WIDTH - 5
    },
    subtotalStyle: {
        color: 'black',
        fontSize: 15,
        marginLeft: 230,
        marginTop: -17
    },
    totalStyle: {
        color: 'black',
        fontSize: 15,
        marginLeft: 230,
        marginTop: -17,
        fontWeight: 'bold'
    },
    downStyle: {
        color: 'black',
        fontSize: 15
    },
    downTotalStyle: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold'
    },
    bottomLineStyle: {
        borderBottomColor: '#e1e5ed',
        borderBottomWidth: 1,
        marginTop: 10
    },
    radiBadgeStyle: {
        color: 'white',
        marginLeft: 7
    },
    viewPriceStyle: {
        backgroundColor: '#401E63',
        height: 30,
        width: PHONE_WIDTH / 5,
        marginTop: 10
    },
    priceStyle: {
        color: 'white',
        alignSelf: 'center',
        marginTop: 5
    },
    viewTimesStyle: {
        backgroundColor: '#641E16',
        height: 30,
        marginLeft: PHONE_WIDTH / 16,
        width: PHONE_WIDTH / 10,
        marginTop: 10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
    cardStyle: {
        width: PHONE_WIDTH,
        height: PHONE_HEIGHT / 25,
        marginLeft: PHONE_WIDTH / 50
    },
    textAreaStyle: {
        fontSize: 15,
        margin: 10,
        backgroundColor: 'white',
        borderRadius: 10
    },
    titleStyle: {
        fontSize: 20,
        paddingLeft: 25,
        color: 'black',
        paddingTop: 10,
        paddingBottom: 10
    },
})

const RadiBadge = styled(Badge)`
    background-color:black;
    padding-top: 5px;
    margin-top: -30px;
`;

const mapStateToProps = ({ Orders }) => {
    const { foodsChoosen, food_id, totalFoodPrice, load, error, instructions } = Orders
    return { foodsChoosen, food_id, totalFoodPrice, load, error, instructions }
}

export default connect(mapStateToProps, {
    getFoodsChoosen, addNumberPeople,
    minusNumberPeople, nextPage, orderInstructions
})(Order);