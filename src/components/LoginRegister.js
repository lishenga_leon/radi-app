import { Container, Content, Header } from 'native-base';
import { Button } from './common'
import { StyleSheet, Platform, Dimensions } from 'react-native';
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
  ? Dimensions.get("window").height
  : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
  ? Dimensions.get("window").width
  : ExtraDimensions.getRealWindowWidth();

class LoginRegister extends Component {

    render() {

        const { 
            contentStyle, headerStyle, descriptionTextStyle, 
            descriptionViewStyle, forDeliverytStyle, buttonContainer
        } = styles

        const { orderData } = this.props

        return (
            <Container>
                <Content style={contentStyle}>
                    <Header style={headerStyle} />
                    <View style={{
                        padding: 20,
                    }}>
                        <Text style={descriptionTextStyle}>Already Registered</Text>
                        <Text style={forDeliverytStyle}>Login to complete your order</Text>
                        <View style={descriptionViewStyle}>
                            <View>
                                <View style={buttonContainer}>
                                    <Button onPress={()=>Actions.loginafterorder({ orderData: orderData })}>
                                        LOGIN
                                    </Button>
                                </View>
                            </View>
                        </View>
                        <View style={{ marginTop: PHONE_HEIGHT/4 }}>
                            <Text style={descriptionTextStyle}>New User?  Welcome</Text>
                            <Text style={forDeliverytStyle}>Click to create an Account</Text>
                        </View>
                        <View style={descriptionViewStyle}>
                            <View>
                                <View style={buttonContainer}>
                                    <Button onPress={()=>Actions.loginafterorder({ orderData: orderData })}>
                                        REGISTER
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    forDeliverytStyle:{
        alignSelf: 'center',
        fontSize: 15,
        marginTop: 15,
        color: '#211414',
        fontFamily: "Helvetica" 
    },
    contentStyle:{
        padding: 0,
        height: PHONE_HEIGHT
    },
    descriptionTextStyle:{
        alignSelf: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 15,
        color: '#211414',
        fontFamily: "Helvetica" 
    },
    descriptionViewStyle:{
        flex: 1, 
        alignSelf: 'stretch', 
        flexDirection: 'row' 
    }, 
    headerStyle:{
        backgroundColor: '#2A2E43',
        borderRadius: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 0,
        elevation: 0,
        height: 100
    },
    buttonContainer:{
        backgroundColor: '#23273A',
        paddingVertical: 15,
        width: PHONE_WIDTH/1.5,
        marginLeft: PHONE_WIDTH/6,
        marginTop:25,
        paddingTop:15,
        paddingBottom:15,
        alignSelf: 'center',
        borderRadius: 10
    }

})

export default LoginRegister;