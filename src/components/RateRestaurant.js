import React, { Component } from 'react';
import { StyleSheet, Image, View, Text, Dimensions, Platform } from 'react-native'
import { Container, Card, Content, Row, Grid } from 'native-base';
import { Button, Spinner } from './common'
import StarRating from 'react-native-star-rating';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { apiIp } from '../Config';
import { connect } from 'react-redux';
import { userNameRateRestaurant, restaurantRate, rateRestaurant } from '../Actions';
import AsyncStorage from '@react-native-community/async-storage';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class RateRestaurant extends Component{

    componentDidMount(){
        AsyncStorage.getItem('user').then((user)=>{
            const data = JSON.parse(user)
            this.props.userNameRateRestaurant(data.data.fullname)
        })
    }
     
    // function for initiating redux change for counting the stars
    onStarRatingPress(rating) {
        this.props.restaurantRate(rating)
    }

    // Function for displaying rate restaurant error
    renderRateError(){
        const { error } = this.props
        if(error){
            return(
                <View>
                    <Text style={styles.errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // Function for initiating rate restaurant redux logic
    onButtonPressRate(){
        const { starCount, orderData } = this.props;
        const { restaurant_id } = orderData

        const body={
            restaurant_id:restaurant_id,
            rating: starCount  
        }
        this.props.rateRestaurant(body) 
    }

    // Function for displaying rate restaurant button
    renderButtonRate(){
        const { load } = this.props

        if(load){
            return <Spinner size='large' />
        }

        return(
            <Button onPress={()=> this.onButtonPressRate()}>
                SUBMIT RATING
            </Button>
        )
    }

    render(){
        const { 
            cardStyle, buttonContainer, bottomLineStyle, viewStyle,
            StarStyle, viewThumbnail, logo, textStyle, textStyles, nameStyle
        } = styles

        const { starCount, fullname, orderData } = this.props
        const { restaurant_image, totalFoodPrice } = orderData

        return(
            <Container style={{ height: PHONE_HEIGHT/50 }}>
                <Content>
                    <Grid>
                        <Row size={1}>
                            <Card style={cardStyle}>
                                <View style={viewThumbnail}>
                                    <Image 
                                        style={logo}
                                        source={{
                                            uri: apiIp+ restaurant_image
                                        }}
                                    />
                                    <View style={viewStyle}>
                                        <Text style={nameStyle}>
                                            {fullname}
                                        </Text>
                                    </View>
                                </View>
                                <View style={bottomLineStyle}/>
                                <View style={viewStyle}>
                                    <Text style={textStyle}>
                                        KSH {totalFoodPrice}
                                    </Text>
                                </View>
                                <View style={bottomLineStyle}/>
                                <View style={viewStyle}>
                                    <Text style={textStyles}>
                                        Rate Restaurant 
                                    </Text>
                                </View>
                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'red'}
                                    starSize={40}
                                    containerStyle={StarStyle}
                                />
                                {this.renderRateError()}
                                <View style={buttonContainer}>
                                    {this.renderButtonRate()}
                                </View>
                            </Card>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
};

const styles = StyleSheet.create({
    viewThumbnail:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    viewStyle:{
        height: 50, 
        alignContent: 'center', 
        justifyContent: 'center', 
        marginLeft: PHONE_WIDTH/3 
    },
    logo:{
        width: 100,
        height: 100,
        alignSelf: 'center',
        marginTop: -40,
        borderRadius: 50,
    },
    textStyle:{
        color: 'white',
        fontSize: 20,
    },
    nameStyle:{
        color: 'white',
        fontSize: 15,
        marginLeft: -110
    },
    textStyles:{
        color: 'white',
        fontSize: 15,
    },
    buttonContainer:{
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: PHONE_WIDTH/3,
        marginTop:15,
        marginBottom: 10,
        paddingTop:15,
        paddingBottom:15,
        alignSelf: 'center',
        borderRadius: 10
    },
    cardStyle:{
        marginTop: 100,
        height: 480,
        marginLeft: 25,
        width: PHONE_WIDTH-50,
        borderRadius: 10,
        backgroundColor: '#23273A'
    },
    bottomLineStyle:{
        borderBottomColor: '#FFFFFF',
        borderBottomWidth: 1,
        marginTop: 10
    },
    StarStyle:{
        width: 30,
        alignSelf: 'center', 
        justifyContent: 'center',
        margin: 10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
})

const mapStateToProps = ({ raterestaurant }) => {
    const { fullname, starCount, load, error } = raterestaurant;
    return { fullname, starCount, load, error }
}

export default connect(mapStateToProps, {
    userNameRateRestaurant, restaurantRate, rateRestaurant
})(RateRestaurant);