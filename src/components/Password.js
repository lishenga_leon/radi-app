import React, { Component } from 'react';
import {
    StyleSheet, View, TouchableWithoutFeedback,
    StatusBar, Keyboard, Text, TextInput, Platform, Dimensions
} from 'react-native';
import { Button, Spinner } from '../components/common';
import { Container, Content, Icon, Grid, Col, Row } from 'native-base';
import { connect } from 'react-redux';
import { loginUser, getPasswordCompoChange } from '../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class Password extends Component {

    state = {
        icon: 'eye-off',
        password: true
    }

    // function for initiating a redux change when typing the password
    onPasswordChange(text) {
        this.props.getPasswordCompoChange(text)
    }

    // function for login a user using redux
    userLogin() {

        const { email } = this.props.userDetails

        const data = {
            email: email.toLowerCase(),
            password: this.props.password,
        }

        this.props.loginUser(data)
    }

    // Function for displaying create error
    renderSubmitError() {
        const { error } = this.props
        if (error) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // Function for displaying submit button
    renderButtonSubmit() {
        const { loading } = this.props

        if (loading) {
            return <Spinner size='large' />
        }

        return (
            <Button onPress={() => this.userLogin()}>SUBMIT</Button>
        )
    }

    // change icon of eye
    _changeIcon() {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            password: !prevState.password
        }))
    }

    render() {

        const {
            container, infoContainer, buttonContainer, inputStyle,
            headerView, viewThumbnail, headingStyle, rowStyle
        } = styles;

        const { password } = this.props

        return (
            <Container style={{ backgroundColor: '#798496' }}>
                <Content style={container}>
                    <View style={headerView}>
                        <View style={viewThumbnail}>
                            <Text style={headingStyle}>Provide Password</Text>
                        </View>
                    </View>
                    <StatusBar barStyle="light-content" />
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={infoContainer}>
                            <Grid >
                                <Row style={rowStyle}>
                                    <Col size={0.8}>
                                        <TextInput
                                            placeholder='Password'
                                            textContentType='password'
                                            keyboardType='default'
                                            style={inputStyle}
                                            placeholderTextColor='black'
                                            autoCompleteType='password'
                                            secureTextEntry={this.state.password}
                                            onChangeText={this.onPasswordChange.bind(this)}
                                            value={password}
                                        />
                                    </Col>
                                    <Col size={0.2}>
                                        <Icon name={this.state.icon} onPress={() => this._changeIcon()} style={{ alignSelf: 'center', marginTop: 15 }} />
                                    </Col>
                                </Row>
                            </Grid>
                            {this.renderSubmitError()}
                            <View style={buttonContainer}>
                                {this.renderButtonSubmit()}
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    viewThumbnail: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headingStyle: {
        fontSize: 30,
        color: 'white'
    },
    container: {
        flex: 1,
        backgroundColor: '#352644',
        flexDirection: 'column'
    },
    infoContainer: {
        left: 0,
        right: 0,
        bottom: 0,
        height: 300,
        padding: 20,
    },
    buttonContainer: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 100,
        marginTop: 30,
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 10,
        alignSelf: 'center',
    },
    textStyle: {
        color: 'white',
        fontSize: 17,
        margin: 20
    },
    headerView: {
        height: 200,
        marginTop: PHONE_HEIGHT / 200
    },
    inputStyle: {
        height: 60,
        color: 'black',
        marginBottom: 20,
        fontSize: 17,
        paddingHorizontal: 10,
        borderColor: 'white',
        borderRadius: 10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
    rowStyle: {
        backgroundColor: '#efe8f7',
        height: 60,
        paddingHorizontal: 10,
        borderRadius: 10,
        marginBottom: 40,
    },
})

const mapStateToProps = ({ login, phone }) => {
    const { error, loading } = login;
    const { password } = phone;
    return { error, loading, password }
}


export default connect(mapStateToProps, {
    loginUser, getPasswordCompoChange
})(Password);