import React, { Component } from 'react';
import {
    StyleSheet, View, Platform,
    TouchableWithoutFeedback, StatusBar,
    Image, Keyboard, Text, Dimensions
} from 'react-native';
import { Input, Button, Spinner } from '../components/common';
import { Container, Content } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import {
    getUserProfPic, profileEmailChanged, profileFullnameChanged, getUpdateToken,
    profilePassChanged, profilePhoneNumberChanged, updateUser, profileUserId
} from '../Actions';
import AsyncStorage from '@react-native-community/async-storage';
import ExtraDimensions from 'react-native-extra-dimensions-android';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class Profile extends Component {

    componentDidMount(){
        
        AsyncStorage.getItem('user').then((user)=>{
            const data = JSON.parse(user)
            this.props.getUpdateToken(data.data.token)
            this.props.profileUserId(data.data.id)
            this.props.profileEmailChanged(data.data.email)
            this.props.profileFullnameChanged(data.data.fullname)
            this.props.profilePhoneNumberChanged(data.data.msisdn)
            AsyncStorage.getItem('profpic').then((profpic)=>{
                const pic = JSON.parse(profpic)
                this.props.getUserProfPic(pic)
            })
        })
    }

    onEmailAddressChange(text) {
        this.props.profileEmailChanged(text)
    }

    onFullNameChange(text) {
        this.props.profileFullnameChanged(text)
    }

    onPhoneChange(text) {
        this.props.profilePhoneNumberChanged(text)
    }

    onPasswordChange(text) {
        this.props.profilePassChanged(text)
    }

    // Function for displaying update error
    renderUpdateError(){
        const { errors } = this.props
        if(errors){
            return(
                <View>
                    <Text style={styles.errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // Function for initiating update redux logic
    onButtonPressUpdate(){
        const { fullname, password, msisdn, email, userId, token } = this.props;

        const body={

            id: userId,
        
            fullname:fullname,
        
            email:email.toLowerCase(),
        
            msisdn:msisdn,
        
            password:password, 

            token: token
                
        }

        this.props.updateUser(body)
        this.componentDidMount()
    }

    // Function for displaying update button
    renderButtonUpdate(){
        const { loader} = this.props

        if(loader){
            return <Spinner size='large' />
        }

        return(
            <Button onPress={this.onButtonPressUpdate.bind(this)}>
                UPDATE
            </Button>
        )
    }

    // function for getting the profile picture of a user
    pickImageHandler = () => {
        ImagePicker.showImagePicker({
            title: "Pick an Image",
            maxWidth: 800,
            maxHeight: 600
        }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                AsyncStorage.setItem('profpic', JSON.stringify(res.uri))
                this.props.getUserProfPic(res.uri)
            }
        });
    }

    // function for rendering profile picture
    renderProfPic(){
        const { logo, noImageStyle } = styles
        const { profpic } = this.props
        if(profpic){
            return(
                <TouchableWithoutFeedback onPress={() => this.pickImageHandler()}>
                    <Image
                        style={logo}
                        source={{
                            uri: profpic
                        }}
                    />
                </TouchableWithoutFeedback>
            )
        }else{
            return(
                <View style={noImageStyle}>
                    <TouchableWithoutFeedback onPress={() => this.pickImageHandler()}>
                        <Text style={{ color: 'black' }}>Click to upload profile picture</Text>
                    </TouchableWithoutFeedback>
                </View>
            )
        }
    }

    render() {

        const { container, infoContainer, viewThumbnail, buttonContainer, logo, headerView } = styles;
        const { password, msisdn, fullname, email, profpic } = this.props

        return (
            <Container style={{ backgroundColor: '#41424D' }}>
                <View style={headerView}>
                    <View style={viewThumbnail}>
                        {this.renderProfPic()}
                    </View>
                </View>
                <Content style={container}>
                    <StatusBar barStyle="light-content" />
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View>
                            <View style={infoContainer}>
                                <Input
                                    placeholder='Full Name'
                                    returnKeyType='next'
                                    keyboardType='default'
                                    onChangeText={this.onFullNameChange.bind(this)}
                                    value={fullname}
                                />
                                <Input
                                    placeholder='Email Address'
                                    returnKeyType='next'
                                    keyboardType='email-address'
                                    onChangeText={this.onEmailAddressChange.bind(this)}
                                    value={email}
                                />
                                <Input
                                    placeholder='Phone Number'
                                    returnKeyType='next'
                                    keyboardType='numeric'
                                    onChangeText={this.onPhoneChange.bind(this)}
                                    value={msisdn}
                                />
                                <Input
                                    placeholder='Password'
                                    returnKeyType='next'
                                    keyboardType='default'
                                    secureTextEntry={true}
                                    onChangeText={this.onPasswordChange.bind(this)}
                                    value={password}
                                />
                                {this.renderUpdateError()}
                                <View style={buttonContainer}>
                                    {this.renderButtonUpdate()}
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    viewThumbnail: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        width: 100,
        height: 100,
        alignSelf: 'center',
        marginTop: 40,
        borderRadius: 50,
    },
    container: {
        flex: 1,
        backgroundColor: '#352644',
        flexDirection: 'column'
    },
    infoContainer: {
        left: 0,
        right: 0,
        bottom: 0,
        padding: 20,
    },
    buttonContainer: {
        backgroundColor: '#ec407a',
        paddingVertical: 15,
        width: PHONE_WIDTH/5,
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        alignSelf: 'center',
        borderRadius: 50
    },
    textStyle: {
        height: 60,
        backgroundColor: 'white',
        color: 'black',
        marginBottom: 20,
        fontSize: 17,
        paddingHorizontal: 10,
        paddingTop: 20
    },
    headerView: {
        height: 200
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
    noImageStyle:{
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 100,
        height: 100,
        backgroundColor: '#fff',
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 40,
    }
})

const mapStateToProps = ({ profile }) => {
    const { loader, errors, password, msisdn, fullname, email, profpic, userId, token } = profile;
    return { loader, errors, password, msisdn, fullname, email, profpic, userId, token }
}


export default connect(mapStateToProps, {
    getUserProfPic, profileEmailChanged, profileFullnameChanged,getUpdateToken,
    profilePassChanged, profilePhoneNumberChanged, updateUser, profileUserId
})(Profile);