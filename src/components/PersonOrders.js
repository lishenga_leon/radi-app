import {
    Container, Content, Grid,
    Row, Col, Card, Header
} from 'native-base';
import {
    StyleSheet, TouchableOpacity, FlatList,
    Dimensions, Image, Text, Platform
} from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { getAllOrdersPersonMade } from '../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import Foods from './common/PersonOrders/Foods';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');

class PersonOrders extends Component {

    componentDidMount() {
        this.props.getAllOrdersPersonMade()
    }

    renderItem = (item) => {
        const { order, content, restaurant } = item.item
        const { contentNameTextStyles, cardStyle } = styles
        return (
            <Card style={cardStyle}>
                <TouchableOpacity onPress={() => Actions.joint({ restaurant_id: restaurant.id })}>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Foods foods={content} />
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <Text style={contentNameTextStyles}>
                                        Restaurant Name: 
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                            { restaurant.name}
                                        </Text>
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                        Type: 
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                            { order.order_type}
                                        </Text>
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                        Food Instructions: 
                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                            { order.food_instructions}
                                        </Text>
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                        Date: 
                                        <Text style={{ fontWeight: 'bold', fontSize: 8 }}>
                                            { Date(order.expected_time)}
                                        </Text>
                                    </Text>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </TouchableOpacity>
            </Card>
        )
    }

    render() {

        const { contentStyle, headerStyle, textHeaderStyle } = styles

        const { orders } = this.props

        console.log(orders)

        return (
            <Container style={{ backgroundColor: '#352644', }}>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                    <Text style={textHeaderStyle}>ORDERS</Text>
                </Header>
                <Content style={contentStyle}>
                    <FlatList
                        data={orders}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.order.id}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    textHeaderStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        marginTop: PHONE_HEIGHT / 50,
        alignContent: 'center',
        alignSelf: 'center',
    },
    cardStyle: {
        alignSelf: 'center',
        borderRadius: 10,
        width: win.width / 1.5
    },
    contentStyle: {
        padding: 0,
        marginTop: 40
    },
    contentNameTextStyles: {
        marginTop: 20,
        fontSize: 10,
        marginLeft: 20,
        marginBottom: 10,
    },
    headerStyle: {
        height: 60
    },
})

const mapStateToProps = ({ personorders }) => {
    const { orders } = personorders;
    return { orders };
};

export default connect(mapStateToProps, {
    getAllOrdersPersonMade
})(PersonOrders);