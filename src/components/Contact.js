import { Container, Content, Header, Grid, Col, Row, Card } from 'native-base';
import { StyleSheet, Text, View, Platform, Dimensions } from 'react-native';
import React, { Component } from 'react';
import { TextInput } from 'react-native-paper';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import axios from 'axios';
import { apiIp } from '../Config';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');

class Contact extends Component {

    constructor(props) {
        super(props)
        this.state = {
            contacts: null,
        }
    }

    componentDidMount() {

        axios.get(apiIp + '/RadiContact/getRadiContact/').then(

            response => {

                if (JSON.stringify(response.data.status_code) == 500) {

                    this.setState({
                        contacts: {}
                    })

                } else if (JSON.stringify(response.data.status_code) == 200) {

                    this.setState({
                        contacts: response.data.data
                    })

                }

            }

        ).catch(() => console.log(error))
    }

    render() {

        const {
            headerStyle, contentStyle, mapStyle,
            textHeaderStyle, inputStyles, infoContainer
        } = styles
        if (this.state.contacts == null) {
            return (
                <Container>
                    <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                        <Text style={textHeaderStyle}>Contact Us</Text>
                    </Header>
                    <Content style={contentStyle}>
                        <Grid style={{ marginTop: 40 }}>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card transparent >
                                        <View style={mapStyle}>

                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>

                </Container>

            )
        }

        const { name, msisdn, email, box_office, street, website, town, country } = this.state.contacts

        return (
            <Container>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                    <Text style={textHeaderStyle}>Contact Us</Text>
                </Header>
                <Content style={contentStyle}>
                    <View style={infoContainer}>
                        <TextInput
                            label="Name"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={name}
                        />
                        <TextInput
                            label="Email Address"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={email}
                        />
                        <TextInput
                            label="Phone Number"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={msisdn}
                        />
                        <TextInput
                            label="P.O BOX"
                            editable={false}
                            disabled={true}
                            editable={false}
                            style={inputStyles}
                            value={box_office}
                        />
                        <TextInput
                            label="Street Name"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={street}
                        />
                        <TextInput
                            label="Website"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={website}
                        />
                        <TextInput
                            label="Town"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={town}
                        />
                        <TextInput
                            label="Country"
                            editable={false}
                            disabled={true}
                            style={inputStyles}
                            value={country}
                        />
                    </View>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: {
        height: PHONE_HEIGHT / 5,
        backgroundColor: '#352644'
    },
    textHeaderStyle: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white',
        padding: 30,
        alignContent: 'center',
        alignSelf: 'center',
    },
    contentStyle: {
        padding: 0,
        backgroundColor: '#34183E',
    },
    mapStyle: {
        height: PHONE_HEIGHT / 4,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 10
    },
    inputStyles: {
        height: PHONE_HEIGHT/12,
        backgroundColor: '#efe8f7',
        color: 'black',
        marginBottom: 20,
        fontSize: 17,
        paddingHorizontal: 10,
        borderColor: 'white',
    },
    infoContainer: {
        left: 0,
        right: 0,
        bottom: 0,
        height: PHONE_HEIGHT,
        padding: 20,
    },

})

export default Contact;