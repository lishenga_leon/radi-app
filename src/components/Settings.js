import React, { Component } from 'React';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, Platform, StatusBar }  from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Content, Grid, Card, Col, Row, Container, Header } from 'native-base';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');


class Settings extends Component {

    home(){
        Actions.drawer()
    }

    render(){

        const { headerStyle, textStyle, textHeaderStyle, mapStyle } = styles;

        return (
            <Container>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle}>
                    <Text style={textHeaderStyle}>Settings</Text>
                </Header>
                <Content  style={{ flex: 2, backgroundColor: '#41424D' }}>
                    <Grid style={{ padding: 20 }}>
                        <Row size={1}>
                            <Col size={1}>
                                <Card style={{ backgroundColor: '#636464' }}>
                                    <View style={mapStyle}>
                                        <TouchableOpacity onPress={() => Actions.profile()}>
                                            <Text style={textStyle}>Profile</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card style={{ backgroundColor: '#636464' }}>
                                    <View style={mapStyle}>
                                        <TouchableOpacity onPress={() => Actions.messages()} >
                                            <Text style={textStyle}>Messages</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card style={{ backgroundColor: '#636464' }}>
                                    <View style={mapStyle}>
                                        <TouchableOpacity onPress={() => Actions.transactions()} >
                                            <Text style={textStyle}>Transactions</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card style={{ backgroundColor: '#636464' }}>
                                    <View style={mapStyle}>
                                        <TouchableOpacity onPress={() => Actions.editpay()} >
                                            <Text style={textStyle}>Payment Options</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card style={{ backgroundColor: '#636464' }}>
                                    <View style={mapStyle}>
                                        <TouchableOpacity onPress={() => Actions.terms()} >
                                            <Text style={textStyle}>Terms and Conditions</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card style={{ backgroundColor: '#636464' }}>
                                    <View style={mapStyle}>
                                        <TouchableOpacity onPress={() => Actions.appinfo()}>
                                            <Text style={textStyle}>App Info</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
};

const styles = StyleSheet.create({
    textThumbnail:{
        fontSize: 15,
        color: '#fff',
        marginTop: 10
    },
    textStyle:{
        fontSize: 18,
        paddingLeft: 70,
        color: 'white'
    },
    mapStyle:{
        height: 30,
        margin: 10,
        borderRadius: 10,
        borderColor: 'black'
    },
    headerStyle:{
        borderRadius: 0, 
        backgroundColor: '#352644',
        height: PHONE_HEIGHT/5.5
    },
    textHeaderStyle:{
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        paddingTop: 50,
        alignContent: 'center',
        alignSelf: 'center',
    },

})

export default Settings;