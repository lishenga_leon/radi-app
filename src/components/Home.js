import React, { Component } from 'react';
import Post from './common/Home/Post'
import Swiper from 'react-native-swiper';
import Maps from './Maps';

class Home extends Component {

    render() {
        return (
            <Swiper ref="swiper" showsPagination={false} loop={true}>
                <Post/>
                <Maps/>
            </Swiper>
        );
    }
}

export default Home;
