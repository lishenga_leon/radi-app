import React, { Component } from 'react';
import {
    StyleSheet, Platform, Image,
    PermissionsAndroid, FlatList, Dimensions,
} from 'react-native'
import styled from 'styled-components';
import {
    Container, Card, CardItem, Text, Header,
    Content, Grid, Col, Row, Left, Button
} from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { skipLogInCreateAccount } from '../Actions';
import { apiIp } from '../Config';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import SplashScreen from 'react-native-splash-screen';
import DeviceInfo from 'react-native-device-info';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();


const PHONE_HEIGHT_ANDROID = ExtraDimensions.getRealWindowHeight()

const PHONE_HEIGHT_IOS = Dimensions.get("window").height


class FirstPage extends Component {

    constructor() {
        super()
        this.state = {
            page: 1,
            refresh: false,
        }
    }

    componentDidMount() {

        const uniqueId = DeviceInfo.getUniqueID();
        try {
            const granted = PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Radi App Location Permission',
                    message: 'Radi App would like to access your location',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted) {
                this.props.skipLogInCreateAccount(uniqueId)
                SplashScreen.hide()
            } else {
                this.props.skipLogInCreateAccount(uniqueId)
                SplashScreen.hide()
            }
        } catch (err) {
            this.props.skipLogInCreateAccount(uniqueId)
            SplashScreen.hide()
        }
    }

    // function for rendering the posts of users
    renderItem = (posters) => {
        const { captionStyle, imageStyle, shareStyle, thumbsStyle } = styles
        const {
            id, restaurant_id, likes, like_person_id, like_id, selectedClass,
            description, gallery, created_at, updated_at, pictureGallery
        } = posters.item

        const { thumbsStyler, likesStyler } = styles

        return (
            <Grid>
                <Row size={1}>
                    <Col size={1}>
                        <Card transparent >
                            <CardItem cardBody>
                                <Image source={{ uri: apiIp + pictureGallery }} style={styles.imageStyle} />
                            </CardItem>
                            <Text style={captionStyle}>{description}</Text>
                            <CardItem>
                                <Left style={{ marginLeft: 20, marginTop: -10 }} >
                                    <Button transparent>
                                        <Icon style={shareStyle} name="share" />
                                    </Button>
                                    <Button transparent>
                                        <Text style={[likesStyler]}>{likes}</Text>
                                        <Image source={require('../../images/heart-outline.png')} style={[selectedClass]} />
                                    </Button>
                                </Left>
                            </CardItem>
                        </Card>
                    </Col>
                </Row>
            </Grid>
        )

    }


    render() {
        const { post } = this.props
        return (
            <Container>
                <Header transparent={true} iosBarStyle='dark-content' androidStatusBarColor='#4f6d7a' rounded  style={{display:'none'}}/>
                <TextGrid style={{ top: PHONE_HEIGHT / 13  }}>
                    <TextCol>
                        <Text style={{ textAlign: 'center', fontSize: 30 }}>Radi</Text>
                        <Text style={{ fontSize: 15, textAlign: 'center', marginTop: 20 }}>Find, Tap and Navigate joint around you</Text>
                    </TextCol>
                </TextGrid>
                <ImageGrid style={{ marginLeft: PHONE_WIDTH / 4, top: PHONE_HEIGHT / 4 }}>
                    <HalfCol>
                        <Image style={{ height: PHONE_HEIGHT / 6, width: PHONE_WIDTH / 4, marginLeft: PHONE_WIDTH / 8 }} source={require('../../images/RADILOGO.png')} />
                    </HalfCol>
                </ImageGrid>
                <Content style={{
                    padding: 0,
                    opacity: 0.2
                }}>
                    <FlatList
                        data={post}
                        renderItem={this.renderItem}
                        keyExtractor={(item, posters) => posters.toString()}
                    />
                </Content>
                <CartGrid style={{ marginLeft: PHONE_WIDTH / 4, top: PHONE_HEIGHT / 1.6 }}>
                    <HalfCol>
                        <CancelButton onPress={() => Actions.login()} block warning >
                            <Text style={{ color: 'white' }}>Sign Up</Text>
                        </CancelButton>
                    </HalfCol>
                </CartGrid>
                <LoginGrid style={{ marginLeft: PHONE_WIDTH / 4, top: PHONE_HEIGHT / 1.37 }}>
                    <HalfCol>
                        <LoginButton onPress={() => Actions.login()} block warning >
                            <Text style={{ color: 'white' }}>Login</Text>
                        </LoginButton>
                    </HalfCol>
                </LoginGrid>
                <SkipGrid style={{ marginLeft: PHONE_WIDTH / 4, top: PHONE_HEIGHT / 1.2 }}>
                    <HalfCol>
                        <SkipButton onPress={() => Actions.drawer()} block warning >
                            <Text style={{ color: 'black' }}>Skip</Text>
                        </SkipButton>
                    </HalfCol>
                </SkipGrid>
            </Container>
        )
    }
}

const mapStateToProps = ({ FirstPage }) => {
    const { post } = FirstPage;
    return { post };
};

const styles = StyleSheet.create({
    mainheaderStyle: {
        height: 100,
        backgroundColor: '#001E24',
        marginTop: -10,
        borderRadius: 0,
        borderColor: '#001E24'
    },
    headerStyle: {
        borderRadius: 0,
        alignSelf: 'flex-start',
        height: null
    },
    textHeaderStyle: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white',
        paddingLeft: 10
    },
    imageBackground: {
        width: '100%',
        marginTop: -10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
    navBarStyle: {
        height: PHONE_HEIGHT / 6,
        backgroundColor: '#001E24'
    },
    cameraIconStyle: {
        fontSize: 50,
        color: '#848991',
        marginBottom: 10,
        alignSelf: 'center'
    },
    captionStyle: {
        paddingLeft: 40,
        paddingTop: 20
    },
    thumbsStyle: {
        marginTop: -5,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,
        width: PHONE_WIDTH / 15,
    },
    thumbsStyler: {
        marginTop: -5,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,
        width: PHONE_WIDTH / 15,
    },
    likesStyler: {
        color: 'black',
        fontSize: 18
    },
    shareStyle: {
        color: 'black',
        fontSize: 22
    },
    imageStyle: {
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 1.3 : PHONE_HEIGHT_ANDROID / 1.3,
        width: PHONE_WIDTH,
        flex: 1,
    },
    topImageStyle1: {
        height: 180,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle2: {
        height: 90,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle3: {
        height: 85,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    selected: {
        color: 'red',
        fontSize: 25
    },
})

const TextGrid = styled(Grid)`
    position: absolute;
`;

const ImageGrid = styled(Grid)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 10;
`;

const CartGrid = styled(Grid)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 10;
`;

const CancelButton = styled(Button)`
    border-radius: 10px;
    color:#ffffff;
    background-color: #161B30;
    width:70%;
`;

const LoginGrid = styled(Grid)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 10;
`;

const LoginButton = styled(Button)`
    border-radius: 10px;
    color:#ffffff;
    background-color: #7B241C;
    width:70%;
`;

const SkipGrid = styled(Grid)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 10;
`;

const SkipButton = styled(Button)`
    border-radius: 10px;
    color:#ffffff;
    background-color: #F1F5F8;
    width:70%;
`;

const HalfCol = styled(Col)`
    width:70%;
    margin:5px;
`;

const TextCol = styled(Col)`
    width:100%;
`;

export default connect(mapStateToProps, {
    skipLogInCreateAccount
})(FirstPage);