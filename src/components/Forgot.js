import React, {Component} from 'react';
import { StyleSheet, View, TouchableWithoutFeedback, StatusBar, Keyboard, Text, TextInput } from 'react-native';
import { Button, Spinner } from '../components/common';  
import { Container, Content } from 'native-base';
import { connect } from 'react-redux';
import { forgotEmailAddressChange, forgotLogic } from '../Actions';

class Forgot extends Component{

    // function for changing the text input for email address
    changeEmailAddress(text){
        this.props.forgotEmailAddressChange(text)
    }

    // Function for displaying forgot password error
    renderForgotPasswordError(){
        const { error } = this.props
        if(error){
            return(
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // Function for initiating forgot password redux logic
    onButtonPressForgotPassword(){
        const { email } = this.props;

        if(email){
            const data = {
                email: email.toLowerCase(),
            }
            this.props.forgotLogic( data )
        }   
    }

    // Function for displaying forgot password button
    renderButtonForgotPassword(){
        const { load } = this.props

        if(load){
            return <Spinner size='large' />
        }

        return(
            <Button onPress={this.onButtonPressForgotPassword.bind(this)}>
                SUBMIT
            </Button>
        )
    }

    render(){

        const { 
            container, infoContainer, buttonContainer, inputStyle,
            headerView, viewThumbnail, headingStyle, textStyle
        } = styles;

        const { email } = this.props

        return(
            <Container style={{ backgroundColor: '#798496' }}>
                <Content style={container}>
                    <View style={headerView}>
                        <View style={viewThumbnail}>
                            <Text style={headingStyle}>Forgot password</Text>
                            <Text style={textStyle}>Enter your email address, we'll send you 
                                instructions on how to change your password
                            </Text>
                        </View>
                    </View>
                    <StatusBar barStyle="light-content"/>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View>
                            <View style={infoContainer}>
                                <TextInput 
                                    placeholder='Email Address'
                                    autoCorrect={false}
                                    style={inputStyle}
                                    placeholderTextColor='black'
                                    keyboardType='email-address'
                                    onChangeText={this.changeEmailAddress.bind(this)}
                                    value={email}
                                />
                                {this.renderForgotPasswordError()}
                                <View style={buttonContainer}>
                                    {this.renderButtonForgotPassword()}
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    viewThumbnail:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
    },
    headingStyle:{
        fontSize: 30,
        color: 'white'
    },
    container:{
        flex: 1,
        backgroundColor: '#352644',
        flexDirection: 'column'
    },
    infoContainer:{
        left: 0,
        right: 0,
        bottom: 0,
        height: 300,
        padding: 20,
    },
    buttonContainer:{
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 100,
        marginTop:30,
        paddingTop:15,
        paddingBottom:15,
        borderRadius: 10,
        alignSelf: 'center'
    },
    textStyle: {
        color: 'white',
        fontSize: 17,
        margin: 20
    },
    headerView:{
        height: 200,
        marginTop: 70
    },
    inputStyle:{
        height: 60,
        backgroundColor: '#efe8f7',
        color: 'black',
        marginBottom: 20,
        fontSize: 17,
        paddingHorizontal: 10,
        borderColor: 'white'
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
})

const mapStateToProps = ({ forgot }) => {
    const { email, load, error } = forgot;
    return { email, load, error }
}

export default connect(mapStateToProps, {
    forgotEmailAddressChange, forgotLogic
})(Forgot);