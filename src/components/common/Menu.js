import React, { Component } from 'React';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Content, Grid, Card, Col, Row } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin } from 'react-native-google-signin';
import ExtraDimensions from 'react-native-extra-dimensions-android';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const homeIcon = (
    <Icon
        name="ios-home"
        size={30}
        style={{ 
            marginTop: -5
        }}
        color="white"
    />
)

const settingsIcon = (
    <Icon
        name="ios-settings"
        size={30}
        style={{ 
            marginTop: -5
        }}
        color="white"
    />
)

const contactIcon = (
    <Icon
        name="ios-chatboxes"
        size={30}
        style={{ 
            marginTop: -5
        }}
        color="white"
    />
)


class Menu extends Component {

    async logOut() {
        // check if a user has logged in using google signin
        const GOOGLE_CURRENT_USER = await GoogleSignin.getCurrentUser();
        console.log(GOOGLE_CURRENT_USER)
        if(GOOGLE_CURRENT_USER){
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            console.log(GOOGLE_CURRENT_USER)
            AsyncStorage.clear().then(() => {
                Actions.reset('auth')
            })
        }else{
            AsyncStorage.clear().then(() => {
                Actions.reset('auth')
            })
        }
    }

    // function for hungry restaurants to show on map
    hungryComponent(){
        Actions.hungry()
    }

    // function for out and about restaurants to show on map
    outAboutComponent(){
        Actions.outandabout()
    }

    // function for thirty restaurants to show on map
    thirstyComponent(){
        Actions.thirsty()
    }

    // function for favorite restaurants
    favoriteComponent(){
        Actions.favorites()
    }

    // function for settings component
    settingsComponent(){
        Actions.settings()
    }

    render() {
        const { viewThumbnail, textStyle, logo, mapStyle, menuImageStyle, privacyStyle, AboutStyle, logOutStyle, othertextStyle } = styles;
        return (
            <View style={{ flex: 1 }}>
                <View style={viewThumbnail}>
                    <Image
                        style={logo}
                        source={require('../../../images/RADILOGO.png')}
                    />
                </View>
                <View style={{ flex: 2, backgroundColor: '#41424D' }}>
                    <Content>
                        <Grid style={{ padding: 20 }}>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => Actions.drawer()}>
                                                {homeIcon}
                                                <Text style={textStyle}>Home</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => this.hungryComponent()} >
                                                <Image
                                                    style={menuImageStyle}
                                                    source={require('../../../images/Hungry.jpg')}
                                                />
                                                <Text style={textStyle}>Hungry</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => this.thirstyComponent()} >
                                                <Image
                                                    style={menuImageStyle}
                                                    source={require('../../../images/thirsty.jpg')}
                                                />
                                                <Text style={textStyle}>Thirsty</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => this.outAboutComponent()} >
                                                <Image
                                                    style={menuImageStyle}
                                                    source={require('../../../images/out&about.jpg')}
                                                />
                                                <Text style={textStyle}>Out & About</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => this.favoriteComponent()} >
                                                <Image
                                                    style={menuImageStyle}
                                                    source={require('../../../images/favorite.png')}
                                                />
                                                <Text style={textStyle}>Favorites</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => Actions.personorders()}>
                                                <Image
                                                    style={menuImageStyle}
                                                    source={require('../../../images/bookings.png')}
                                                />
                                                <Text style={textStyle}>Orders</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => this.settingsComponent()}>
                                                {settingsIcon}
                                                <Text style={othertextStyle}>Settings</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => Actions.contact()}>
                                                {contactIcon}
                                                <Text style={othertextStyle}>Contact Us</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card style={{ backgroundColor: '#636464' }}>
                                        <View style={mapStyle}>
                                            <TouchableOpacity onPress={() => Actions.privacy()}>
                                                <Text style={AboutStyle}>About Us</Text>
                                                <Text style={privacyStyle}>Privacy Policy</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1}>
                                    <TouchableOpacity onPress={this.logOut.bind(this)}>
                                        <Text style={logOutStyle}>Log Out</Text>
                                    </TouchableOpacity>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>
                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    viewThumbnail: {
        flex: 1,
        backgroundColor: '#512E5F',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        width: PHONE_WIDTH / 3,
        height: PHONE_HEIGHT / 5.7,
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 30,
    },
    menuImageStyle: {
        width: PHONE_WIDTH / 13,
        height: PHONE_HEIGHT / 22,
        borderRadius: 20,
        marginTop: -5
    },
    textThumbnail: {
        fontSize: 15,
        color: '#fff',
        marginTop: 10
    },
    textStyle: {
        fontSize: 15,
        paddingLeft: 70,
        marginTop: -29,
        color: 'white'
    },
    othertextStyle: {
        fontSize: 15,
        paddingLeft: 70,
        marginTop: -25,
        color: 'white'
    },
    logOutStyle: {
        fontSize: 15,
        paddingLeft: 70,
        color: 'white',
        marginTop: 10
    },
    privacyStyle: {
        fontSize: 15,
        paddingLeft: 70,
        color: '#FFFFFF',
        marginTop: -5
    },
    AboutStyle: {
        fontSize: 15,
        paddingLeft: 70,
        color: '#FFFFFF',
        marginTop: -10
    },
    mapStyle: {
        height: PHONE_HEIGHT / 30,
        margin: 10,
        borderRadius: 10,
        borderColor: 'black'
    },

})

export default Menu;