import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground, StyleSheet, View } from 'react-native'
import styled from 'styled-components';
import { 
    Container, Header, Left, Button, Icon, Text, Card, 
    CardItem, Content, Row, Grid, Col, Badge, 
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import { Spinner } from '../';
import { connect } from 'react-redux';
import { 
    getPicture, searchMapChange
} from '../../../Actions';

class Index extends Component{

    onSearchChange(text){
        this.props.searchMapChange(text)
        Actions.maps()
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({
            title: "Pick an Image",
            maxWidth: 800, 
            maxHeight: 600
        }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.props.getPicture(res)
                Actions.uploadpost()
            }
        });
    }

    // Function for displaying image picker error
    renderImagePickerError(){
        const { errors } = this.props
        if(errors){
            return(
                <View>
                    <Text style={styles.errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // function for rendering Icon for picking an image
    renderImagePickerIcon(){
        const { loader} = this.props

        if(loader){
            return <Spinner size='large' />
        }

        return(
            <CancelButton block warning
                onPress={() => this.pickImageHandler()}>
                <Icon name="camera" size={50} style={{ fontSize: 38, color: '#848991' }} />
            </CancelButton>
        )
    }

    render(){
        const { 
            headerStyle, textHeaderStyle, imageBackground, captionStyle, 
            mainheaderStyle, imageStyle, shareStyle, thumbsStyle, topImageStyle1, topImageStyle2, topImageStyle3
        } = styles
        return(
            <Container>
                <Content style={{
                    padding: 0,
                }}>
                    <Header style={mainheaderStyle}>
                        <ImageBackground source={require('../../../../images/truck.jpg')} style={imageBackground}>
                            <Header transparent rounded style={headerStyle} >
                                <Text style={textHeaderStyle}>News</Text>
                            </Header>
                        </ImageBackground>
                    </Header>
                    <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Top Photos</Text>
                    <Grid>
                        <Row size={1}>
                            <Col size={0.5}>
                                <TouchableOpacity onPress={() => Actions.joint()}>
                                    <Card transparent  >
                                        <CardItem cardBody>
                                            <Image source={{ uri: 'https://images.unsplash.com/photo-1505253668822-42074d58a7c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80' }} style={topImageStyle1}/>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                            <Col size={0.5}>
                                <TouchableOpacity onPress={() => Actions.joint()}>
                                    <Card transparent  >
                                        <CardItem cardBody>
                                            <Image source={{ uri: 'https://images.unsplash.com/photo-1505253668822-42074d58a7c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80' }} style={topImageStyle2}/>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.joint()}>
                                    <Card transparent  >
                                        <CardItem cardBody>
                                            <Image source={{ uri: 'https://images.unsplash.com/photo-1505253668822-42074d58a7c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80' }} style={topImageStyle3}/>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <CardItem cardBody>
                                        <Image source={{ uri: 'https://images.unsplash.com/photo-1505253668822-42074d58a7c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80' }} style={imageStyle}/>
                                    </CardItem>
                                    <Text style={captionStyle}>Awesome hotel</Text>
                                    <CardItem >
                                        <Left style={{ marginLeft: 20, marginTop: -10 }} >
                                            <Button transparent>
                                                <Icon style={shareStyle} name="share" />
                                            </Button>
                                            <Button transparent>
                                                <Text style={thumbsStyle}>4k</Text>
                                                <Icon style={thumbsStyle} name="thumbs-up" />
                                            </Button>
                                        </Left>
                                    </CardItem>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <CardItem cardBody>
                                        <Image source={{ uri: 'https://images.unsplash.com/photo-1505253668822-42074d58a7c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80' }} style={imageStyle} />
                                    </CardItem>
                                    <Text style={captionStyle}>Awesome hotel</Text>
                                    <CardItem>
                                        <Left style={{ marginLeft: 20, marginTop: -10 }} >
                                            <Button transparent>
                                                <Icon style={shareStyle} name="share" />
                                            </Button>
                                            <Button transparent>
                                                <Text style={thumbsStyle}>4k</Text>
                                                <Icon style={thumbsStyle} name="thumbs-up" />
                                            </Button>
                                        </Left>
                                    </CardItem>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <CartGrid>
                    <HalfCol>
                        {this.renderImagePickerError()}
                        {this.renderImagePickerIcon()}
                    </HalfCol>
                </CartGrid>
            </Container>
        )
    }
};

const mapStateToProps = ({ indexHome }) => {
    const { search, loader, errors } = indexHome;

    return { search, loader, errors }
}

const styles = StyleSheet.create({
    imageBackground:{
        width: '100%', 
        marginTop: -10
    },
    captionStyle:{
        paddingLeft: 40,
        paddingTop: 20
    },
    thumbsStyle:{
        color: 'black'
    },
    shareStyle:{
        color: 'black'
    },
    imageStyle:{
        height: 200,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle1:{
        height: 180,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle2:{
        height: 90,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle3:{
        height: 85,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    mainheaderStyle:{
        height: 100, 
        backgroundColor: '#001E24',
        marginTop: -10,
        borderRadius: 0, 
        borderColor: '#001E24'
    },
    headerStyle:{
        borderRadius: 0, 
        alignSelf: 'flex-start', 
        height: null 
    },
    textHeaderStyle:{
        fontSize: 40,
        fontWeight: 'bold',
        color:'white',
        paddingLeft: 10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
})

export default connect(mapStateToProps, { 
    searchMapChange, getPicture
})(Index);

const RadiBadge = styled(Badge)`
    background-color:black;
    padding-top: 5px;
    margin-top: -30px;
`;

const RadiIcon = styled(Icon)`
    font-size:14px;
    margin-right:5px;
`;

const CartGrid = styled(Grid)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 10;
`;

const CancelButton = styled(Button)`
    border-radius: 10px;
    color:#ffffff;
    background-color: transparent;
`;

const HalfCol = styled(Col)`
    width:100%;
    margin:5px;
`;