import React, { Component } from 'react';
import {
    ImageBackground, StyleSheet, View, Platform, Image, RefreshControl, TextInput,
    FlatList, TouchableWithoutFeedback, Dimensions, TouchableOpacity, StatusBar
} from 'react-native'
import styled from 'styled-components';
import { Text, Grid, Col, Row, Button } from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import { Spinner } from '../';
import Share from 'react-native-share';
import { connect } from 'react-redux';
import {
    getPostsServer, getPicture, searchMapChange, dateTimeBookingTimer, setIsRefreshState, nextPagePagination,
    createLike, userId, showCameraIcon, searchRestaurantMap, sendPushNotForOrder, setLoadingState, unLike, postsRefresh
} from '../../../Actions';
import AsyncStorage from "@react-native-community/async-storage";
import { apiIp } from '../../../Config';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import moment from 'moment';
import SplashScreen from 'react-native-splash-screen'



// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

const PHONE_HEIGHT_ANDROID = ExtraDimensions.getRealWindowHeight()
const PHONE_HEIGHT_IOS = Dimensions.get("window").height

const PHONE_WIDTH_ANDROID = ExtraDimensions.getRealWindowWidth()
const PHONE_WIDTH_IOS = Dimensions.get("window").width

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const menuIcon = (
    <Icon
        name="menu-fold"
        size={30}
        style={{
            marginLeft: 15,
            color: 'black',
            marginTop: Platform.OS === 'ios' ? 10 : 0
        }}
        onPress={() => Actions.drawerOpen()}
        color="black"
    />
)

class Post extends Component {

    constructor(props) {
        super(props)
        this.state = {
            refresh: false,
            cameraIcon: false,
            new: 1560486376000
        }
    }

    componentDidMount() {
        this.props.setLoadingState(true)
        AsyncStorage.getItem("user").then(token => {
            if (token === null) {
                this.setState({
                    cameraIcon: false
                })
                this.props.showCameraIcon()
                this.props.setLoadingState(false)
            } else {
                this.setState({
                    cameraIcon: true
                })
                this.props.userId(JSON.parse(token).data.id)
                this.props.getPostsServer({ page: this.props.page, user_id: JSON.parse(token).data.id })
                this.props.setLoadingState(false)
            }
        });
    }

    // pull more posts from server
    handleLoadMore = () => {
        if (!this.props.loading) {
            this.props.nextPagePagination(this.props.page + 1)
            AsyncStorage.getItem("user").then(token => {
                if (token === null) {
                    this.setState({
                        cameraIcon: false
                    })
                    this.props.showCameraIcon()
                } else {
                    this.setState({
                        cameraIcon: true
                    })
                    this.props.userId(JSON.parse(token).data.id)
                    this.props.getPostsServer({ page: this.props.page, user_id: JSON.parse(token).data.id })
                }
            });
        }
    };

    // render footer of flatlist
    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.props.loading) return null;
        return (
            <Spinner />
        );
    };

    // render header component of flatlist
    renderHeaderComponent = () => {

        const { mainheaderStyle, headerStyle, textHeaderStyle, imageBackground, navBarStyle, viewHeaderStyle } = styles
        const { restaurant_name } = this.props
        return (
            <View>
                <View style={navBarStyle}>
                    <Grid style={mainheaderStyle}>
                        <Row size={1}>
                            <Col size={0.2}>
                                {menuIcon}
                            </Col>
                            <Col size={0.8}>
                                <TextInput
                                    placeholder='Where to'
                                    autoCorrect={false}
                                    placeholderTextColor='black'
                                    keyboardType='default'
                                    value={restaurant_name}
                                    onChangeText={this.onSearchRestaurantName.bind(this)}
                                    style={{
                                        width: PHONE_WIDTH / 2,
                                        height: Platform.OS === 'ios' ? PHONE_HEIGHT / 5 : PHONE_HEIGHT / 3,
                                        marginTop: Platform.OS === 'ios' ? -PHONE_HEIGHT / 15 : -PHONE_HEIGHT_ANDROID / 7,
                                    }}
                                />
                            </Col>
                        </Row>
                    </Grid>
                    <ImageBackground source={require('../../../../images/truck.jpg')} style={imageBackground}>
                        <View style={headerStyle} >
                            <Text style={textHeaderStyle}>News</Text>
                        </View>
                    </ImageBackground>
                </View>
                <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Top Photos</Text>
            </View>
        );
    };

    // pull afresh posts from server
    onRefresh() {
        this.props.setIsRefreshState(true)
        this.props.postsRefresh()
        AsyncStorage.getItem("user").then(token => {
            if (token === null) {
                this.setState({
                    cameraIcon: false
                })
                this.props.showCameraIcon()
                this.props.setIsRefreshState(false)
            } else {
                this.setState({
                    cameraIcon: true
                })
                this.props.userId(JSON.parse(token).data.id)
                this.props.getPostsServer({ page: 1, user_id: JSON.parse(token).data.id })
                this.props.setIsRefreshState(false)
            }
        });
    }

    // timer for booking
    timer(dateTimeBooking) {
        const { timerStyle } = styles;
        const duration = moment.duration(dateTimeBooking)
        const centiseconds = Math.floor(duration.milliseconds() / 10)
        return (
            <View>
                <Text style={timerStyle}>{duration.years()}:{duration.days()}:{duration.minutes()}:{duration.seconds()},{centiseconds}</Text>
                <Text style={timerStyle}>ORDER DATE:  {this.props.bookingDate}</Text>
            </View>
        )
    }

    // render booking date timer
    renderBookingDateTimer() {
        setInterval(() => {
            this.props.dateTimeBookingTimer(this.props.dateTimeBooking - 1)
        }, 10000)
        const { viewTimerStyle } = styles
        return (
            <View style={viewTimerStyle}>
                {this.timer(this.props.dateTimeBooking)}
            </View>
        )
    }


    // search for a restaurant on the map
    onSearchRestaurantName(text) {
        this.props.searchRestaurantMap(text)
        Actions.maps()
    }

    // function for seraching on a map
    onSearchChange(text) {
        Actions.maps()
        this.props.searchMapChange(text)
    }

    // Function for displaying create error
    renderLikeError() {
        const { errors } = this.props
        const { errorTextStyle } = styles
        if (errors) {
            return (
                <View>
                    <Text style={errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // Function for initiating like redux logic
    onButtonPressLike(posters) {
        if (!posters.item.isSelect) {
            posters.item.isSelect = !posters.item.isSelect;
            posters.item.likes = posters.item.likes + 1
            posters.item.selectedClass = posters.item.isSelect
                ? styles.selected
                : styles.thumbsStyle;
            const index = this.props.allPosts.findIndex(
                item => posters.item.id == item.id
            );
            this.props.allPosts[index] = posters.item
            const body = {
                post_id: posters.item.id,
                person_id: this.props.person_id
            }
            this.props.createLike(body, this.props.allPosts)
            this.setState({
                refresh: !this.state.refresh
            })
        } else {
            posters.item.isSelect = !posters.item.isSelect;
            posters.item.likes = posters.item.likes - 1
            posters.item.selectedClass = posters.item.isSelect
                ? styles.selected
                : styles.thumbsStyle;
            const index = this.props.allPosts.findIndex(
                item => posters.item.id == item.id
            );
            this.props.allPosts[index] = posters.item
            const body = {
                post_id: posters.item.id,
                person_id: this.props.person_id
            }
            this.props.unLike(body, this.props.allPosts)
            this.setState({
                refresh: !this.state.refresh
            })
        }
    }

    // function for picking an image
    pickImageHandler = () => {
        ImagePicker.showImagePicker({
            title: "Pick an Image",
            maxWidth: 800,
            maxHeight: 600
        }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.props.getPicture(res)
                Actions.uploadpost()
            }
        });
    }

    // Function for displaying image picker error
    renderImagePickerError() {
        const { errors } = this.props
        if (errors) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // function for rendering Icon for picking an image
    renderImagePickerIcon() {
        const { loader, dateTimeBooking } = this.props
        const { cameraIconStyle, cameraIconStyles } = styles

        if (this.state.cameraIcon) {

            if (dateTimeBooking !== 0) {
                if (loader) {
                    return <Spinner size='large' />
                }

                return (
                    <CartGrid style={{ height: PHONE_HEIGHT / 17 }}>
                        <HalfCol>
                            {this.renderImagePickerError()}
                            <CancelButton block warning onPress={() => this.pickImageHandler()}>
                                <Icon name="camerao" style={cameraIconStyles} />
                            </CancelButton>
                        </HalfCol>
                        <HalfCol>
                            {this.renderBookingDateTimer()}
                        </HalfCol>
                    </CartGrid>
                )
            } else if (dateTimeBooking == 0) {
                return (
                    <CartGrid style={{ height: PHONE_HEIGHT / 12, marginLeft: PHONE_WIDTH / 2.5 }}>
                        <HalfCol>
                            {this.renderImagePickerError()}
                            <CancelButton block warning onPress={() => this.pickImageHandler()}>
                                <Icon name="camerao" style={cameraIconStyle} />
                            </CancelButton>
                        </HalfCol>
                    </CartGrid>
                )
            }
        }
    }

    // function for rendering the posts of users
    renderItem = (posters) => {
        const { captionStyle, imageStyle, shareStyle, thumbsStyle } = styles
        const {
            id, restaurant_id, likes, like_person_id, like_id, selectedClass, isSelect,
            description, gallery, created_at, updated_at, pictureGallery, base64Pic
        } = posters.item

        const { thumbsStyler, likesStyler } = styles

        const { user_id, headers } = this.props

        const expanse = user_id == like_person_id

        const shareImageBase64 = {
            url: 'data:image/png;base64,' + base64Pic,
            type: "image/png",
            subject: "Share Link" //  for email
        };

        if (this.state.cameraIcon) {
            SplashScreen.hide()
            return (
                <Grid>
                    <Row size={1}>
                        <Col size={1}>
                            <TouchableOpacity onPress={() => Actions.joint({ restaurant_id: restaurant_id })}>
                                <View>
                                    <View>
                                        <Image source={{ uri: apiIp + pictureGallery }} style={styles.imageStyle} />
                                    </View>
                                    <Text style={captionStyle}>{description}</Text>
                                    <Grid>
                                        <Row>
                                            <Col size={0.2}>
                                                <Button onPress={() => Share.open(shareImageBase64)} transparent>
                                                    <Icon style={shareStyle} name="sharealt" />
                                                </Button>
                                            </Col>
                                            <Col size={0.2}>
                                                <Button onPress={() => this.onButtonPressLike(posters)} transparent>
                                                    <Text style={[likesStyler]}>{likes}</Text>
                                                    <Image source={isSelect ? require('../../../../images/heart-red.png') : require('../../../../images/heart-outline.png')} style={[selectedClass]} />
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </View>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Grid>
            )

        } else {

            SplashScreen.hide()
            return (
                <Grid>
                    <Row size={1}>
                        <Col size={1}>
                            <TouchableOpacity onPress={() => Actions.joint({ restaurant_id: restaurant_id })}>
                                <View>
                                    <View>
                                        <Image source={{ uri: apiIp + pictureGallery }} style={styles.imageStyle} />
                                    </View>
                                    <Text style={captionStyle}>{description}</Text>
                                    <Grid>
                                        <Row>
                                            <Col size={0.2}>
                                                <Icon style={shareStyle} name="sharealt" />
                                            </Col>
                                            <Col size={0.2}>
                                                <Button transparent>
                                                    <Text style={[likesStyler]}>{likes}</Text>
                                                    <Image source={require('../../../../images/heart-outline.png')} style={thumbsStyle} />
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Grid>
                                </View>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Grid>
            )
        }

    }

    render() {
        const { allPosts, isRefreshing, loading, page } = this.props

        if (loading && page === 1) {
            return (
                <View style={{
                    width: '100%',
                    height: '100%'
                }}>
                    <Spinner />
                </View>
            );
        }

        return (
            <View style={{ width: '100%', height: '100%' }}>
                <StatusBar
                    barStyle='light-content'
                    backgroundColor="#4f6d7a"
                />
                <FlatList
                    data={allPosts}
                    renderItem={this.renderItem}
                    refreshControl={
                        <RefreshControl
                            refreshing={isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                    keyExtractor={(item, posters) => posters.toString()}
                    extraData={(this.props, this.state.refresh)}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    onEndReachedThreshold={0.4}
                    onEndReached={this.handleLoadMore.bind(this)}
                    ListHeaderComponent={this.renderHeaderComponent.bind(this)}
                />
                {this.renderImagePickerIcon()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainheaderStyle: {
        backgroundColor: 'white',
        marginTop: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 20 : PHONE_HEIGHT_ANDROID / 25,
        height: PHONE_HEIGHT / 10,
        width: PHONE_WIDTH / 1.1,
        borderRadius: 10,
        alignSelf: 'center'
    },
    headerStyle: {
        borderRadius: 0,
        alignSelf: 'flex-start',
        height: PHONE_HEIGHT / 9
    },
    textHeaderStyle: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white',
        paddingLeft: 10
    },
    imageBackground: {
        width: '100%',
        marginTop: 10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
    navBarStyle: {
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 4. : PHONE_HEIGHT_ANDROID / 4.5,
        backgroundColor: '#001E24',
    },
    cameraIconStyle: {
        fontSize: PHONE_WIDTH / 10,
        color: '#848991',
        marginTop: Platform.OS === 'ios' ? 10 : PHONE_HEIGHT_ANDROID / 110,
        alignSelf: 'center'
    },
    cameraIconStyles: {
        fontSize: PHONE_WIDTH / 10,
        color: '#848991',
        marginBottom: 10,
        marginLeft: PHONE_WIDTH / 20,
        alignSelf: 'flex-start'
    },
    viewTimerStyle: {
        fontSize: PHONE_WIDTH / 10,
        color: '#848991',
        marginBottom: 10,
        marginRight: PHONE_WIDTH / 20,
        alignSelf: 'flex-end'
    },
    captionStyle: {
        paddingLeft: 40,
        paddingTop: 20
    },
    thumbsStyle: {
        marginTop: -15,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,
        width: PHONE_WIDTH / 15,
    },
    thumbsStyler: {
        marginTop: -15,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,
        width: PHONE_WIDTH / 15,
    },
    likesStyler: {
        color: 'black',
        fontSize: 18,
        marginTop: -17,
        marginLeft: 10
    },
    shareStyle: {
        color: 'black',
        fontSize: 22,
        marginLeft: 30,
        marginTop: -10
    },
    imageStyle: {
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 1.3 : PHONE_HEIGHT_ANDROID / 1.3,
        width: PHONE_WIDTH,
        flex: 1,
    },
    topImageStyle1: {
        height: 180,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle2: {
        height: 90,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    topImageStyle3: {
        height: 85,
        width: null,
        flex: 1,
        borderRadius: 19,
    },
    selected: {
        marginTop: -15,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 30 : PHONE_HEIGHT_ANDROID / 30,
        width: PHONE_WIDTH / 15,
        padding: 0
    },
    viewHeaderStyle: {
        height: PHONE_HEIGHT / 15,
        backgroundColor: 'white',
        marginBottom: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 1.6 : PHONE_HEIGHT_ANDROID / 20,
        width: PHONE_WIDTH / 1.1,
        borderRadius: 10,
        marginTop: 40,
        alignSelf: 'center'
    },
    timerStyle: {
        color: '#FFFFFF',
        fontSize: 7,
        fontWeight: '100'
    }
})

const mapStateToProps = ({ indexHome, posts, map }) => {

    const {
        person_id, like_id, user_id, page, isRefreshing, arrayNumber,
        cameraIcon, dateTimeBooking, startTimer, bookingDate, loading, headers
    } = posts;

    const allPosts = posts.allPosts.slice(arrayNumber)

    if (allPosts.length) {

        const { restaurant_name } = map

        const { search, loader, errors } = indexHome;

        return {
            search, loader, errors, person_id, like_id, allPosts, bookingDate, isRefreshing,
            user_id, cameraIcon, restaurant_name, dateTimeBooking, startTimer, page, loading, headers
        }

    } else {

        const allPosts = posts.allPosts

        const { restaurant_name } = map

        const { search, loader, errors } = indexHome;

        return {
            search, loader, errors, person_id, like_id, allPosts, bookingDate, isRefreshing,
            user_id, cameraIcon, restaurant_name, dateTimeBooking, startTimer, page, loading, headers
        }

    }

}

export default connect(mapStateToProps, {
    getPostsServer, searchMapChange, getPicture, createLike, sendPushNotForOrder, setLoadingState, unLike,
    userId, showCameraIcon, searchRestaurantMap, dateTimeBookingTimer, setIsRefreshState, nextPagePagination, postsRefresh
})(Post);

const CartGrid = styled(Grid)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 1;
    opacity: 0.7;
    background-color: #2F2F2F;
    border-radius:50px;
    color:#ffffff;
    width: 20%;
`;

const CancelButton = styled(TouchableWithoutFeedback)`
    
`;

const HalfCol = styled(Col)`
    width:100%;
    margin:5px;
`;