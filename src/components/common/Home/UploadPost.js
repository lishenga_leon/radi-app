import React, { Component } from 'react';
import {
    StyleSheet, Image, View, Dimensions, Platform,
    Text, FlatList, TouchableOpacity, StatusBar
} from 'react-native'
import { Container, Col, Content, Row, Grid, Form, Textarea, Card } from 'native-base';
import { Button, Spinner } from '../'
import { connect } from 'react-redux';
import { restaurantNameChange, postChange, uploadPost, pickRestaurant } from '../../../Actions';
import { SearchBar } from 'react-native-elements';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

const PHONE_HEIGHT_ANDROID = ExtraDimensions.getRealWindowHeight()
const PHONE_HEIGHT_IOS = Dimensions.get("window").height

const PHONE_WIDTH_ANDROID = ExtraDimensions.getRealWindowWidth()
const PHONE_WIDTH_IOS = Dimensions.get("window").width

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class UploadPost extends Component {

    // function for initiating redux change for post
    onPostChange(texter) {
        this.props.postChange(texter)
    }

    // function for initiating redux change for restaurant name
    onRestaurantNameChange(text) {
        this.props.restaurantNameChange(text)
    }

    // Function for displaying create error
    renderUploadError() {
        const { errors } = this.props
        if (errors) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // function to separate restaurants that are being rendered
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    margin: 10,
                    backgroundColor: 'black'
                }}
            />
        );
    };

    // Function for initiating upload post redux logic
    onButtonPressUpload() {
        const { restaurant_id, post, picture, location } = this.props;

        this.props.uploadPost({ restaurant_id, post, picture, location })
    }

    // Function for displaying post button
    renderButtonUpload() {
        const { loader } = this.props

        if (loader) {
            return <Spinner size='large' />
        }

        return (
            <Button onPress={this.onButtonPressUpload.bind(this)}>
                POST
            </Button>
        )
    }

    pickRest(rest) {
        this.props.pickRestaurant(rest.name, rest.id, rest.location)
    }

    // function for displaying the name of the restaurants being searched
    renderItem(rest) {
        return (
            <Card style={styles.cardStyle}>
                <TouchableOpacity onPress={this.pickRest.bind(this, rest.item)} style={{ height: PHONE_HEIGHT/15 }}>
                    <View>
                        <Text style={styles.titleStyle}>
                            {rest.item.name}
                        </Text>
                    </View>
                </TouchableOpacity>
            </Card>
        )
    };

    render() {
        const { post, restaurant, picture, restaurants, showLoading } = this.props
        const { viewThumbnail, logo, buttonContainers, inputStyle, textAreaStyle } = styles
        return (
            <Container>
                <StatusBar
                    barStyle='dark-content'
                    backgroundColor="#4f6d7a"
                />
                <Content>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <View style={viewThumbnail}>
                                    <Image
                                        style={logo}
                                        source={{
                                            uri: picture.uri
                                        }}
                                    />
                                </View>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Form>
                                    <SearchBar
                                        placeholder="Restaurant Name"
                                        lightTheme
                                        round
                                        showLoading={showLoading}
                                        onChangeText={this.onRestaurantNameChange.bind(this)}
                                        autoCorrect={false}
                                        style={inputStyle}
                                        value={restaurant}
                                    />
                                    <FlatList
                                        data={restaurants}
                                        renderItem={item => this.renderItem(item)}
                                        keyExtractor={item => item.id.toString()}
                                    />
                                    <Textarea
                                        rowSpan={5}
                                        bordered
                                        placeholderTextColor="black"
                                        keyboardType='default'
                                        maxLength={20}
                                        onChangeText={this.onPostChange.bind(this)}
                                        value={post}
                                        placeholder="Post"
                                        style={textAreaStyle}
                                    />
                                    {this.renderUploadError()}
                                    <View style={buttonContainers}>
                                        {this.renderButtonUpload()}
                                    </View>
                                </Form>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
};

const styles = StyleSheet.create({
    buttonContainers: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 170,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    cardStyle: {
        borderWidth: 1,
        width: PHONE_WIDTH / 1.12,
        marginLeft: PHONE_WIDTH / 30
    },
    viewThumbnail: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: PHONE_HEIGHT/10
    },
    logo: {
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 2 : PHONE_HEIGHT_ANDROID / 2,
        width: PHONE_WIDTH,
        flex: 1,
        alignSelf: 'center',
    },
    inputStyle: {
        fontSize: 15,
        height: 60,
        margin: 10,
        backgroundColor: 'white'
    },
    textAreaStyle: {
        fontSize: 15,
        margin: 10,
        backgroundColor: 'white'
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
    titleStyle: {
        fontSize: 15,
        paddingLeft: 15,
        color: 'black',
        paddingTop: 15
    },
})

const mapStateToProps = ({ uploadPost }) => {
    const { 
        restaurant, post, picture, loader, location,
        errors, restaurants, restaurant_id, showLoading 
    } = uploadPost;
    return { 
        restaurant, post, picture, loader, errors, 
        restaurants, restaurant_id, showLoading, location 
    }
}

export default connect(mapStateToProps, { restaurantNameChange, postChange, uploadPost, pickRestaurant })(UploadPost);