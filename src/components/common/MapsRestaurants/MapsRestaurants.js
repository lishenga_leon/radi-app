import { 
    Container, Content, Grid, 
    Row, Col, Card, 
} from 'native-base';
import { StyleSheet, TouchableOpacity, FlatList, Dimensions, Image, Text } from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { apiIp } from '../../../Config';
import { connect } from 'react-redux';
import { getRestaurantDistance } from '../../../Actions';

const win = Dimensions.get('window');

class MapsRestaurants extends Component {

    componentDidMount(){
        this.props.getRestaurantDistance(this.props.restaurants)
    }

    renderItem = (item) =>{
        const { id ,name, cover_image, location, distance } = item.item
        const { thumbNailStyle, contentNameTextStyles, cardStyle } = styles

        return(
            <Card style={cardStyle}>
                <TouchableOpacity onPress={()=> Actions.joint({ restaurant_id: id })}>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Image source={{ uri: apiIp+cover_image }} style={thumbNailStyle}/>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <Text style={contentNameTextStyles}>
                                        {name.toUpperCase()}  
                                    </Text>
                                    <Text style={contentNameTextStyles}>
                                        {distance},  5 stars 
                                    </Text>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </TouchableOpacity>
            </Card>
        )
    }

    render() {

        const { contentStyle, headerStyle, contentNameTextStyles } = styles

        const { finalData } = this.props

        return (
            <Container style={{ backgroundColor: '#352644', }}>
                <Text style={{ fontSize: 35, marginTop: 70, marginLeft: 10, color: 'white' }}>Nearby</Text>
                <Content style={contentStyle}>
                    <FlatList
                        data={finalData}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.toString()}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    cardStyle:{
        alignSelf: 'center', 
        borderRadius: 10, 
        width: win.width/2
    },
    contentStyle:{
        padding: 0,
        marginTop: 40
    },
    thumbNailStyle:{  
        height: 100, 
        width: win.width/2,
        borderRadius: 10,
    },
    contentNameTextStyles:{
        marginTop: 20,
        fontSize: 10,
        marginLeft: 20,
        fontWeight: 'bold'
    },
    headerStyle:{
        height: 50
    },
})

const mapStateToProps = ({ mapsrestaurant }) => {
    const { finalData } = mapsrestaurant;
    return { finalData };
};

export default connect(mapStateToProps, {
    getRestaurantDistance
})(MapsRestaurants);