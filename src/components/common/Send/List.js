import React, { Component } from 'react';
import {
  Text, TouchableWithoutFeedback, View,
} from 'react-native';
import { connect } from 'react-redux';
import { CardSection } from '../CardSection';
import { 
    selectContact, sendMsisdnChanged
} from '../../../actions';
import { Actions } from 'react-native-router-flux';

class List extends Component {

  renderDescription(name) {
    const { contact } = this.props;
    const expanded = name === this.props.own;
    if (expanded) {
      this.props.sendMsisdnChanged(contact.item.mobile)
      Actions.popTo('send')
    }
  }

  render() {
    const { titleStyle } = styles;
    const { name } = this.props.contact.item

    return (
      <TouchableWithoutFeedback
        onPress={() => this.renderDescription(name)}
      >
        <View>
          <CardSection>
            <Text style={titleStyle}>
                {name}
            </Text>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15,
    color: 'black',
  },
  descriptionStyle: {
    paddingLeft: 10,
    paddingRight: 10
  }
};

const mapStateToProps = (state, ownProps) => {
  const own = ownProps.contact.item.name;
  return { own };
};

export default connect(mapStateToProps, {
  selectContact, sendMsisdnChanged
})(List);
