import React, { Component } from 'react';
import { FlatList, View, Platform, PermissionsAndroid } from 'react-native';
import { SearchBar } from 'react-native-elements';
import _ from 'lodash';
import { connect } from 'react-redux';
import List from './List';
import Contacts from 'react-native-contacts'; 
import { 
    getAllContacts, searchContact 
} from '../../../actions';

class Contacter extends Component{

    componentWillMount() {
        let dating = []
        let modified = []
        if(Platform.OS === 'android'){
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                  'title': 'Contacts',
                  'message': 'This app would like to view your contacts.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err === 'denied'){
                    } else {
                        let data = contacts
                        /**
                         * Loop through Array
                        */
                        for (let index = 0; index < data.length; index++) {
                            const element = data[index];

                            /*
                            * Build new user object
                            */
                            var mod = {
                                "name": element.givenName + " " + element.familyName,
                                "main": "",
                                "mobile":"",
                                "home fax":"",
                            }
                            /**
                             * Get only main number
                             */
                            element.phoneNumbers.forEach(number => {
                                if (number.label == "main") {
                                    mod.main = number.number
                                }

                                if (number.label == "mobile") {
                                    mod.mobile = number.number
                                }

                                if (number.label == "home fax") {
                                    mod["home fax"] = number.number
                                }
                            });

                            /*
                            * Push to main modified class
                            */
                            modified.push(mod)

                        }
                        this.props.getAllContacts(modified)
                    }
                })
            })
        }else if (Platform.OS === 'ios'){
            Contacts.requestPermission((error, results) => {
                if(error){
                    alert(JSON.stringify(error))
                }else if(results  == 'authorized'){
                    Contacts.getAll((err, contacts) => {
                        if (err === 'denied'){
                        } else {
                            let data = contacts
                            /**
                             * Loop through Array
                            */
                            for (let index = 0; index < data.length; index++) {
                                const element = data[index];

                                /*
                                * Build new user object
                                */
                                var mod = {
                                    "name": element.givenName + " " + element.familyName,
                                    "main": "",
                                    "mobile":"",
                                    "home fax":"",
                                }
                                /**
                                 * Get only main number
                                 */
                                element.phoneNumbers.forEach(number => {
                                    if (number.label == "main") {
                                        mod.main = number.number
                                    }

                                    if (number.label == "mobile") {
                                        mod.mobile = number.number
                                    }

                                    if (number.label == "home fax") {
                                        mod["home fax"] = number.number
                                    }
                                });

                                /*
                                * Push to main modified class
                                */
                                modified.push(mod)

                            }
                            this.props.getAllContacts(modified)
                        }
                    })
                }
            })
        }
    }

    renderItem(contact){
        return <List contact={contact} />
    }

    searchFilterFunction(text){
        if(text == ''){
            this.props.searchContact('')
            this.componentWillMount()
        }else{
            this.props.searchContact(text)
            const newData = this.props.contacts.filter(item => {                  
                const itemData = `${JSON.stringify(item.name).toUpperCase()}   
                ${JSON.stringify(item.name).toUpperCase()} ${JSON.stringify(item.name).toUpperCase()}`;
                const textData = text.toUpperCase();
                
                return itemData.indexOf(textData) > -1; 
            }); 

            this.props.getAllContacts(newData)
        }
    }

    render (){
        if(this.props.contacts == 'leon'){
            return(
                <View>
                    {this.componentWillMount()}
                </View>
            )
        }else{
            return(
                <View>
                    <View>
                        <SearchBar        
                            placeholder="Search Contact"        
                            lightTheme        
                            round        
                            onChangeText={this.searchFilterFunction.bind(this)}
                            autoCorrect={false}   
                            value={this.props.value}          
                        /> 
                    </View>
                    <View>
                    <FlatList
                        data={this.props.contacts}
                        renderItem={this.renderItem}
                        keyExtractor={ contact => contact.toString()}
                    />
                    </View>
                </View>
            )
        }
        
    }
}

const mapStateToProps = ({ contacter }) => {
    const { value, contacts } = contacter
    return { contacts, value };
};

export default connect(mapStateToProps, {
    getAllContacts, searchContact
})(Contacter);
