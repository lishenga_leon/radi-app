import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { googleFacebookSignIn } from '../../../Actions';
import { Spinner } from '../';
import { connect } from 'react-redux';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { apiIp } from '../../../Config';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
  ? Dimensions.get("window").height
  : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
  ? Dimensions.get("window").width
  : ExtraDimensions.getRealWindowWidth();


class FaceBookSignIn extends Component {

  callFacebook(orderData) {
    LoginManager.logInWithPermissions(["public_profile"]).then(
      function (result) {
        if (result.isCancelled) {
          alert(JSON.stringify(result))
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              const { accessToken } = data
              fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=' + accessToken)
                .then((response) => response.json())
                .then((json) => {

                  const data = {

                    fullname: json.name,

                    email: json.email.toLowerCase()

                  }
                  axios.post(apiIp + '/person/googleFacebookSignIn/', data).then(

                    response => {
        
                        if(JSON.stringify(response.data.status_code) == 500){
        
                            Actions.phonenumberafterorder({ userDetails: data, orderData: orderData  })
        
                        }else if(JSON.stringify(response.data.status_code) == 200){
        
                            Actions.passwordafterorder({ userDetails: data, orderData: orderData  })
        
                        }
        
                    }
        
                )

                })
            }
          )
        }
      },
      function (error) {
        alert(JSON.stringify(error))
        console.log("Login fail with error: " + error);
      }
    );
  }

   // Function for displaying create error
   renderCreateError() {

    const { errors } = this.props

    if (errors) {

      return (

        <View>

          <Text style={styles.errorTextStyle}>

            {errors}

          </Text>

        </View>

      )

    }

  }

  // Function for displaying create button
  renderButtonCreate() {

    const { textStyle } = styles;

    const { loader } = this.props

    if (loader) {

      return <Spinner size='large' />

    }

    return (

      <TouchableOpacity onPress={this.callFacebook.bind(this, this.props.orderData.orderData)}>
        <Text style={textStyle}>
          Sign Up with Facebook
          </Text>
      </TouchableOpacity>

    )

  }

  render() {

    const { buttonStyle, textStyle } = styles;

    return (
      <View style={buttonStyle}>
        {this.renderButtonCreate()}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: '#665EFF',
    paddingVertical: 15,
    width: PHONE_WIDTH / 2.3,
    margin: 10,
    marginBottom: 17,
    padding: 15,
    paddingBottom: 15,
    borderRadius: 10,
    height: PHONE_HEIGHT/14
  },
  textStyle: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: PHONE_WIDTH/30
  }
})

const mapStateToProps = ({ create }) => {

  const { loader, errors } = create;

  return { loader, errors }

}

export default connect(mapStateToProps, {

  googleFacebookSignIn

})(FaceBookSignIn);