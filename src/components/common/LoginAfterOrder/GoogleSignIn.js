import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Platform, Dimensions } from 'react-native';
import { Spinner } from '../';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { connect } from 'react-redux';
import { createAfterOrderGoogleSignIn } from '../../../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"

  ? Dimensions.get("window").height

  : ExtraDimensions.getRealWindowHeight();


// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"

  ? Dimensions.get("window").width

  : ExtraDimensions.getRealWindowWidth();


class GoogleSignIn extends Component {

  // Function for calling to authenticate a user and log him or her in.
  callGoogle = async () => {

    try {
      const userInfo = await GoogleSignin.signIn();

      const data = {

        fullname: userInfo.user.name,

        email: userInfo.user.email.toLowerCase(),

        password: '',

        msisdn: ''

      }

      this.props.createAfterOrderGoogleSignIn(data, this.props.orderData.orderData )

    } catch (error) {

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {

        // user cancelled the login flow
        alert(error.code)

        console.log(error)

      } else if (error.code === statusCodes.IN_PROGRESS) {

        // operation (f.e. sign in) is in progress already
        alert(error.code)
        console.log(error)

      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {

        // play services not available or outdated
        alert(error.code)
        console.log(error)

      } else {

        // some other error happened
      }

    }

  }

  // Function for displaying create error
  renderCreateError() {

    const { errors } = this.props

    if (errors) {

      return (

        <View>

          <Text style={styles.errorTextStyle}>

            {errors}

          </Text>

        </View>

      )

    }

  }

  // Function for displaying create button
  renderButtonCreate() {

    const { textStyle } = styles;

    const { loader } = this.props

    if (loader) {

      return <Spinner size='large' />

    }

    return (

      <TouchableOpacity onPress={this.callGoogle.bind(this)}>

        <Text style={textStyle}>

          Sign Up with Google

        </Text>

      </TouchableOpacity>

    )

  }

  render() {

    const { buttonStyle } = styles;

    return (

      <View>

        <View style={buttonStyle}>

          {this.renderButtonCreate()}

        </View>

      </View>

    );

  }

};

const styles = StyleSheet.create({

  buttonStyle: {

    backgroundColor: '#2A233A',

    paddingVertical: 15,

    width: PHONE_WIDTH / 2.5,

    margin: 5,

    marginTop: -PHONE_HEIGHT/11,

    padding: 15,

    paddingBottom: 15,

    borderRadius: 10,

    marginLeft: PHONE_WIDTH / 1.7,

    height: PHONE_HEIGHT/14

  },

  textStyle: {

    textAlign: 'center',

    color: 'white',

    fontWeight: 'bold',

    fontSize: PHONE_WIDTH/30

  },

  errorTextStyle: {

    fontSize: 15,

    alignSelf: 'center',

    color: 'red',

    marginBottom: 10,

  },

})

const mapStateToProps = ({ createAfterOrder }) => {

  const { loader, errors } = createAfterOrder;

  return { loader, errors }

}

export default connect(mapStateToProps, {

  createAfterOrderGoogleSignIn

})(GoogleSignIn);