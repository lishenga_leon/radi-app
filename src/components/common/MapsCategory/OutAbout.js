import React, { Component } from "react";
import {
    StyleSheet, View, Dimensions, StatusBar,
    TouchableOpacity, Text, Platform, TextInput
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker, Circle } from 'react-native-maps';
import { Grid, Row, Col, Thumbnail, Right } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button } from '../';
import { connect } from 'react-redux';
import { getOutAndAboutUserLocation, restaurantOutAndAboutGetDetails, searchOutAndAboutRestaurantMap } from '../../../Actions';
import { apiIp } from "../../../Config";
import { Actions } from "react-native-router-flux";
import ExtraDimensions from 'react-native-extra-dimensions-android';
import axios from 'axios';
import Geolocation from 'react-native-geolocation-service';
import AsyncStorage from "@react-native-community/async-storage";

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

const PHONE_HEIGHT_ANDROID = ExtraDimensions.getRealWindowHeight()
const PHONE_HEIGHT_IOS = Dimensions.get("window").height

const PHONE_WIDTH_ANDROID = ExtraDimensions.getRealWindowWidth()
const PHONE_WIDTH_IOS = Dimensions.get("window").width

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const menuIcon = (
    <Icon
        name="ios-menu"
        size={30}
        style={{
            marginLeft: 15,
            color: 'black',
            marginTop: Platform.OS === 'ios' ? 10 : 5
        }}
        color="black"
        onPress={() => Actions.drawerOpen()}
    />
)

class OutAbout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            coord: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 1,
                longitudeDelta: 1,
            },
            restaurants: [],
            restaurant_name: null
        }
    }

    componentDidMount() {
        Geolocation.getCurrentPosition(
            (position) => {
                // const region = {
                //     latitude: -1.2691192,
                //     longitude: 36.8076891,
                //     latitudeDelta: 1,
                //     longitudeDelta: 1
                // }
                const region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: 1,
                    longitudeDelta: 1
                }
                this.setState({
                    coord: region
                })
                const data = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    radius: 2
                }
                AsyncStorage.getItem('token').then((token) => {

                    let headers = {

                        accept: "application/json",

                        "Accept-Language": "en-US,en;q=0.8",

                        'Content-Type': 'application/json',

                        "token": token

                    }
                    axios.post(apiIp + '/restaurant/searchProximity', data, { headers:headers }).then(
                        response => {
                            this.setState({
                                restaurants: response.data.data
                            })
                            this.props.getOutAndAboutUserLocation(region)
                        }
                    )
                })
            },
            (error) => console.log({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }

    // search for a restaurant on the map
    onSearchRestaurantName(text) {
        this.setState({
            restaurant_name: text
        })
        this.props.searchOutAndAboutRestaurantMap(text)
    }

    // click on a specific restaurant on map to display on the map
    marker(coordinates) {
        const { restaurants } = this.props
        const { coordinate } = coordinates
        this.state.restaurants.map((coord) => {
            const find = coord.location.latitude == coordinate.latitude && coord.location.longitude == coordinate.longitude
            if (find) {
                const region = {
                    latitude: coordinate.latitude,
                    longitude: coordinate.longitude,
                    latitudeDelta: 1,
                    longitudeDelta: 1
                }
                this.setState({
                    coord: region
                })
                const data = {
                    restaurant_id: coord.id
                }
                this.props.restaurantOutAndAboutGetDetails(data, coordinates.coordinate)
            }
        })
        restaurants.map((marker) => {
            const datas = marker.location.latitude == coordinate.latitude && marker.location.longitude == coordinate.longitude
            if (datas) {
                const region = {
                    latitude: coordinate.latitude,
                    longitude: coordinate.longitude,
                    latitudeDelta: 1,
                    longitudeDelta: 1
                }
                this.setState({
                    coord: region
                })
                const data = {
                    restaurant_id: marker.id
                }
                this.props.restaurantOutAndAboutGetDetails(data, coordinates.coordinate)
            }
        })
    }

    // render details of a restaurant clicked on the map
    renderLastDetails() {
        const {
            cardStyle, buttonContainer, thumbNailStyle, colStyles, foodDrinksStyles,
            nearbyStyle, colStyle, foodDrinksStyle, resultsStyle, cardStyles, nearbyStyles
        } = styles;
        if (this.props.joint.id) {
            const {
                id, name, location, description, lat, long,
                status, cover_image, created_at, updated_at
            } = this.props.joint
            return (
                <TouchableOpacity style={cardStyles} onPress={() => Actions.joint({ restaurant_id: id })}>
                    <Grid>
                        <Row size={1}>
                            <Col size={0.2} style={colStyle}>
                                <Icon name='md-arrow-up' size={30} color="#FFFFFF" style={{ marginLeft: 10, marginTop: 15 }} />
                            </Col>
                            <Col size={0.4} style={colStyle}>
                                <Text style={nearbyStyle}>{name}</Text>
                                <Text style={foodDrinksStyles}>{this.props.distance}</Text>
                                <Thumbnail source={{ uri: apiIp + cover_image }} style={thumbNailStyle} />
                            </Col>
                            <Col size={0.4} style={colStyle}>
                                <View style={buttonContainer}>
                                    <Button onPress={() => Actions.joint({ restaurant_id: id })}>
                                        GO THERE
                                    </Button>
                                </View>
                            </Col>
                        </Row>
                    </Grid>
                </TouchableOpacity>
            )
        } else {
            if (this.props.restaurants.length) {
                return (
                    <TouchableOpacity style={cardStyle} onPress={() => Actions.mapsrestaurants({ restaurants: this.props.restaurants })}>
                        <Grid>
                            <Row size={1}>
                                <Col size={0.2} style={colStyles}>
                                    <Icon name='md-arrow-up' size={30} color="#FFFFFF" style={{ marginLeft: 10, marginTop: 15 }} />
                                </Col>
                                <Col size={0.4} style={colStyles}>
                                    <Text style={nearbyStyles}>Nearby</Text>
                                    <Text style={foodDrinksStyles}>Food, drinks, places</Text>
                                </Col>
                                <Col size={0.4} style={colStyles}>
                                    <Text style={resultsStyle}>{this.props.total} results</Text>
                                </Col>
                            </Row>
                        </Grid>
                    </TouchableOpacity>
                )
            } else if (this.state.restaurants.length) {
                return (
                    <TouchableOpacity style={cardStyle} onPress={() => Actions.mapsrestaurants({ restaurants: this.state.restaurants })}>
                        <Grid>
                            <Row size={1}>
                                <Col size={0.2} style={colStyles}>
                                    <Icon name='md-arrow-up' size={30} color="#FFFFFF" style={{ marginLeft: 10, marginTop: 15 }} />
                                </Col>
                                <Col size={0.4} style={colStyles}>
                                    <Text style={nearbyStyles}>Nearby</Text>
                                    <Text style={foodDrinksStyles}>Food, drinks, places</Text>
                                </Col>
                                <Col size={0.4} style={colStyles}>
                                    <Text style={resultsStyle}>{this.props.total} results</Text>
                                </Col>
                            </Row>
                        </Grid>
                    </TouchableOpacity>
                )
            }else {
                return (
                    <TouchableOpacity style={cardStyle}>
                        <Grid>
                            <Row size={1}>
                                <Col size={0.2} style={colStyles}>
                                    <Icon name='md-arrow-up' size={30} color="#FFFFFF" style={{ marginLeft: 10, marginTop: 15 }} />
                                </Col>
                                <Col size={0.4} style={colStyles}>
                                    <Text style={nearbyStyles}>Nearby</Text>
                                    <Text style={foodDrinksStyles}>Food, drinks, places</Text>
                                </Col>
                                <Col size={0.4} style={colStyles}>
                                    <Text style={resultsStyle}>No results</Text>
                                </Col>
                            </Row>
                        </Grid>
                    </TouchableOpacity>
                )
            }
        }
    }

    // check if a person has searched restaurants so as to show restaurants near a person or not
    renderSearchRestaurants() {

        const { restaurant_name } = this.props

        if (restaurant_name == '') {
            return (
                this.state.restaurants.map(marker => (
                    <Marker
                        coordinate={marker.location}
                        title={marker.name}
                        description={marker.description}
                        onPress={e => this.marker(e.nativeEvent)}
                    />

                ))
            )
        }
    }

    render() {

        const { container, map, mapDrawerOverlay, userLocationStyle, navBarStyle, viewHeaderStyle } = styles;

        const { restaurants, restaurant_name, location } = this.props

        if(restaurants.length){

            return (
                <View>
                    <StatusBar
                        barStyle='dark-content'
                        backgroundColor="#4f6d7a"
                    />
                    <View>
                        <MapView
                            provider={PROVIDER_GOOGLE}
                            showUserLocation={true}
                            followsUserLocation={true}
                            showsBuildings={true}
                            loadingEnabled={true}
                            minZoomLevel={17}
                            userLocationAnnotationTitle='My Location'
                            region={location}
                            style={map}
                        >
                            <Circle
                                center={this.state.coord}
                                radius={20}
                                strokeWidth={1}
                                strokeColor="rgba(0, 0, 0, 0.1)"
                                fillColor="rgba(0, 0, 0, 0.1)"
                            />
                            <Marker
                                coordinate={this.state.coord}
                                title='My Location'
                                description='My Location'
                            />
    
                            {restaurants.map(marker => (
                                <Marker
                                    coordinate={marker.location}
                                    title={marker.name}
                                    description={marker.description}
                                    onPress={e => this.marker(e.nativeEvent)}
                                />
                            ))}
                        </MapView>
                        <View style={viewHeaderStyle}>
                            <Grid>
                                <Row size={1}>
                                    <Col size={0.2}>
                                        {menuIcon}
                                    </Col>
                                    <Col size={0.6}>
                                        <TextInput
                                            placeholder='Where to'
                                            autoCorrect={false}
                                            placeholderTextColor='black'
                                            keyboardType='default'
                                            value={this.state.restaurant_name}
                                            onChangeText={this.onSearchRestaurantName.bind(this)}
                                            style={{
                                                width: PHONE_WIDTH / 2,
                                                height: Platform.OS === 'ios' ? PHONE_HEIGHT / 5 : PHONE_HEIGHT / 5,
                                                marginTop: Platform.OS === 'ios' ? -PHONE_HEIGHT / 15 : -PHONE_HEIGHT_ANDROID / 16,
                                            }}
                                        />
                                    </Col>
                                    <Col size={0.2}>
                                        <Right>
                                            <Icon
                                                name="ios-search"
                                                size={30}
                                                style={{
                                                    marginTop: Platform.OS === 'ios' ? 10 : 5
                                                }}
                                                color="black"
                                            />
                                        </Right>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
    
                        <TouchableOpacity onPress={this.componentDidMount.bind(this)} style={userLocationStyle}>
                            <Icon name='md-locate' size={30} color="black" />
                        </TouchableOpacity>
                        <View style={mapDrawerOverlay} />
                        {this.renderLastDetails()}
                    </View>
                </View>
            )

        }

        return (
            <View>
                <StatusBar
                    barStyle='dark-content'
                    backgroundColor="#4f6d7a"
                />
                <View>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        showUserLocation={true}
                        followsUserLocation={true}
                        showsBuildings={true}
                        loadingEnabled={true}
                        minZoomLevel={17}
                        userLocationAnnotationTitle='My Location'
                        region={this.state.coord}
                        style={map}
                    >
                        <Circle
                            center={this.state.coord}
                            radius={20}
                            strokeWidth={1}
                            strokeColor="rgba(0, 0, 0, 0.1)"
                            fillColor="rgba(0, 0, 0, 0.1)"
                        />
                        <Marker
                            coordinate={this.state.coord}
                            title='My Location'
                            description='My Location'
                        />

                        {restaurants.map(marker => (
                            <Marker
                                coordinate={marker.location}
                                title={marker.name}
                                description={marker.description}
                                onPress={e => this.marker(e.nativeEvent)}
                            />
                        ))}
                    </MapView>
                    <View style={viewHeaderStyle}>
                        <Grid>
                            <Row size={1}>
                                <Col size={0.2}>
                                    {menuIcon}
                                </Col>
                                <Col size={0.6}>
                                    <TextInput
                                        placeholder='Where to'
                                        autoCorrect={false}
                                        placeholderTextColor='black'
                                        keyboardType='default'
                                        value={restaurant_name}
                                        onChangeText={this.onSearchRestaurantName.bind(this)}
                                        style={{
                                            width: PHONE_WIDTH / 2,
                                            height: Platform.OS === 'ios' ? PHONE_HEIGHT / 5 : PHONE_HEIGHT / 5,
                                            marginTop: Platform.OS === 'ios' ? -PHONE_HEIGHT / 15 : -PHONE_HEIGHT_ANDROID / 16,
                                        }}
                                    />
                                </Col>
                                <Col size={0.2}>
                                    <Right>
                                        <Icon
                                            name="ios-search"
                                            size={30}
                                            style={{
                                                marginTop: Platform.OS === 'ios' ? 10 : 5
                                            }}
                                            color="black"
                                        />
                                    </Right>
                                </Col>
                            </Row>
                        </Grid>
                    </View>
                    <TouchableOpacity onPress={this.componentDidMount.bind(this)} style={userLocationStyle}>
                        <Icon name='md-locate' size={30} color="black" />
                    </TouchableOpacity>
                    <View style={mapDrawerOverlay} />
                    {this.renderLastDetails()}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: PHONE_WIDTH,
        height: PHONE_HEIGHT / 1.15,
    },
    mapDrawerOverlay: {
        position: 'absolute',
        left: 0,
        top: PHONE_HEIGHT / 3,
        opacity: 0.0,
        height: PHONE_HEIGHT / 1.5,
        width: 100,
    },
    userLocationStyle: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        marginBottom: 20,
        marginRight: 20,
        backgroundColor: '#fff',
        borderRadius: 50,
        alignSelf: 'flex-end',
        marginTop: PHONE_HEIGHT / 1.8
    },
    cardStyle: {
        backgroundColor: '#231147',
        borderRadius: 10,
        width: PHONE_WIDTH,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 6 : PHONE_HEIGHT_ANDROID / 5.8,
        alignSelf: 'flex-end',
        marginTop: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 12 : PHONE_HEIGHT_ANDROID / 85,
    },
    cardStyles: {
        backgroundColor: '#454F63',
        borderRadius: 10,
        width: PHONE_WIDTH,
        height: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 6 : PHONE_HEIGHT_ANDROID / 5.8,
        alignSelf: 'flex-end',
        marginTop: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 12 : PHONE_HEIGHT_ANDROID / 85,
    },
    foodDrinksStyle: {
        color: '#FFFFFF'
    },
    nearbyStyle: {
        color: '#FFFFFF'
    },
    foodDrinksStyles: {
        color: '#FFFFFF',
        marginTop: 6
    },
    nearbyStyles: {
        color: '#FFFFFF',
        marginTop: 5
    },
    resultsStyle: {
        color: '#80838E',
        fontSize: PHONE_WIDTH / 15
    },
    colStyle: {
        marginTop: 10
    },
    colStyles: {
        marginTop: 20
    },
    thumbNailStyle: {
        height: 40,
        width: 120,
        marginTop: 10
    },
    buttonContainer: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 100,
        marginTop: 35,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    viewHeaderStyle: {
        backgroundColor: 'white',
        marginTop: Platform.OS === 'ios' ? PHONE_HEIGHT_IOS / 20 : PHONE_HEIGHT_ANDROID / 25,
        height: PHONE_HEIGHT / 15,
        width: PHONE_WIDTH / 1.1,
        borderRadius: 10,
        alignSelf: 'center'
    }
});

const mapStateToProps = ({ outandabout }) => {
    const { coord, restaurants, joint, distance, restaurant_name, total, location } = outandabout;
    return { coord, restaurants, joint, distance, restaurant_name, total, location };
};

export default connect(mapStateToProps, {
    getOutAndAboutUserLocation, restaurantOutAndAboutGetDetails, searchOutAndAboutRestaurantMap
})(OutAbout);
