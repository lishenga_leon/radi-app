import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { Container, Content, Form, Tab, Tabs, Grid, Col, Row, Icon, Header } from 'native-base';
import { Input, Button, Spinner } from '../index';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import GoogleSignIn from '../LoginAfterOrder/GoogleSignIn';
import FaceBookSignIn from '../LoginAfterOrder/FaceBookSignIn';
import {
    emailAddressAfterOrderChange, passwordAfterOrderChange, createAfterOrderUser, emailAfterOrderChanged,
    fullnameAfterOrderChanged, passAfterOrderChanged, phoneNumberAfterOrderChanged, loginAfterOrderUser, getUserDeviceAfterOrderUid
} from '../../../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import PushNotification from 'react-native-push-notification';
import { FIREBASE_SENDER_ID } from '../../../Config';
import AsyncStorage from '@react-native-community/async-storage';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class LoginAfterOrderSwiper extends Component {

    state = {
        icon: 'eye-off',
        password: true
    }

    componentDidMount() {
        // register device uid for push notification
        PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: function (token) {
                AsyncStorage.setItem('device_uid', JSON.stringify(token.token))
            },
            // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
            senderID: FIREBASE_SENDER_ID,

            // IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
                alert: true,
                badge: true,
                sound: true
            },

            // Should the initial notification be popped automatically
            // default: true
            popInitialNotification: true,

            /**
              * (optional) default: true
              * - Specified if permissions (ios) and token (android and ios) will requested or not,
              * - if not, you must call PushNotificationsHandler.requestPermissions() later
              */
            requestPermissions: true,
        });
        AsyncStorage.getItem('device_uid').then((token)=>{
            this.props.getUserDeviceAfterOrderUid(JSON.parse(token))
        })
    }

    Forgot() {
        Actions.forgot()
    }

    onEmailAddressChange(text) {
        this.props.emailAddressAfterOrderChange(text)
    }

    onEmailChange(text) {
        this.props.emailAfterOrderChanged(text)
    }

    onFullNameChange(text) {
        this.props.fullnameAfterOrderChanged(text)
    }

    onPhoneChange(text) {
        this.props.phoneNumberAfterOrderChanged(text)
    }

    onPasswordChange(text) {
        this.props.passwordAfterOrderChange(text)
    }

    onPassChange(text) {
        this.props.passAfterOrderChanged(text)
    }

    _changeIcon(){
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            password: !prevState.password
        }))
    }

    // Function for displaying login error
    renderLoginError() {
        const { error } = this.props
        if (error) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // Function for displaying create error
    renderCreateError() {
        const { errors } = this.props
        if (errors) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {errors}
                    </Text>
                </View>
            )
        }
    }

    // Function for initiating login redux logic
    onButtonPressLogin() {
        const { email, password, orderData } = this.props;

        const data = {
            email: email.toLowerCase(),
            password: password
        }
        this.props.loginAfterOrderUser(data, orderData)
    }

    // Function for displaying login button
    renderButtonLogin() {
        const { loading } = this.props

        if (loading) {
            return <Spinner size='large' />
        }

        return (
            <Button onPress={this.onButtonPressLogin.bind(this)}>
                CONTINUE
            </Button>
        )
    }

    // Function for initiating create redux logic
    onButtonPressCreate() {

        const { fullname, password, phone, email, orderData } = this.props;

        const body = {

            fullname: fullname,

            email: email.toLowerCase(),

            msisdn: phone,

            password: password,

        }

        this.props.createAfterOrderUser(body, orderData)
    }

    // Function for displaying create button
    renderButtonCreate() {
        const { loader } = this.props

        if (loader) {
            return <Spinner size='large' />
        }

        return (
            <Button onPress={this.onButtonPressCreate.bind(this)}>
                CONTINUE
            </Button>
        )
    }

    render() {
        const { textStyle, buttonContainer, buttonContainers, rowStyle } = styles;
        const { emailAddress, password, email, fullname, phone, pass, orderData } = this.props;

        return (
            <Container style={{ paddingTop: PHONE_HEIGHT / 10, backgroundColor: '#F2F2F2' }}>
                <Header transparent={true} iosBarStyle='dark-content' androidStatusBarColor='#4f6d7a' rounded  style={{display:'none'}}/>
                <FaceBookSignIn orderData={{ orderData }} />
                <GoogleSignIn orderData={{ orderData }} />
                <Tabs>
                    <Tab heading="SIGN IN"
                        textStyle={{ color: '#888' }}
                        activeTextStyle={{ color: '#000000' }}
                        tabStyle={{ backgroundColor: '#ffffff', borderRightColor: '#888', borderRightWidth: 1 }}
                        activeTabStyle={{ backgroundColor: '#ffffff', borderRightColor: '#888', borderRightWidth: 1 }}>

                        <Content style={{ backgroundColor: '#F2F2F2' }}>
                            <Form style={{
                                padding: 15
                            }}>
                                <Input
                                    placeholder='Email Address'
                                    returnKeyType='next'
                                    keyboardType='email-address'
                                    onChangeText={this.onEmailAddressChange.bind(this)}
                                    value={emailAddress}
                                />
                                <Grid>
                                    <Row size={1} style={rowStyle}>
                                        <Col size={0.8}>
                                            <Input
                                                placeholder='Password'
                                                returnKeyType='next'
                                                textContentType='password'
                                                keyboardType='default'
                                                autoCompleteType='password'
                                                secureTextEntry={this.state.password}
                                                onChangeText={this.onPasswordChange.bind(this)}
                                                value={password}
                                            />
                                        </Col>
                                        <Col size={0.2}>
                                            <Icon name={this.state.icon} onPress={()=> this._changeIcon()} style={{ alignSelf: 'center', marginTop: 15 }}/>
                                        </Col>
                                    </Row>
                                </Grid>

                                <TouchableOpacity onPress={this.Forgot.bind(this)}>
                                    <Text style={textStyle}>
                                        Forgot password or Email address
                                </Text>
                                </TouchableOpacity>
                                {this.renderLoginError()}
                                <View style={buttonContainer}>
                                    {this.renderButtonLogin()}
                                </View>
                            </Form>
                        </Content>
                    </Tab>
                    <Tab heading="SIGN UP"
                        textStyle={{ color: '#888' }}
                        activeTextStyle={{ color: 'white' }}
                        tabStyle={{ backgroundColor: '#ffffff' }}
                        activeTabStyle={{ backgroundColor: '#7B241C' }}>
                        <Content style={{ backgroundColor: '#F2F2F2' }}>
                            <Form style={{
                                padding: 15
                            }}>

                                <Input
                                    placeholder='Full Name'
                                    returnKeyType='next'
                                    keyboardType='default'
                                    onChangeText={this.onFullNameChange.bind(this)}
                                    value={fullname}
                                />
                                <Input
                                    placeholder='Email Address'
                                    returnKeyType='next'
                                    keyboardType='email-address'
                                    onChangeText={this.onEmailChange.bind(this)}
                                    value={email}
                                />
                                <Input
                                    placeholder='Phone Number'
                                    returnKeyType='next'
                                    keyboardType='numeric'
                                    onChangeText={this.onPhoneChange.bind(this)}
                                    value={phone}
                                />
                                <Grid>
                                    <Row size={1} style={rowStyle}>
                                        <Col size={0.8}>
                                            <Input
                                                placeholder='Password'
                                                returnKeyType='next'
                                                textContentType='password'
                                                keyboardType='default'
                                                autoCompleteType='password'
                                                secureTextEntry={this.state.password}
                                                onChangeText={this.onPassChange.bind(this)}
                                                value={pass}
                                            />
                                        </Col>
                                        <Col size={0.2}>
                                            <Icon name={this.state.icon} onPress={()=> this._changeIcon()} style={{ alignSelf: 'center', marginTop: 15 }}/>
                                        </Col>
                                    </Row>
                                </Grid>
                                {this.renderCreateError()}
                                <View style={buttonContainers}>
                                    {this.renderButtonCreate()}
                                </View>
                            </Form>
                        </Content>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
};


const styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: PHONE_WIDTH / 3,
        marginTop: 25,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    rowStyle:{
        backgroundColor: 'white', 
        height: PHONE_HEIGHT/15, 
        paddingHorizontal: 10, 
        borderRadius: 10, 
        marginBottom: 20,
    },
    textStyle: {
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15
    },
    buttonContainers: {
        backgroundColor: '#23273A',
        paddingVertical: 15,
        width: PHONE_WIDTH / 3,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    viewStyle: {
        backgroundColor: 'rgb(219, 220, 221)',
        marginTop: 200,
        marginLeft: 5,
        marginRight: 5
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
});

const mapStateToProps = ({ loginAfterOrder, createAfterOrder }) => {
    const { emailAddress, password, error, loading } = loginAfterOrder;
    const { fullname, phone, pass, email, loader, errors, device_uid } = createAfterOrder;

    return {
        emailAddress, password, error, loading, device_uid,
        pass, fullname, phone, email, loader, errors
    }
}

export default connect(mapStateToProps, {
    emailAddressAfterOrderChange, passwordAfterOrderChange, createAfterOrderUser, emailAfterOrderChanged,
    fullnameAfterOrderChanged, passAfterOrderChanged, phoneNumberAfterOrderChanged, loginAfterOrderUser, getUserDeviceAfterOrderUid
})(LoginAfterOrderSwiper);