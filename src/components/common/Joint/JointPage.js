import {
    Container, Content, Grid, Right,
    Row, Col, Card, Header,
} from 'native-base';
import { 
    StyleSheet, FlatList, Platform, 
    TouchableOpacity, Dimensions, StatusBar 
} from 'react-native';
import React, { Component } from 'react';
import { ImageBackground, Text, View } from 'react-native';
import StarRating from 'react-native-star-rating';
import { Button } from '../';
import { Actions } from 'react-native-router-flux';
import { apiIp } from '../../../Config'
import { connect } from 'react-redux';
import { getUserTokenRestaurant, showOffersModal } from '../../../Actions';
import List from './MenuList/List';
import Modal from "react-native-modal";
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();



class JointPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 2.5,
            data: []
        };
    }

    componentDidMount() {
        this.props.getUserTokenRestaurant(this.props.restaurant_id)
    }

    // function for toggling the offers modal
    toggleModal = (value) => {
        this.props.showOffersModal(value);
    }

    // function for showing modal of offers
    offersModal() {

        const { 
            modalHeaderStyle, modalHeaderTextStyle, modalButtonStyle, buttonModalTextStyle, 
            colModalStyle, modalHeaderColStyle, textModalHeaderStyle, textColStyle
        } 
        = styles

        return (
            <Modal
                deviceWidth={PHONE_WIDTH}
                isVisible={this.props.showModal}
            >
                <View style={{ flex: 1 }}>
                    <Header transparent rounded style={modalHeaderStyle} >
                        <Text style={modalHeaderTextStyle}>Offers</Text>
                    </Header>
                    <Content>
                        <Grid>
                            <Row size={1}>
                                <Col size={1} style={colModalStyle}>
                                    <Header transparent rounded style={modalHeaderColStyle}>
                                        <Text style={textModalHeaderStyle}> FIRST ORDER DISCOUNT  15% OFF</Text>
                                    </Header>
                                    <Text style={textColStyle}>
                                        Get 15% off your first order at Crust Gourmet Pizza Bar.
                                        Not available with other offers
                                    </Text>
                                </Col>
                            </Row>
                            <Row size={1}>
                                <Col size={1} style={colModalStyle}>
                                    <Header transparent rounded style={modalHeaderColStyle}>
                                        <Text style={textModalHeaderStyle}>10% OFF</Text>
                                    </Header>
                                    <Text style={textColStyle}>
                                        Order $30.00 or more and receive 10% off your order
                                        Not available with other offers
                                    </Text>
                                </Col>
                            </Row>
                        </Grid>
                        <View style={modalButtonStyle}>
                            <TouchableOpacity onPress={() => this.toggleModal(value=false)}>
                                <Text style={buttonModalTextStyle}>
                                    OK
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </Content>
                </View>
            </Modal>
        )
    }

    // function for rendering the foods in menus
    renderItem(menus) {
        return <List menus={menus} />
    }

    render() {

        const {
            imageBackground, headerStyle, contentStyle,
            textHeaderStyle, descriptionTextStyle, TimeTextStyle,
            phoneTextStyle, offersStyle, textHeaderUnderStyle,
            StarStyle, buttonContainers
        } = styles

        const { image, menu, restaurant_name, bookingData, restaurant_rating } = this.props

        return (
            <Container>
                <StatusBar
                    barStyle='dark-content'
                    backgroundColor="#4f6d7a"
                />
                <Content style={contentStyle}>
                    <ImageBackground source={{ uri: apiIp + image }} style={imageBackground}>
                        <Header transparent rounded style={headerStyle} >
                            <Text style={textHeaderStyle}>CRUST</Text>
                            <Text style={textHeaderUnderStyle}>{restaurant_name}</Text>
                        </Header>
                    </ImageBackground>

                    <View style={{
                        padding: 20,
                    }}>
                        <Right style={offersStyle}>
                            <TouchableOpacity onPress={()=> this.toggleModal(value=true)}>
                                <Card transparent >
                                    <View>
                                        <Text>Offers</Text>
                                        {this.offersModal()}
                                    </View>
                                </Card>
                            </TouchableOpacity> 
                        </Right>
                        <View>
                            <Text style={descriptionTextStyle}>{restaurant_name}</Text>
                            <StarRating
                                disabled={true}
                                emptyStar={'ios-star-outline'}
                                fullStar={'ios-star'}
                                halfStar={'ios-star-half'}
                                iconSet={'Ionicons'}
                                maxStars={5}
                                rating={restaurant_rating}
                                fullStarColor={'red'}
                                starSize={20}
                                containerStyle={StarStyle}
                            />
                        </View>
                    </View>

                    <Grid>
                        <Row size={1}>
                            <Col size={0.5}>
                                <Card transparent >
                                    <Text style={TimeTextStyle}>Open Door</Text>
                                    <Text style={TimeTextStyle}>09.00 - 00.00 </Text>
                                </Card>
                            </Col>
                            <Col size={0.5}>
                                <Card transparent >
                                    <Text style={phoneTextStyle}>Cuisine</Text>
                                    <Text style={phoneTextStyle}>Pizza</Text>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <FlatList
                                        data={menu}
                                        renderItem={this.renderItem}
                                        keyExtractor={(item, menus) => menus.toString()}
                                    />
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                    <View style={buttonContainers}>
                        <Button onPress={() => Actions.order({ restaurant_id: this.props.restaurant_id, image: image, bookingData: bookingData })}>
                            CHECK OUT
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    imageBackground: {
        width: '100%',
        height: PHONE_HEIGHT/3,
        paddingTop: 0
    },
    headerStyle: {
        borderRadius: 0,
        alignSelf: 'flex-start',
        height: 200
    },
    contentStyle: {
        padding: 0,
    },
    textHeaderStyle: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white',
        paddingTop: -280,
        paddingLeft: 10
    },
    descriptionTextStyle: {
        alignContent: 'center',
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 15,
        color: 'black',
        fontFamily: "Helvetica"
    },
    iconStyle: {
        paddingLeft: 40,
        color: 'black',
        fontSize: 20
    },
    phoneTextStyle: {
        fontSize: 15,
        color: 'black',
        fontFamily: "Helvetica",
        paddingLeft: 60
    },
    mapStyle: {
        height: 150,
        backgroundColor: 'black',
        margin: 10,
        borderRadius: 10
    },
    buttonStyle: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 200,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    offersStyle: {
        marginLeft: PHONE_WIDTH/1.5,
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10,
        width: 60,
        paddingRight: 5
    },
    textHeaderUnderStyle: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'white',
        paddingTop: 40,
        fontFamily: "Helvetica",
    },
    StarStyle: {
        width: 30
    },
    TimeTextStyle: {
        fontSize: 15,
        color: 'black',
        fontFamily: "Helvetica",
        paddingLeft: 60
    },
    buttonContainers: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 170,
        marginTop: 25,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    modalButtonStyle:{
        backgroundColor: '#FAFAFA',
        paddingVertical: 15,
        width: 170,
        marginTop: 25,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    modalHeaderStyle: {
        height: PHONE_HEIGHT/9,
        backgroundColor: '#7B241C'
    },
    modalHeaderTextStyle: {
        marginLeft: -PHONE_WIDTH/1.5, 
        marginTop: 7, 
        fontSize: 25,
        color: '#FFFFFF'
    },
    buttonModalTextStyle:{
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15
    },
    colModalStyle:{
        backgroundColor: 'white'
    },
    modalHeaderColStyle:{
        backgroundColor: '#FAFAFA'
    },
    textModalHeaderStyle:{
        margin: 10, 
        color: 'black', 
        fontSize: 15
    },
    textColStyle:{
        margin: 10, 
        color: 'black'
    }
})

const mapStateToProps = ({ jointPage }) => {
    const { menu, showModal, restaurant_name } = jointPage;
    return { menu, showModal, restaurant_name }
}

export default connect(mapStateToProps, {
    getUserTokenRestaurant, showOffersModal
})(JointPage);