import React, { Component } from 'react';
import {
    StyleSheet, TouchableOpacity,
    View, Text, FlatList, Dimensions, Platform
} from 'react-native'
import { connect } from 'react-redux';
import {
    menuGottenSuccessfully, selectMenu,
    checkedFood, numberOfPeople, selectFoods
} from '../../../../Actions';
import { Grid, Col, Row, Card, Thumbnail, Right } from 'native-base';
import { apiIp } from '../../../../Config';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/AntDesign';
import _ from 'lodash'
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
  ? Dimensions.get("window").height
  : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
  ? Dimensions.get("window").width
  : ExtraDimensions.getRealWindowWidth();

const downIcon = (
    <Icon
        name="down"
        size={20}
        color="black"
    />
)

const upIcon = (
    <Icon
        name="up"
        size={20}
        color="black"
    />
)

class List extends Component {

    constructor(props) {
        super(props)
        this.state = { refresh: false }
    }

    // function for choosing food using changing the index of the choosen food with new values if isSelect and selectedClass
    food(men) {
        men.item.isSelect = !men.item.isSelect;
        men.item.selectedClass = men.item.isSelect
            ? styles.selected
            : styles.CheckBoxStyle;
        const index = this.props.foods.findIndex(
            item => men.item.id === item.id
        );
        this.props.foods[index] = men.item;
        this.props.selectFoods(this.props.foods)
        this.setState({
            refresh: !this.state.refresh
        })
        const data = {
            id: this.props.menus.item.id,
            foods: this.props.foods
        }
        const MenuName = 'PeopleEating' + this.props.menus.item.id
        AsyncStorage.setItem(MenuName, JSON.stringify(data))
    }

    //function for rendering the up and down icon
    renderRightIcon() {
        if (this.props.expanded) {
            return (
                <Right>
                    {upIcon}
                </Right>
            )
        } else {
            return (
                <Right>
                    {downIcon}
                </Right>
            )
        }
    }

    // function for rendering the foods of a menu that has been choosen
    renderItem(men) {
        const { description, price, cover, id, selectedClass, name } = men.item
        const { checked, checker, expanded, numbers } = this.props
        const { thumbNailStyle, contentTextStyle, contentTextStyles, contentNameTextStyles, CheckBoxStyle } = styles
        if (expanded) {
            return (
                <TouchableOpacity onPress={this.food.bind(this, men)}>
                    <Grid>
                        <Row size={1}>
                            <Col size={0.2}>
                                <Thumbnail source={{ uri: apiIp + cover }} style={thumbNailStyle} />
                            </Col>
                            <Col size={0.7}>
                                <Card transparent >
                                    <Text style={contentNameTextStyles}>
                                        {name.toUpperCase()}
                                    </Text>
                                    <Text style={contentTextStyles}>
                                        {description}
                                    </Text>
                                    <Text style={contentTextStyle}>
                                        KSH {price}
                                    </Text>
                                </Card>
                            </Col>
                            <Col size={0.1}>
                                <Card transparent >
                                    <Icon name='checkcircle' style={[CheckBoxStyle, selectedClass]} onPress={this.food.bind(this, men)} checked={checker} />
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </TouchableOpacity>
            )
        }
    }

    // function used to render and also get the foods to be rendered and the menu name stored in phone storage
    render() {

        const { titleStyle, containerStyle, cardStyle } = styles;
        const { name, id } = this.props.menus.item

        return (
            <Card style={cardStyle}>
                <TouchableOpacity onPress={() => this.props.selectMenu({ menu_id: id, MenuName: 'PeopleEating' + id })}>
                    <View>
                        <View style={containerStyle}>
                            <Text style={titleStyle}>
                                {name}
                            </Text>
                            {this.renderRightIcon()}
                        </View>
                        <FlatList
                            data={this.props.foods}
                            renderItem={item => this.renderItem(item)}
                            keyExtractor={item => item.id.toString()}
                            extraData={this.state.refresh}
                        />
                    </View>
                </TouchableOpacity>
            </Card>
        );
    }
};

const styles = StyleSheet.create({
    cardStyle: {
        borderWidth: 1,
        width: PHONE_WIDTH / 1.12,
        marginLeft: PHONE_WIDTH / 30
    },
    thumbNailStyle: {
        margin: 20,
        height: 100,
        width: 100
    },
    CheckBoxStyle: {
        marginTop: 30,
        marginRight: 5,
        fontSize: 20
    },
    contentTextStyle: {
        fontWeight: 'bold',
        marginLeft: 60
    },
    contentTextStyles: {
        marginLeft: 60
    },
    contentNameTextStyles: {
        marginTop: 20,
        fontSize: 20,
        marginLeft: 60,
        fontWeight: 'bold'
    },
    titleStyle: {
        fontSize: 15,
        paddingLeft: 15,
        margin: 10
    },
    containerStyle: {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative'
    },
    selected: {
        color: "#FA7B5F",
        marginRight: 5,
        fontSize: 20
    }
});


const mapStateToProps = ({ listMenu }, ownProps) => {
    const foods = listMenu.foods.flat()
    const expanded = listMenu.menu_id === ownProps.menus.item.id
    const { number, checked, numbers, checker } = listMenu
    return { foods, number, checked, numbers, checker, expanded };
}

export default connect(mapStateToProps, {
    selectMenu, menuGottenSuccessfully,
    checkedFood, numberOfPeople, selectFoods
})(List);
