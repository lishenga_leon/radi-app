import { Grid, Row, Col, } from 'native-base';
import {
    StyleSheet, FlatList,
    Dimensions, Image, Text, Platform
} from 'react-native';
import React, { Component } from 'react';
import { apiIp } from '../../../Config';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');

class Foods extends Component {

    renderFoods = (item) => {

        const { food, quantity } = item.item
        const { thumbNailStyle, contentNameTextStyles } = styles
        return (
            <Grid>
                <Row size={1}>
                    <Col size={1}>
                        <Image source={{ uri: apiIp + '/media/food/picture/' + food.cover_image }} style={thumbNailStyle} />
                    </Col>
                </Row>
                <Row size={1}>
                    <Col size={1}>
                        <Text style={contentNameTextStyles}>
                            Food: 
                            <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                { food.name}
                            </Text>
                        </Text>
                        <Text style={contentNameTextStyles}>
                            Plates: 
                            <Text style={{ fontWeight: 'bold', fontSize: 10 }}>
                                { quantity}
                            </Text>
                        </Text>
                    </Col>
                </Row>
            </Grid>
        )
    }

    render() {

        const { foods } = this.props

        return (
            <FlatList
                data={foods}
                renderItem={this.renderFoods}
                keyExtractor={(item) => item.food.created_at}
            />
        );
    }
}

const styles = StyleSheet.create({
    thumbNailStyle: {
        height: 100,
        width: win.width / 1.5,
        borderRadius: 10,
    },
    contentNameTextStyles: {
        marginTop: 20,
        fontSize: 10,
        marginLeft: 20,
        marginBottom: 10,
    },
})

export default Foods;