import { Toast } from "native-base";

export const Toaster = () =>{
    return (
        Toast.show({
            text: 'Kindly check your email',
            buttonText: "Okay",
            type: "success"
        })
    )
}