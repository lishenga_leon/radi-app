import React from 'React';
import { TextInput, StyleSheet, Platform, Dimensions }  from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const Input = ({ 
    placeholder, 
    secureTextEntry, 
    onChangeText,
    value, 
    returnKeyType,  
    keyboardType 
}) =>{
    const { inputStyle } = styles;
    return (
        <TextInput 
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            autoCorrect={false}
            style={inputStyle}
            placeholderTextColor='black'
            keyboardType={keyboardType}
            value={value}
            onChangeText={onChangeText}
            returnKeyType={returnKeyType}
        />
    )
};

const styles = StyleSheet.create({
    inputStyle: {
        height: PHONE_HEIGHT/15,
        backgroundColor: 'white',
        color: 'black',
        marginBottom: 20,
        fontSize: PHONE_WIDTH/25,
        paddingHorizontal: 10,
        borderRadius: 10
    }, 
})

export { Input };