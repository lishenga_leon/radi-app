import React from 'React';
import { View, Image, StyleSheet }  from 'react-native';

const LoginLogo = () =>{
    const { logo, logoContainer } = styles;
    return (
        <View style={logoContainer}>
            <Image style={logo} source={require('../../../images/swift.png')}>

            </Image>
        </View>
    )
};

const styles = StyleSheet.create({
    logoContainer:{
        marginTop: -200,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    logo: {
        width: 300,
        height: 100
    },
})

export { LoginLogo };