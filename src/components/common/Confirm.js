import React from 'react';
import { Text, View, Modal, StyleSheet, TouchableOpacity } from 'react-native';

const Confirm = ({ visible, onUSA, onUK, onKE, onCN }) => {
    const { containerStyle, textStyles, buttonStyle } = styles;

    return(
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
            onRequestClose={() => {}}
        >
            <View style={containerStyle}>
                <TouchableOpacity onPress={onUSA} style={buttonStyle}>
                    <Text style={textStyles}>
                        USA
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={onUK} style={buttonStyle}>
                    <Text style={textStyles}>
                        UNITED KINGDOM
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={onKE} style={buttonStyle}>
                    <Text style={textStyles}>
                        KENYA
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={onCN} style={buttonStyle}>
                    <Text style={textStyles}>
                        CANADA
                    </Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center'
    },
    textStyles:{
        alignSelf: 'center',
        color: 'black',
        fontSize: 15,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center',
    },
    buttonStyle: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#b3b6bc',
        marginLeft: 15,
        marginRight: 5,
    }
})

export { Confirm };