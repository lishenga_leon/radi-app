import React from 'react';
import { Text, TouchableOpacity, StyleSheet, Platform, Dimensions } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
  ? Dimensions.get("window").height
  : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
  ? Dimensions.get("window").width
  : ExtraDimensions.getRealWindowWidth();

const Button = ( { onPress, children } ) =>{

    const { textStyle } = styles;

    return(
        <TouchableOpacity onPress={onPress}>
            <Text style={textStyle}>
                {children}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    textStyle:{
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: PHONE_WIDTH/25
    },
})

export { Button };