import React, { Component } from 'react';
import { StyleSheet, Image, View, TextInput, Dimensions, Text, Platform } from 'react-native'
import { 
    Container, Header, Card, 
    CardItem, Content, Row, Grid
} from 'native-base';
import { Button, Spinner } from './common'
import { connect } from 'react-redux';
import { addCardNumber, addCardCVC, addCardMonth, addCardYear, registerCard, editPayCard } from '../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';

const win = Dimensions.get('window');

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class AddCard extends Component{

    // fucntion to initiate redux change when typing card number
    onCardNumberChange(text){
        this.props.addCardNumber(text)
    }

    // function to initiate redux change when typing card cvc
    onCardCVCChange(text){
        this.props.addCardCVC(text)
    }

    // function to initiate redux change when typing card expire month
    onCardMonthChange(text){
        this.props.addCardMonth(text)
    }

    // function to initiate redux change when typing card expire year
    onCardYearChange(text){
        this.props.addCardYear(text)
    }

    // function for registering the card of a user
    addCard(){
        const { cardcvc, cardyear, cardnumber, cardmonth, orderData } = this.props
        if(orderData){
            const data = {
                number: cardnumber,
                exp_month: cardmonth,
                exp_year: cardyear,
                cvc: cardcvc,
                person_id: orderData.person_id,
                orderData: orderData
            }
            this.props.registerCard(data)
        }else{
            const data = {
                number: cardnumber,
                exp_month: cardmonth,
                exp_year: cardyear,
                cvc: cardcvc,
                person_id: this.props.person_id,
            }
            this.props.editPayCard(data) 
        }
        
    }

    // Function for displaying add card error
    renderAddCardError(){
        const { error } = this.props
        if(error){
            return(
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // Function for displaying add card button
    renderButtonAddCard(){
        const { load} = this.props

        if(load){
            return <Spinner size='large' />
        }

        return(
            <Button onPress={()=> this.addCard()}>
                SAVE
            </Button>
        )
    }

    render(){
        const { 
            headerStyle, imageStyle, cardStyle, expiresStyle, cardItemsCVCStyle,
            cardNumberStyle, cardItemStyle, cardItemsStyle, buttonContainer, cardItemsYearStyle
        } = styles

        const { cardcvc, cardyear, cardnumber, cardmonth } = this.props

        return(
            <Container>
                <Content>
                    <Header transparent rounded style={headerStyle} >
                        <Image source={require('../../images/visa.png')} style={imageStyle} />
                    </Header>
                    <Grid>
                        <Row size={1}>
                            <Card style={cardStyle}>
                                <CardItem style={cardItemStyle} cardBody>
                                    <TextInput 
                                        placeholder='Card Number'
                                        autoCorrect={false}
                                        style={cardNumberStyle}
                                        placeholderTextColor='#FFFFFF'
                                        keyboardType='default'
                                        value={cardnumber}
                                        onChangeText={this.onCardNumberChange.bind(this)}
                                    />
                                </CardItem>
                                <CardItem style={cardItemsStyle} cardBody>
                                    <TextInput 
                                        placeholder='Exp Month'
                                        autoCorrect={false}
                                        style={expiresStyle}
                                        placeholderTextColor='#FFFFFF'
                                        keyboardType='default'
                                        value={cardmonth}
                                        onChangeText={this.onCardMonthChange.bind(this)}
                                    />
                                </CardItem>
                                <CardItem style={cardItemsCVCStyle} cardBody>
                                    <TextInput 
                                        placeholder='CVC'
                                        autoCorrect={false}
                                        style={expiresStyle}
                                        placeholderTextColor='#FFFFFF'
                                        keyboardType='default'
                                        value={cardcvc}
                                        onChangeText={this.onCardCVCChange.bind(this)}
                                    />
                                </CardItem>
                                <CardItem style={cardItemsYearStyle} cardBody>
                                    <TextInput 
                                        placeholder='Exp Year'
                                        autoCorrect={false}
                                        style={expiresStyle}
                                        placeholderTextColor='#FFFFFF'
                                        keyboardType='default'
                                        value={cardyear}
                                        onChangeText={this.onCardYearChange.bind(this)}
                                    />
                                </CardItem>
                            </Card>
                        </Row>
                    </Grid>
                    {this.renderAddCardError()}
                    <View style={buttonContainer}>
                        {this.renderButtonAddCard()}
                    </View>
                </Content>
            </Container>
        )
    }
};

const styles = StyleSheet.create({
    headerStyle:{
        borderRadius: 0, 
        alignSelf: 'flex-start', 
        height: null 
    },
    buttonContainer:{
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: 170,
        marginTop:25,
        paddingTop:15,
        paddingBottom:15,
        alignSelf: 'center',
        borderRadius: 10
    },
    textHeaderStyle:{
        fontSize: 20,
        fontWeight: 'bold',
        color:'black',
        marginTop: 300,
        alignContent: 'center',
        alignSelf: 'center',
        marginLeft: 100,
        height: 300
    },
    forDeliverytStyle:{
        alignContent: 'center',
        fontSize: 15,
        marginLeft: 50, 
        color: '#A7ADB4',
        fontFamily: "Helvetica" 
    },
    cardStyle:{
        marginLeft: 17,
        marginTop: 45,
        marginBottom: 25,
        height: 180,
        width: PHONE_WIDTH -40,
        borderRadius: 10,
        backgroundColor: '#23273A'
    },
    imageStyle:{
        height: 60,
        width: 50,
        margin: 30
    },
    cardItemStyle:{
        width: PHONE_WIDTH/2.5, 
        marginTop: 20, 
        marginLeft: 20,
        marginBottom: 20,
        backgroundColor: '#454F63', 
        borderRadius: 10,
    },
    cardItemsCVCStyle:{
        marginLeft: 20,
        marginTop: 20,
        backgroundColor: '#454F63', 
        borderRadius: 10,
        width: PHONE_WIDTH/2.5,
        height: 60
    },
    cardItemsStyle:{
        marginLeft: PHONE_WIDTH/2,
        marginTop: -80,
        backgroundColor: '#454F63', 
        borderRadius: 10,
        width: 130,
        height: 60
    },
    cardItemsYearStyle:{
        marginLeft: PHONE_WIDTH/2,
        marginTop: -60,
        backgroundColor: '#454F63', 
        borderRadius: 10,
        width: 130,
        height: 60
    },
    cardNumberStyle: {
        height: 40,
        margin: 10,
        color: "white",
        fontSize: 17,
        paddingHorizontal: 10,
        backgroundColor: '#454F63'
    },
    expiresStyle: {
        height: 40,
        fontSize: 17,
        margin: 10,
        color: "white",
        paddingHorizontal: 10,
        backgroundColor: '#454F63',
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
})

const mapStateToProps = ({ addcard }) => {
    const { cardcvc, cardyear, cardnumber, cardmonth, load, error } = addcard;
    return { cardcvc, cardyear, cardnumber, cardmonth, load, error };
};

export default connect(mapStateToProps, {
    addCardNumber, addCardCVC, addCardMonth, addCardYear, registerCard, editPayCard
})(AddCard);