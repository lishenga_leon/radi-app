import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, Dimensions, Platform } from 'react-native'
import {
    Container, Header, Text, Card,
    CardItem, Content, Row, Grid, Col,
} from 'native-base'
import { Actions } from 'react-native-router-flux';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import AsyncStorage from '@react-native-community/async-storage';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');

class EditPay extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: null,
            date_card_added: null,
            person_id: null,
            card_last_four: null,
            card_brand: null
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("user").then(token => {
            const details = JSON.parse(token).data
            console.log(details)
            this.setState({
                email: details.email,
                date_card_added: Date(details.date_card_added),
                person_id: details.id,
                card_last_four: details.card_last_four,
                card_brand: details.card_brand
            })
        });
    }

    render() {
        const {
            headerStyle, textHeaderStyle,
            forDeliverytStyle, imageStyle, cardStyle
        } = styles
        return (
            <Container style={{ backgroundColor: '#34183E' }}>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                    <Text style={textHeaderStyle}>Payments Methods</Text>
                </Header>
                <Content>
                    <Grid style={{ backgroundColor: '#34183E' }}>
                        <Row>
                            <Col size={1}>
                                <TouchableOpacity onPress={() => Actions.addcard({ person_id: this.state.person_id })}>
                                    <Card style={cardStyle}>
                                        <CardItem cardBody>
                                            <Image source={require('../../images/visa.png')} style={imageStyle} />
                                        </CardItem>
                                        <CardItem>
                                            <Text style={forDeliverytStyle}>{this.state.email}</Text>
                                        </CardItem>
                                        <CardItem>
                                            <Text style={forDeliverytStyle}>Card brand {this.state.card_brand}</Text>
                                        </CardItem>
                                        <CardItem>
                                            <Text style={forDeliverytStyle}>Last 4 numbers {this.state.card_last_four}</Text>
                                        </CardItem>
                                        <CardItem>
                                            <Text style={forDeliverytStyle}>Added on {this.state.date_card_added}</Text>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
};

const styles = StyleSheet.create({
    headerStyle: {
        height: PHONE_HEIGHT / 5,
        backgroundColor: '#352644'
    },
    textHeaderStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        padding: 30,
        alignContent: 'center',
        alignSelf: 'center',
    },
    forDeliverytStyle: {
        alignContent: 'center',
        fontSize: 15,
        marginLeft: 50,
        color: '#A7ADB4',
        fontFamily: "Helvetica"
    },
    cardStyle: {
        marginLeft: 25,
        marginRight: 25,
        marginTop: 25,
        marginBottom: 25
    },
    imageStyle: {
        height: 60,
        width: 230,
        margin: 30,
        borderRadius: 10
    }
})

export default EditPay;