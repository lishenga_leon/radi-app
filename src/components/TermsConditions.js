import { 
    Container, Content, Grid, Textarea,
    Row, Col, Card, Icon, Header, 
} from 'native-base';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Platform } from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import AsyncStorage from '@react-native-community/async-storage';
import { apiIp } from '../Config';
import axios from 'axios';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');


class TermsConditions extends Component {

    constructor(props) {
        super(props)
        this.state = {
            terms: null,
        }
    }

    componentDidMount() {

        AsyncStorage.getItem('token').then((token) => {

            let headers = {

                accept: "application/json",

                "Accept-Language": "en-US,en;q=0.8",

                'Content-Type': 'application/json',

                "token": token

            }
            const data = {

            }

            axios.post(apiIp + '/RadiTermsConditions/getRadiTermsConditionsApp/', data, { headers: headers }).then(

                response => {

                    if (JSON.stringify(response.data.status_code) == 500) {

                        this.setState({
                            terms: null
                        })

                    } else if (JSON.stringify(response.data.status_code) == 200) {

                        this.setState({
                            terms: response.data.data
                        })

                    }

                }

            ).catch((error) => console.log(error))

        })
    }

    render() {

        const { 
            headerStyle, contentStyle,
            textHeaderStyle, mapStyle
        } = styles

        if(this.state.terms == null){

            return (
                <Container>
                    <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                        <TouchableOpacity onPress={() => Actions.onBack()}>
                            <Icon style={{ marginTop: 40, fontSize: 20, color: 'white', marginLeft: -85  }} name="arrow-back" />
                        </TouchableOpacity>
                        <Text style={textHeaderStyle}>Terms and Conditions</Text>
                    </Header>
                    <Content style={contentStyle}>
    
                        <Grid style={{ marginTop: 40 }}>
                            <Row size={1}>
                                <Col size={1}>
                                    <Card transparent >
                                        <View style={mapStyle}>
    
                                        </View>
                                    </Card>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>
    
                </Container>
            );

        }

        return (
            <Container>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                    <TouchableOpacity onPress={() => Actions.onBack()}>
                        <Icon style={{ marginTop: 40, fontSize: 20, color: 'white', marginLeft: -85  }} name="arrow-back" />
                    </TouchableOpacity>
                    <Text style={textHeaderStyle}>Terms and Conditions</Text>
                </Header>
                <Content style={contentStyle}>

                    <Grid style={{ marginTop: 40 }}>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <Textarea
                                        rowSpan={5}
                                        editable={false}
                                        bordered
                                        placeholderTextColor="black"
                                        maxLength={100000}
                                        value={this.state.terms.message}
                                        style={mapStyle}
                                    />
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle:{
        height: PHONE_HEIGHT/5,
        backgroundColor: '#352644' 
    },
    textHeaderStyle:{
        fontSize: 25,
        fontWeight: 'bold',
        color:'white',
        padding: 30,
        alignContent: 'center',
        alignSelf: 'center',
        backgroundColor: '#352644' 
    },
    contentStyle:{
        padding: 0,
        backgroundColor: '#34183E',
    },
    mapStyle:{
        height: PHONE_HEIGHT / 1.5,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 10
    },

})

export default TermsConditions;