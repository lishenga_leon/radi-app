import React, { Component } from 'react';
import { StyleSheet, SafeAreaView, StatusBar } from 'react-native';
import  LoginSwiper  from './common/Swiper/LoginSwiper';

class Login extends Component {

    render() {

        const { container } = styles;
        
        return (
            <SafeAreaView style={container}>
                <StatusBar barStyle="light-content"/>
                <LoginSwiper/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'rgb(219, 220, 221)',
        flexDirection: 'column',
    },
})

export default Login;