import {
    Container, Content, Grid,
    Row, Col, Card, Icon, Header,
} from 'native-base';
import { 
    StyleSheet, ImageBackground, Text, 
    View, Dimensions, Platform, StatusBar 
} from 'react-native';
import React, { Component } from 'react';
import { Button } from './common'
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { apiIp } from '../Config';
import { restaurantDetails } from '../Actions';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
  ? Dimensions.get("window").height
  : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
  ? Dimensions.get("window").width
  : ExtraDimensions.getRealWindowWidth();

class Joint extends Component {

    componentDidMount() {
        const data = {
            restaurant_id: this.props.restaurant_id
        }
        this.props.restaurantDetails(data)
    }
    render() {

        const {
            imageBackground, headerStyle, contentStyle, textStyle,
            textHeaderStyle, descriptionTextStyle, descriptionViewStyle,
            iconStyle, phoneTextStyle, mapStyle, buttonStyle, map
        } = styles

        const { coord, joint } = this.props

        const {
            id, name, location, description, lat, long, max_capacity,
            status, cover_image, created_at, updated_at, phone_number, rating
        } = joint

        return (
            <Container>
                <Content style={contentStyle}>
                <StatusBar
                    barStyle='light-content'
                    backgroundColor="#4f6d7a"
                />
                    <ImageBackground source={{ uri: apiIp + cover_image }} style={imageBackground}>
                        <Header transparent rounded style={headerStyle} >
                            <Text style={textHeaderStyle}>{name}</Text>
                        </Header>
                    </ImageBackground>

                    <View style={{
                        padding: 20,
                    }}>
                        <Text style={descriptionTextStyle}>Description</Text>

                        <View style={descriptionViewStyle}>
                            <View>
                                <Text style={textStyle}>
                                    {description}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <Grid>
                        <Row size={1}>
                            <Col size={0.4}>
                                <Card transparent >
                                    <Icon style={iconStyle} name="call" />
                                </Card>
                            </Col>
                            <Col size={0.6}>
                                <Card transparent >
                                    <Text style={phoneTextStyle}>{phone_number}</Text>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>

                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <View style={mapStyle}>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            showUserLocation={true}
                                            followsUserLocation={true}
                                            showsBuildings={true}
                                            loadingEnabled={true}
                                            userLocationAnnotationTitle='My Location'
                                            region={coord}
                                            style={map}
                                        >
                                            <Marker
                                                coordinate={coord}
                                                title={name}
                                                description={description}
                                            />
                                        </MapView>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <View style={buttonStyle}>
                                        <Button onPress={() => Actions.bookings({
                                            restaurant_id: id,
                                            image: cover_image,
                                            naming: name,
                                            max_capacity: max_capacity,
                                            rating: rating
                                        })}>
                                            BOOKINGS
                                    </Button>
                                    </View>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>

                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    imageBackground: {
        width: '100%',
        height: PHONE_HEIGHT/3,
        paddingTop: 0
    },
    headerStyle: {
        borderRadius: 0,
        alignSelf: 'flex-start',
        height: 300
    },
    contentStyle: {
        padding: 0,
        backgroundColor: '#2A2E43',
        height: PHONE_HEIGHT
    },
    textHeaderStyle: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white',
        paddingTop: PHONE_HEIGHT/15,
        paddingLeft: 10
    },
    descriptionTextStyle: {
        alignContent: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 15,
        color: 'white',
        fontFamily: "Helvetica"
    },
    descriptionViewStyle: {
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row'
    },
    textStyle: {
        fontSize: 14,
        color: 'white',
        fontFamily: "Helvetica",
        paddingTop: 10
    },
    iconStyle: {
        paddingLeft: 40,
        color: 'white',
        fontSize: 20
    },
    phoneTextStyle: {
        fontSize: 20,
        color: 'white',
        fontFamily: "Helvetica",
    },
    mapStyle: {
        height: 150,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 10,
    },
    buttonStyle: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: PHONE_WIDTH/3,
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        alignSelf: 'center',
        borderRadius: 10
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },

})

const mapStateToProps = ({ joints }) => {
    const { joint, coord } = joints;
    return { joint, coord }
}

export default connect(mapStateToProps, {
    restaurantDetails
})(Joint);