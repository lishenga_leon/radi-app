import {
    Container, Content, Grid,
    Row, Col, Header, Right, Form
} from 'native-base';
import {
    StyleSheet, ImageBackground, Text, Dimensions, Platform,
    View, TouchableWithoutFeedback, TouchableOpacity, StatusBar
} from 'react-native';
import { Button, Input, Spinner } from './common'
import React, { Component } from 'react';
import { apiIp } from '../Config';
import * as Progress from 'react-native-progress';
import DateTimePicker from "react-native-modal-datetime-picker";
import { connect } from 'react-redux';
import {
    numberOfSeatsChanged, dateTimeChangedBooking,
    dateTimeVisibleModal, makeABooking, typeOfOrder
} from '../Actions';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import RNPickerSelect from 'react-native-picker-select';


// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();


const ORDER_TYPE = [
    {
        label: 'Eat in',
        value: 'EATIN',
    },
    {
        label: 'Take out',
        value: 'TAKEOUT',
    },
];


class Bookings extends Component {

    constructor(props) {
        super(props);

        this.inputRefs = {
            order_type: null,
        };

        this.state = {
            order_type: undefined,
        };
    }

    // function for showing the datetime modal
    showDateTimePicker = () => {
        this.props.dateTimeVisibleModal();
    };

    // function for hiding the datetime modal
    hideDateTimePicker = () => {
        this.props.dateTimeVisibleModal();
    };

    // function for initiating datetime redux code
    handleDatePicked = date => {
        this.props.dateTimeChangedBooking(date)
        this.hideDateTimePicker();
    };

    // Function for initiating number of people redux logic
    bookingNumberPeopleChange(text) {
        this.props.numberOfSeatsChanged(text)
    }

    // Function for initiating booking redux logic
    onButtonPressBook() {

        const { image, naming, restaurant_id, datetimeBooking, numberSeats, rating, order_type } = this.props

        if (numberSeats == undefined) {

            const data = {
                datetimeBooking: JSON.stringify(datetimeBooking),
                numberSeats: 1,
                restaurant_id: restaurant_id,
                image: image,
                naming: naming,
                rating: rating,
                order_type: order_type
            }

            this.props.makeABooking(data)

        } else {

            const data = {
                datetimeBooking: JSON.stringify(datetimeBooking),
                numberSeats: numberSeats,
                restaurant_id: restaurant_id,
                image: image,
                naming: naming,
                rating: rating,
                order_type: order_type
            }

            this.props.makeABooking(data)

        }
    }

    // Function for displaying booking error
    renderBookError() {
        const { error } = this.props
        if (error) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // function for rendering the book button
    renderButtonBook() {
        const { load } = this.props

        if (load) {
            return <Spinner size='large' />
        }

        return (
            <Button onPress={this.onButtonPressBook.bind(this)}>
                BOOK
            </Button>
        )
    }

    // function for showing the field for displaying datetime
    renderButtonDate() {

        const { dateTextStyle, buttonDateContainer, pickedDateTextStyle } = styles

        const { datetimeBooking } = this.props

        if (datetimeBooking) {
            return (
                <View style={buttonDateContainer}>
                    <TouchableWithoutFeedback onPress={() => this.showDateTimePicker()}>
                        <Text style={pickedDateTextStyle}>{datetimeBooking.toString()}</Text>
                    </TouchableWithoutFeedback>
                </View>
            )
        } else {
            return (
                <View style={buttonDateContainer}>
                    <TouchableWithoutFeedback onPress={() => this.showDateTimePicker()}>
                        <Text style={dateTextStyle}>Pick a Date and Time</Text>
                    </TouchableWithoutFeedback>
                </View>
            )
        }
    }

    render() {

        const {
            imageBackground, headerStyle, contentStyle, progressBarStyle, textSkipStyle,
            textHeaderStyle, textHeaderUnderStyle, buttonContainer
        } = styles

        const { image, restaurant_id, numberSeats, isDateTimePickerVisible, naming, max_capacity, datetimeBooking, order_type } = this.props

        const placeholder = {
            label: 'Type of order',
            value: null,
            color: 'black',

        };

        return (
            <Container>
                <Content style={contentStyle}>
                    <StatusBar
                        barStyle='light-content'
                        backgroundColor="#4f6d7a"
                    />
                    <ImageBackground source={{ uri: apiIp + image }} style={imageBackground}>
                        <View style={headerStyle}>
                            <Header transparent rounded>
                                <Text style={textHeaderStyle}>{naming}</Text>
                            </Header>
                            <Header transparent rounded>
                                <Text style={textHeaderUnderStyle}>Bookings</Text>
                            </Header>
                        </View>
                    </ImageBackground>
                    <Right>
                        <Text style={{ color: 'white', marginTop: 40 }}>Capacity</Text>
                        <Progress.Bar color={'white'} borderColor={'white'} style={progressBarStyle} progress={max_capacity} width={70} />
                    </Right>
                    <Grid>
                        <Row size={1}>
                            <Col size={1}>
                                <RNPickerSelect
                                    placeholder={placeholder}
                                    items={ORDER_TYPE}
                                    onValueChange={value => {
                                        this.props.typeOfOrder(value)
                                    }}
                                    style={pickerSelectStyles}
                                    value={order_type}
                                    useNativeAndroidPickerStyle={false}
                                    ref={el => {
                                        this.inputRefs.order_type = el;
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                <Form style={{
                                    padding: 15
                                }}>
                                    <Input
                                        placeholder='Number of People'
                                        keyboardType='numeric'
                                        onChangeText={this.bookingNumberPeopleChange.bind(this)}
                                        value={numberSeats}
                                    />
                                </Form>
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                {this.renderButtonDate()}
                                <DateTimePicker
                                    isVisible={isDateTimePickerVisible}
                                    onConfirm={this.handleDatePicked}
                                    onCancel={this.hideDateTimePicker}
                                    mode='datetime'
                                />
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                {this.renderBookError()}
                                <View style={buttonContainer}>
                                    {this.renderButtonBook()}
                                </View>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    textSkipStyle: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15
    },
    buttonContainer: {
        backgroundColor: '#7B241C',
        paddingVertical: 15,
        width: PHONE_WIDTH / 3,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    dateTextStyle: {
        color: 'black',
        fontSize: PHONE_WIDTH / 25,
        marginLeft: 20,
        marginBottom: 5,
        height: PHONE_HEIGHT / 30
    },
    pickedDateTextStyle: {
        color: 'black',
        fontSize: PHONE_WIDTH / 30,
        marginLeft: 20,
        marginBottom: 5,
        height: PHONE_HEIGHT / 30
    },
    buttonDateContainer: {
        backgroundColor: 'white',
        paddingVertical: 15,
        height: PHONE_HEIGHT / 15,
        width: PHONE_WIDTH - 30,
        margin: 10,
        paddingBottom: 15,
        alignSelf: 'center',
        borderRadius: 10
    },
    imageBackground: {
        width: '100%',
        height: PHONE_HEIGHT / 3,
        paddingTop: 0
    },
    headerStyle: {
        borderRadius: 0,
        marginTop: PHONE_HEIGHT / 20,
        height: PHONE_HEIGHT / 2.5
    },
    textHeaderStyle: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white',
    },
    contentStyle: {
        padding: 0,
        backgroundColor: '#2A2E43'
    },
    progressBarStyle: {
        marginLeft: 200,
        marginTop: 8,
        height: 8
    },
    textHeaderUnderStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center',
        fontFamily: "Helvetica",
    },
    cardStyle: {
        marginLeft: 25,
        marginRight: 25,
        marginTop: 25,
        marginBottom: 25,
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red',
        margin: 10
    },
})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        backgroundColor: 'white',
        paddingVertical: 15,
        height: PHONE_HEIGHT / 15,
        width: PHONE_WIDTH - 30,
        margin: 10,
        paddingBottom: 15,
        fontSize: PHONE_WIDTH / 25,
        alignSelf: 'center',
        color: 'black',
        borderRadius: 10 // to ensure the text is never behind the icon
    },
    inputAndroid: {
        backgroundColor: 'white',
        paddingVertical: 15,
        height: PHONE_HEIGHT / 15,
        width: PHONE_WIDTH - 30,
        margin: 10,
        paddingBottom: 15,
        fontSize: PHONE_WIDTH / 25,
        alignSelf: 'center',
        color: 'black',
        borderRadius: 10 // to ensure the text is never behind the icon
    },
});

const mapStateToProps = ({ bookings }) => {
    const { numberSeats, datetimeBooking, load, error, isDateTimePickerVisible, order_type } = bookings;
    return { numberSeats, datetimeBooking, load, error, isDateTimePickerVisible, order_type };
};

export default connect(mapStateToProps, {
    numberOfSeatsChanged, dateTimeChangedBooking, dateTimeVisibleModal, makeABooking, typeOfOrder
})(Bookings);