import React, { Component } from 'react';
import {
    StyleSheet, Image, TouchableOpacity, Modal,
    TextInput, Platform, Dimensions, View
} from 'react-native'
import {
    Container, Header, Text,
    Card, CardItem, Content,
    Row, Grid, Col,
} from 'native-base'
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { makePayment, orderPhoneNumber } from '../Actions';
import { Spinner } from './common'
import ExtraDimensions from 'react-native-extra-dimensions-android';
import AsyncStorage from '@react-native-community/async-storage';
import { WebView } from 'react-native-webview';
import { apiIp } from '../Config';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

class Payments extends Component {

    constructor(props) {
        super(props)
        this.state = {
            card_last_four: null,
            card_brand: null,
            showModal: false,
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("user").then(token => {
            const details = JSON.parse(token).data
            this.setState({
                card_last_four: details.card_last_four,
                card_brand: details.card_brand
            })
        });
    }

    //function for paying via mpesa
    payMpesa() {

        const {
            bookingData, restaurant_id, restaurant_image, person_id, fullname,
            foods, totalFoodPrice, msisdn, order_type, expected_time
        } = this.props.orderData

        if (foods.length) {

            const data = {
                restaurant_id: restaurant_id,
                person_id: person_id,
                fullname: fullname,
                foods: foods,
                totalFoodPrice: totalFoodPrice,
                msisdn: msisdn,
                restaurant_image: restaurant_image,
                order_type: order_type,
                bookingData: bookingData,
                expected_time: JSON.parse(expected_time),
                type: 'MPESA',
                instructions: bookingData.instructions
            }

            this.props.makePayment(data)

        } else {

            return;

        }

    }

    onPhoneChange(text) {

        this.props.orderPhoneNumber(text)

    }

    // Pay with card via visa or stripe
    payVisa() {

        AsyncStorage.getItem('user').then((user) => {

            const details = JSON.parse(user)

            if (details.data.card_last_four == 0) {

                Actions.addcard({ orderData: this.props.orderData })

            } else {

                const {
                    bookingData, restaurant_id, restaurant_image, person_id, expected_time,
                    fullname, foods, totalFoodPrice, msisdn, order_type, instructions
                } = this.props.orderData

                if (foods.length) {

                    const data = {
                        restaurant_id: restaurant_id,
                        person_id: person_id,
                        fullname: fullname,
                        foods: foods,
                        totalFoodPrice: totalFoodPrice,
                        msisdn: msisdn,
                        restaurant_image: restaurant_image,
                        order_type: order_type,
                        bookingData: bookingData,
                        expected_time: JSON.parse(expected_time),
                        type: 'CARD',
                        instructions: instructions
                    }

                    this.props.makePayment(data)

                } else {

                    return;

                }

            }

        })

    }

    // Function for displaying Pay error
    renderPayError() {
        const { error } = this.props
        if (error) {
            return (
                <View>
                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>
                </View>
            )
        }
    }

    // handle response from paypal
    handlePayPalResponse = (data, dating) => {
        if (data.title == 'success') {
            this.setState({ showModal: false })
            this.props.makePayment(dating)
        } else if (data.title == 'cancel') {
            this.setState({ showModal: false })
        } else {
            return;
        }
    }

    // function to pay with paypal
    renderPayPayPal() {

        const { forDeliverytStyle, imageStyle, cardStyle } = styles

        const {
            bookingData, restaurant_id, restaurant_image, person_id, fullname,
            foods, totalFoodPrice, msisdn, order_type, email, instructions, expected_time
        } = this.props.orderData

        if (foods.length) {

            const dating = {
                restaurant_id: restaurant_id,
                person_id: person_id,
                fullname: fullname,
                foods: foods,
                totalFoodPrice: totalFoodPrice,
                msisdn: msisdn,
                restaurant_image: restaurant_image,
                order_type: order_type,
                bookingData: bookingData,
                expected_time: JSON.parse(expected_time),
                type: 'PAYPAL',
                instructions: instructions
            }

            // const datas = { 'id': 'PAYID-LUKVOKI1J1689818D763121K', 'intent': 'sale', 'state': 'approved', 'cart': '4FV8637519095144C', 'payer': { 'payment_method': 'paypal', 'status': 'VERIFIED', 'payer_info': { 'email': 'lishengaleon-buyer@gmail.com', 'first_name': 'test', 'last_name': 'buyer', 'payer_id': 'WFBRF7LSGFUSN', 'shipping_address': { 'recipient_name': 'test buyer', 'line1': '1 Main St', 'city': 'San Jose', 'state': 'CA', 'postal_code': '95131', 'country_code': 'US' }, 'country_code': 'US' } }, 'transactions': [{ 'amount': { 'total': '5.00', 'currency': 'USD', 'details': {} }, 'payee': { 'merchant_id': 'XGHLCW2EZABRU', 'email': 'lishengaleon-facilitator@gmail.com' }, 'description': 'This is the payment transaction description.', 'item_list': { 'items': [{ 'name': 'item', 'sku': 'item', 'price': '5.00', 'currency': 'USD', 'quantity': 1 }], 'shipping_address': { 'recipient_name': 'test buyer', 'line1': '1 Main St', 'city': 'San Jose', 'state': 'CA', 'postal_code': '95131', 'country_code': 'US' }, 'shipping_options': [] }, 'related_resources': [{ 'sale': { 'id': '86P22570PT511790M', 'state': 'completed', 'amount': { 'total': '5.00', 'currency': 'USD', 'details': { 'subtotal': '5.00' } }, 'payment_mode': 'INSTANT_TRANSFER', 'protection_eligibility': 'ELIGIBLE', 'protection_eligibility_type': 'ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE', 'transaction_fee': { 'value': '0.45', 'currency': 'USD' }, 'parent_payment': 'PAYID-LUKVOKI1J1689818D763121K', 'create_time': '2019-06-27T23:55:41Z', 'update_time': '2019-06-27T23:55:41Z', 'links': [{ 'href': 'https://api.sandbox.paypal.com/v1/payments/sale/86P22570PT511790M', 'rel': 'self', 'method': 'GET' }, { 'href': 'https://api.sandbox.paypal.com/v1/payments/sale/86P22570PT511790M/refund', 'rel': 'refund', 'method': 'POST' }, { 'href': 'https://api.sandbox.paypal.com/v1/payments/payment/PAYID-LUKVOKI1J1689818D763121K', 'rel': 'parent_payment', 'method': 'GET' }] } }] }], 'redirect_urls': { 'return_url': 'http://localhost:8000/paypal/transactionResult/?paymentId=PAYID-LUKVOKI1J1689818D763121K', 'cancel_url': 'http://localhost:8000/paypal/transactionCancelled/' }, 'create_time': '2019-06-27T23:55:42Z', 'update_time': '2019-06-27T23:55:39Z', 'links': [{ 'href': 'https://api.sandbox.paypal.com/v1/payments/payment/PAYID-LUKVOKI1J1689818D763121K', 'rel': 'self', 'method': 'GET' }] }
            // console.log(datas)

            return (
                <View>
                    <Modal
                        visible={this.state.showModal}
                        onRequestClose={() => this.setState({ showModal: false })}
                    >
                        <WebView
                            source={{ uri: apiIp + '/paypal/UI/' }}
                            onNavigationStateChange={data => this.handlePayPalResponse(data, dating)}
                            injectedJavaScript={`
                            setTimeout(() => {
                                document.getElementById('person_id').value=${JSON.stringify(person_id)}
                                document.getElementById('restaurant_id').value=${JSON.stringify(restaurant_id)}
                                document.getElementById('totalFoodPrice').value=${JSON.stringify(totalFoodPrice)}
                                document.f1.submit();
                            }, 100);
                        `}
                        />
                    </Modal>
                    <TouchableOpacity onPress={() => this.setState({ showModal: true })}>
                        <Card style={cardStyle}>
                            <CardItem cardBody>
                                <Image source={require('../../images/paypal.png')} style={imageStyle} />
                            </CardItem>
                            <CardItem>
                                <Text style={forDeliverytStyle}>{email}</Text>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                </View>
            )

        } else {

            return (
                <View>
                    <TouchableOpacity>
                        <Card style={cardStyle}>
                            <CardItem cardBody>
                                <Image source={require('../../images/paypal.png')} style={imageStyle} />
                            </CardItem>
                            <CardItem>
                                <Text style={forDeliverytStyle}>{email}</Text>
                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    // Function for displaying mpesa button
    renderMpesaButton() {
        const { loadMpesa, msisdn } = this.props
        const { forDeliverytStyle, imageStyle, cardStyle } = styles

        if (loadMpesa) {
            return <Spinner size='large' />
        }

        if (this.props.orderData.msisdn == '') {
            return (
                <TouchableOpacity onPress={this.payMpesa.bind(this)}>
                    {this.renderPayError()}
                    <Card style={cardStyle}>
                        <CardItem cardBody>
                            <Image source={require('../../images/lipaNaMpesa.png')} style={imageStyle} />
                        </CardItem>
                        <CardItem>
                            <TextInput
                                placeholder='Phone Number'
                                keyboardType='numeric'
                                onChangeText={this.onPhoneChange.bind(this)}
                                value={msisdn}
                                style={{ width: 50 }}
                            />
                        </CardItem>
                        <CardItem>
                            <Text style={forDeliverytStyle}>Till N.O 2839263</Text>
                        </CardItem>
                    </Card>
                </TouchableOpacity>
            )
        }

        return (
            <TouchableOpacity onPress={this.payMpesa.bind(this)}>
                {this.renderPayError()}
                <Card style={cardStyle}>
                    <CardItem cardBody>
                        <Image source={require('../../images/lipaNaMpesa.png')} style={imageStyle} />
                    </CardItem>
                    <CardItem>
                        <Text style={forDeliverytStyle}>Till N.O 2839263</Text>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        )
    }

    // Function for displaying visa button
    renderVisaButton() {
        const { loadCard } = this.props
        const { email, date_card_added } = this.props.orderData
        const { forDeliverytStyle, imageStyle, cardStyle } = styles

        if (loadCard) {
            return <Spinner size='large' />
        }

        return (
            <TouchableOpacity onPress={() => this.payVisa()}>
                <Card style={cardStyle}>
                    <CardItem cardBody>
                        <Image source={require('../../images/visa.png')} style={imageStyle} />
                    </CardItem>
                    <CardItem>
                        <Text style={forDeliverytStyle}>{email}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={forDeliverytStyle}>Card brand {this.state.card_brand}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={forDeliverytStyle}>Last 4 numbers {this.state.card_last_four}</Text>
                    </CardItem>
                    <CardItem>
                        <Text style={forDeliverytStyle}>Added on {Date(date_card_added)}</Text>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        )
    }

    render() {

        const { headerStyle, textHeaderStyle } = styles

        return (
            <Container>
                <Content>
                    <Header transparent rounded style={headerStyle} >
                        <Text style={textHeaderStyle}>Payments Methods</Text>
                    </Header>
                    <Grid>
                        <Row>
                            <Col size={1}>
                                {this.renderMpesaButton()}
                            </Col>
                        </Row>
                        <Row>
                            <Col size={1}>
                                {this.renderPayPayPal()}
                            </Col>
                        </Row>
                        <Row size={1}>
                            <Col size={1}>
                                {this.renderVisaButton()}
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
};

const styles = StyleSheet.create({
    headerStyle: {
        borderRadius: 0,
        alignSelf: 'flex-start',
        height: 100
    },
    textHeaderStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginTop: PHONE_HEIGHT / 50,
        alignContent: 'center',
        alignSelf: 'center',
        marginLeft: PHONE_WIDTH / 4,
    },
    forDeliverytStyle: {
        alignContent: 'center',
        fontSize: 15,
        marginLeft: 50,
        color: '#A7ADB4',
        fontFamily: "Helvetica"
    },
    cardStyle: {
        marginLeft: 25,
        marginRight: 25,
        marginTop: 25,
        marginBottom: 25
    },
    imageStyle: {
        height: 60,
        width: 230,
        margin: 30,
        borderRadius: 10
    },
    errorTextStyle: {
        fontSize: 15,
        alignSelf: 'center',
        color: 'red'
    },
})

const mapStateToProps = ({ payments }) => {
    const { loadMpesa, loadCard, loadPaypal, error, msisdn } = payments;
    return { loadMpesa, loadCard, loadPaypal, error, msisdn };
};

export default connect(mapStateToProps, {
    makePayment, orderPhoneNumber
})(Payments);