import { 
    Container, Content, Grid, 
    Row, Col, Card, Header, 
} from 'native-base';
import { StyleSheet, Text, View, Platform, Dimensions } from 'react-native';
import React, { Component } from 'react';
import ExtraDimensions from 'react-native-extra-dimensions-android';

// function to get phone height
const PHONE_HEIGHT = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : ExtraDimensions.getRealWindowHeight();

// function to get phone width
const PHONE_WIDTH = Platform.OS === "ios"
    ? Dimensions.get("window").width
    : ExtraDimensions.getRealWindowWidth();

const win = Dimensions.get('window');


class AppInfo extends Component {
    render() {

        const { 
            headerStyle, contentStyle,
            textHeaderStyle, mapStyle
        } = styles

        return (
            <Container>
                <Header transparent={true} iosBarStyle='light-content' androidStatusBarColor='#4f6d7a' rounded style={headerStyle} >
                    <Text style={textHeaderStyle}>App Info</Text>
                </Header>
                <Content style={contentStyle}>

                    <Grid style={{ marginTop: 40 }}>
                        <Row size={1}>
                            <Col size={1}>
                                <Card transparent >
                                    <View style={mapStyle}>

                                    </View>
                                </Card>
                            </Col>
                        </Row>
                    </Grid>
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle:{
        height: PHONE_HEIGHT/5,
        backgroundColor: '#352644' 
    },
    textHeaderStyle:{
        fontSize: 25,
        fontWeight: 'bold',
        color:'white',
        padding: 30,
        alignContent: 'center',
        alignSelf: 'center',
    },
    contentStyle:{
        padding: 0,
        backgroundColor: '#34183E',
    },
    mapStyle:{
        height: PHONE_HEIGHT/4,
        backgroundColor: 'white',
        margin: 10,
        borderRadius: 10
    },

})

export default AppInfo;