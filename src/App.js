import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './Reducers';
import Router from './Router';
import { Spinner } from './components/common';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin } from 'react-native-google-signin';
import { FIREBASE_SENDER_ID } from './Config';
import { ToastAndroid } from 'react-native';

class App extends Component{

    constructor() {
        super();
        this.state = { hasToken: false, isLoaded: false };
    }
    componentDidMount() {

        if (Platform.OS === 'ios') {
            var PushNotificationIOS = require("@react-native-community/push-notification-ios");
            PushNotificationIOS.addEventListener('registrationError', (registrationError) => {
                console.log('was error')
                console.log(registrationError)
                console.log(registrationError)
                console.log(registrationError)
            })
            // yes I'm aware I've added an event listener in the constructor also. Neither of these callbacks fire
            PushNotificationIOS.addEventListener('register', (token) => {
                console.log('this is the token', token);
            });
            console.log('requesting permissions')
            PushNotificationIOS.requestPermissions();
            GoogleSignin.configure();
            AsyncStorage.getItem('user').then((token) => {
                this.setState({ user: token !== null, isLoaded: true })
            });

        } else if (Platform.OS === 'android') {
            var PushNotification = require('react-native-push-notification');
            PushNotification.configure({

                onRegister: function (token) {
                    console.log(token)
                    AsyncStorage.setItem('device_uid', JSON.stringify(token.token))
                },

                // (required) Called when a remote or local notification is opened or received
                onNotification: function (notification) {
                    setTimeout(() => {
                        if (!notification['foreground']) {
                            ToastAndroid.show('You have clicked', ToastAndroid.SHORT)
                        }
                    }, 1)
                    PushNotification.localNotification({
                        title: 'Notification with my name',
                        message: notification['name'],
                        date: new Date(Date.now())
                    })
                    // process the notification

                    // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
                    notification.finish(PushNotificationIOS.FetchResult.NoData);
                },

                // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
                senderID: FIREBASE_SENDER_ID,

                // IOS ONLY (optional): default: all - Permissions to register.
                permissions: {
                    alert: true,
                    badge: true,
                    sound: true
                },

                // Should the initial notification be popped automatically
                // default: true
                popInitialNotification: true,

                /**
                  * (optional) default: true
                  * - Specified if permissions (ios) and token (android and ios) will requested or not,
                  * - if not, you must call PushNotificationsHandler.requestPermissions() later
                  */
                requestPermissions: true,
            });
            GoogleSignin.configure();
            AsyncStorage.getItem('user').then((token) => {
                this.setState({ user: token !== null, isLoaded: true })
            });
        }
    }
    

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
        if (!this.state.isLoaded) {
            return (
                <Spinner size='large' />
            )
        } else {
            if(this.state.user){
                return (
                    <Provider store={store}>
                        <Router main={true} auth={false} />
                    </Provider>
                );
            }else if(!this.state.user){
                return (
                    <Provider store={store}>
                        <Router main={false} auth={true} />
                    </Provider>
                ); 
            }
        }
    }
}


export default App;